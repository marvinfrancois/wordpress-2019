<?php

/*
Plugin Name: A360 Plugin Core
Plugin URI: http://www.whatsyouranthem.com
Description: Provides core class and helpers as well as initalize composer autoloader.
Version: 0.1
Author: Frank McCoy
Author URI: http://www.whatsyouranthem.com
*/

/**
 * Copyright (c) `date "+%Y"` Frank McCoy. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

add_action('activate_connections/connections.php', 'a360_connections_sp_create', 97);
add_action('activate_connections/connections.php', 'a360_connections_sp_call', 98);
add_action('activate_connections/connections.php', 'a360_connections_parse', 99);

function a360_connections_sp_call() {
	$mydb = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (get_option('a360_connections_sp_create') == '') {
	} else {
		if (get_option('a360_connections_sp_run') == '') {
			$mydb->query("CALL load_connections();");
		}
	}
	update_option('a360_connections_sp_run',time());
	unset($mydb);
}

function a360_connections_sp_create() {
	$mydb = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	$sql=<<<EOT
CREATE DEFINER=`root`@`localhost` PROCEDURE `load_connections`()
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE _pid, _new_id, _owner, _editor, _adder INT;
DECLARE _slug, _org CHAR(32);
DECLARE cur1 CURSOR FOR
	SELECT ID, post_title AS org, post_name AS slug, post_author AS owner, post_author AS editor, post_author AS adder
	FROM wp_posts
	WHERE post_type = 'listing';
OPEN cur1;
read_loop: LOOP
FETCH cur1 INTO _pid, _org, _slug, _owner, _editor, _adder;
	IF done THEN
		LEAVE read_loop;
	END IF;
	INSERT INTO wp_connections (`entry_type`, `visibility`, `slug`, `organization`, `added_by`, `edited_by`, `owner`, `user`, `status`, `options`)
		VALUES ('organization', 'public', _slug, _org, _adder, _editor, _owner, 0, 'approved', 'a:2:{s:5:"entry";a:1:{s:4:"type";s:12:"organization";}s:5:"group";a:1:{s:6:"family";a:0:{}}}');
	SET _new_id = LAST_INSERT_ID();
	INSERT INTO wp_connections_meta (entry_id, meta_key, meta_value)
		SELECT _new_id AS entry_id, meta_key, meta_value FROM wp_postmeta WHERE post_id = _pid;
END LOOP;
CLOSE cur1;
END;
EOT;
	if (get_option('a360_connections_sp_create') == '') {
		if ($mydb->query($sql) === "true") {
		} else {
			error_log($mydb->error);
		}
	}
	update_option('a360_connections_sp_create',time());
	unset($mydb);
}

function a360_connections_parse() {
	global $wpdb;
	if (get_option('a360_connections_parse') == '') {
		$connections = $wpdb->get_results("SELECT * FROM wp_connections_meta");
		$last_id = 0;
		$prefphoneused = false;
		$prefemailused = false;
		$phoneorder = 0;
		$emailorder = 0;
		$fields = array();
		$values = array();
		$street = '';
		$locality = '';
		$region = '';
		$postcode = '';
		$country = '';
		foreach($connections as $connection) {
			if ($last_id == $connection->entry_id) {
			} else {
				// switched to new contact, reset vars, update the records
				$last_id = $connection->entry_id;
				$prefphoneused = false;
				$prefemailused = false;
				$phoneorder = 0;
				$emailorder = 0;
				$street = '';
				$locality = '';
				$region = '';
				$postcode = '';
				$country = '';
			}
			$value = maybe_unserialize($connection->meta_value);
			switch ($connection->meta_key) {
				case 'a360_listing_companyname':
					break;
				case 'a360_listing_address':
					extract(json_decode($value, true), EXTR_OVERWRITE);
					$sql=<<<EOT
INSERT INTO wp_connections_address (`entry_id`, `order`, `preferred`, `type`, `line_1`, `line_2`, `line_3`, `city`, `state`, `zipcode`, `country`, `visibility`)
	VALUES ($last_id, 0, 1, 'work', '$street', '', '', '$locality', '$region', '$postcode', '$country', 'public');
EOT;
					$wpdb->query($sql);
					break;
				case 'a360_listing_tollfree':
				case 'a360_listing_phone':
				case 'a360_listing_contact_phone':
					$preferred = ($prefphoneused ? 0 : 1);
					$sql=<<<EOT
INSERT INTO wp_connections_phone (`entry_id`, `order`, `preferred`, `type`, `number`, `visibility`)
	VALUES ($last_id, $phoneorder, $preferred, 'workphone', '$value', 'public');
EOT;
					$phoneorder++;
					$prefphoneused = true;
					$wpdb->query($sql);
					break;
				case 'a360_listing_fax':
					$sql=<<<EOT
INSERT INTO wp_connections_phone (`entry_id`, `order`, `preferred`, `type`, `number`, `visibility`)
	VALUES ($last_id, $phoneorder, 0, 'workfax', '$value', 'public');
EOT;
					$phoneorder++;
					$wpdb->query($sql);
					break;
				case 'a360_listing_email':
				case 'a360_listing_contact_email':
					$preferred = ($prefemailused ? 0 : 1);
					$sql=<<<EOT
INSERT INTO wp_connections_email (`entry_id`, `order`, `preferred`, `type`, `address`, `visibility`)
	VALUES ($last_id, $emailorder, $preferred, 'work', '$value', 'public');
EOT;
					$emailorder++;
					$prefemailused = true;
					$wpdb->query($sql);
					break;
				case 'a360_listing_website':
					$sql=<<<EOT
INSERT INTO wp_connections_link (`entry_id`, `order`, `preferred`, `type`, `title`, `url`, `visibility`)
	VALUES ($last_id, 0, 1, 'website', '$value', '$value', 'public');
EOT;
					$wpdb->query($sql);
					break;
				case 'a360_listing_description':
					$sql=<<<EOT
UPDATE wp_connections SET `notes` = '$value' WHERE `id` = $last_id;
EOT;
					$wpdb->query($sql);
					break;
				case 'a360_listing_contact_name':
					$names = explode("/", $value);
					$name = array_shift($names);
					$nameparts = explode(" ", $name);
					$first = array_shift($nameparts);
					$last = array_pop($nameparts);
					$sql=<<<EOT
UPDATE wp_connections
	SET `first_name` = '$first', `last_name` = '$last',
		`contact_first_name` = '$first', `contact_last_name` = '$last'
	WHERE `id` = $last_id;
EOT;
					$wpdb->query($sql);
					break;
			}
		}
	}
	update_option('a360_connections_parse', time());
}

defined('A360_VENDOR_DIR') or define('A360_VENDOR_DIR', WP_CONTENT_DIR . '/vendor' );

require_once( A360_VENDOR_DIR . '/autoload.php' );