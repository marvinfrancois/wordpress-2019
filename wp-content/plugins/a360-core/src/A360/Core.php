<?php

namespace A360;

class Core
{
	const PREFIX = '_a360';
	const BGPREFIX = '_a360';
	const ASSET_VERSION = 5;

	public static function get_core_public_uri()
	{
		static $uri;

		if (is_null($uri))
		{
//			$uri = plugin_dir_url( __FILE__ );
//			$path_part = dirname(dirname(dirname(str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']))));
//			$uri = 'http://' . $_SERVER['HTTP_HOST'] . $path_part . "/";
//			$dir = getcwd();
//			chdir(dirname(__FILE__));
			$uri = plugin_dir_url(dirname(dirname(__FILE__)));
//			$uri = 'http://'.$_SERVER['HTTP_HOST'].'/'. ltrim(substr(rtrim(realpath('../../'), '/').'/', strlen($_SERVER['DOCUMENT_ROOT'])), '/');
//			chdir($dir);
		}

		return $uri;
	}
}
