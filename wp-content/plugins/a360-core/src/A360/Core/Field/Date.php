<?php

namespace A360\Core\Field;

use A360\Core;
use A360\Core\Field;

class Date extends Field
{

    public function __construct($name, $title, Core\FieldGroup $field_group = null)
    {
        is_admin() and add_action('init', __CLASS__ . '::enqueue_assets');
        parent::__construct($name, $title, $field_group);
    }

    public static function enqueue_assets()
    {
//		wp_enqueue_style('datepicker-core', Core::get_core_public_uri().'assets/js/components/pickadate/lib/themes/default.css');
//		wp_enqueue_style('datepicker-date', Core::get_core_public_uri().'assets/js/components/pickadate/lib/themes/default.date.css');
//		wp_enqueue_style('datepicker-time', Core::get_core_public_uri().'assets/js/components/pickadate/lib/themes/default.time.css');
        wp_enqueue_style('datepicker-core', Core::get_core_public_uri() . 'assets/css/style.min.css');
    }

    public static function render_field_template()
    {
        ?>
        <div style="position:relative; width:400px; ">
            <input type="text" class="regular-text datepicker" data-bind="attr: { name: $root.field, value: $data }">
            <a href="#" data-bind="visible: $root.multiple, click: function() { $root.removeField($index) }">Clear</a>
        </div>
        <?php
    }

    public function queue_viewmodel($element_id, $data)
    {
        // Knockout view model definition
        $data = json_encode($data);
        $default = json_encode(static::$default);
        $model = $this->get_viewmodel($element_id);

        add_action('admin_footer', function() use($model, $element_id, $data, $default) {
            ?>
            <script>
                a360Ready(['field/date'], function(bind) {
                    bind('#<?= $element_id; ?>', <?= $model; ?>, <?= $data; ?>, <?= $default; ?>);
                });
            </script>
            <?php
        });
    }

    public function validate_value(&$value)
    {
        if (!is_numeric($value) and (strtotime($value)) === false) {
            return null;
        }

        return $value;
    }

    /**
     * Store in GMT
     * 
     * @param type $value
     * @return type
     */
    public function serialize_value($value)
    {
        if (!is_numeric($value)) {
            $value = strtotime($value);
        }
        
        return $value;
    }

    /**
     * Convert GMT timestamp to local time equivalent
     * 
     * @param type $value
     * @param type $format
     * @return type
     */
    protected function to_local_time($value, $format = null)
    {
        if ($this->multiple && is_array($value)) {
            foreach ($value as &$v) {
                $v = $this->to_local_time($v);
            }
            return $value;
        }
        
        $value += (get_option('gmt_offset') * 3600);

        return $format ? date($format, $value) : $value;
    }

    public function deserialize_value($value)
    {
        $value = parent::deserialize_value($value);
        return $this->to_local_time($value, is_admin() ? 'Y-m-d g:i A' : null);
    }

}
