<?php

namespace A360\Core\Field;

use \A360\Core\Field;

class Textarea extends Field
{
	public static function render_field_template()
	{
		?>
        <textarea rows="5" cols="80" data-bind="attr: { name: $root.field }, text: $data"></textarea>
		<a href="#" data-bind="visible: $root.multiple, click: function() { $root.removeField($index) }">Clear</a>
		<?php
	}
}