<?php

namespace A360\Core\Field\Reference;

use \A360\Core\Field\Reference;
use \A360\Core\Content AS CoreContent;
use \A360\Core\FieldGroup;

class Content extends Reference
{
	public static function search($term = null, $args = array())
	{
		//Post Type must be represented as an array or string. 
		//Double checking to make sure a comma delimited string isn't being passed 
		// in. If so, we need to convert to an array. -MM 4/21
		if(array_key_exists('post_type', $args))
		{
			$args['post_type'] = explode(',', $args['post_type']);
		}
		
		$args = wp_parse_args($args, array(
			'posts_per_page' => 10,
			's'	=> $term,
		));

		$query = new \WP_Query($args);

		$data = array();
		foreach ($query->get_posts() as $post)
		{
			$data[] = array(
				'ID'		=> $post->ID,
				'display'	=> $post->post_title
			);
		}
		
		return $data;
	}
	
	
	protected $target_types = array();

	public function __construct($name, $title, FieldGroup $field_group = null, $target_types = array('post'))
	{
		parent::__construct($name, $title, $field_group);

		$this->target_types = $target_types;
	}

	public function deserialize_value($value = null)
	{
		if (is_array($value))
		{
			$value = parent::deserialize_value($value);
			foreach ($value as &$v)
			{
				$v = $this->deserialize_value($v);
			}
			return $value;
		}
		elseif (is_numeric($value))
		{
			return CoreContent::forge($value);
		}
	}

	public function get_viewmodel($element_id)
	{
		return json_encode(array(
			'id'		=> $element_id.'_model',
			'field'		=> $this->get_field_name(),
			'multiple'	=> $this->multiple,
			'title'		=> $this->title,
			'args'		=> array(
				'type'		=> 'Content',
				'post_type'	=> implode(',', $this->target_types),
			),
		));
	}
	
	public function prepare_viewmodel_data($data)
	{
		foreach ($data as &$ref)
		{
			if (is_object($ref) and $ref instanceof CoreContent)
			{
				$ref = array(
					'ID'		=> $ref->ID,
					'display'	=> $ref->post_title,
				);
			}
		}
		
		return $data;
	}
}