<?php

namespace A360\Core\Field\Reference;

use \A360\Core\Field\Reference;
use \A360\Core\User AS CoreUser;
use \A360\Core\FieldGroup;

class User extends Reference
{
	
	public static function search($term = null, $args = array())
	{
		global $wpdb;
		$args = wp_parse_args($args, array(
			'roles' => array()
		));
		
		extract($args, EXTR_SKIP);

		$query = "
			SELECT `ID`, `display_name` AS `display`
			FROM $wpdb->users a
			JOIN $wpdb->usermeta b ON b.user_id = a.ID AND b.meta_key = '{$wpdb->prefix}capabilities'
			JOIN $wpdb->usermeta c ON c.user_id = a.ID AND c.meta_key = 'first_name'
			JOIN $wpdb->usermeta d ON d.user_id = a.ID AND d.meta_key = 'last_name'
			WHERE 1=1
		";
		
		if ($term)
		{
			$term_query = array();
			foreach (array('a.user_login', 'c.meta_value', 'd.meta_value') as $field)
			{
				$term_query[] = "$field LIKE '%$term%'";
			}
			$query .= ' AND ('.implode(' OR ', $term_query).')';
		}
		
		if ($roles)
		{
			if (is_string($roles))
			{
				$roles = explode(',', $roles);
				array_walk($roles, 'trim');
			}
			
			$role_query = array();
			foreach ($roles as $role)
			{
				$role_query[] = "b.meta_value LIKE '%\"$role\"%'";
			}
			$query .= ' AND ('.implode(' OR ', $role_query).')';
		}
		
		$query .= " ORDER BY a.display_name ASC";
		$query .= " LIMIT 10";
			
		return $wpdb->get_results($query, OBJECT);
	}
	
	private $roles = array();

	public function __construct($name, $title, FieldGroup $field_group = null, $roles = array('editor'))
	{
		parent::__construct($name, $title, $field_group);
		
		is_scalar($roles) and $roles = array($roles);

		$this->roles = $roles;
	}

	public function deserialize_value($value = null)
	{
		if (is_array($value))
		{
			$value = parent::deserialize_value($value);
			foreach ($value as &$v)
			{
				$v = $this->deserialize_value($v);
			}
			return $value;
		}
		elseif (is_numeric($value))
		{
			return CoreUser::forge($value);
		}
	}

	public function get_viewmodel($element_id)
	{
		return json_encode(array(
			'id'		=> $element_id.'_model',
			'field'		=> $this->get_field_name(),
			'multiple'	=> $this->multiple,
			'title'		=> $this->title,
			'args'		=> array(
				'type'		=> 'User',
				'roles'		=> implode(',', $this->roles),
			),
		));
	}
	
	public function prepare_viewmodel_data($data)
	{
		foreach ($data as &$ref)
		{
			if (is_object($ref) and $ref instanceof CoreUser)
			{
				$ref = array(
					'ID'		=> $ref->ID,
					'display'	=> $ref->display_name,
				);
			}
		}
		
		return $data;
	}
}