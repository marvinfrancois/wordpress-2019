<?php

namespace A360\Core\Field;

use \A360\Core\Field;
use \A360\Core\FieldGroup;

abstract class Reference extends Field
{
	const TOKEN_PLACEHOLDER = '_REFERENCE_PLACEHOLDER_';

	protected static $default = '';
	protected static $ajax_init = false;


	public static function queue_templates()
	{
		$ui_class = static::get_ui_class();
		?>
		<script type="text/html" id="tpl-editor-<?= $ui_class; ?>">
			<th scope="row">
				<label data-bind="text: title"></label>
			</th>
			<td>
				<input type="hidden" value="<?= static::TOKEN_PLACEHOLDER; ?>" data-bind="if: $root.multiple, attr: { name: $root.field }">
				<ul style="margin:0;">
					<!-- ko foreach: data -->
					<li data-bind="template: { name: 'tpl-field-<?= $ui_class; ?>' }"></li>
					<!-- /ko -->
					<li data-bind="if: multiple">
						<a href="#" data-bind="click: addField">+ Add Another</a>
					</li>
				</ul>
			</td>
		</script>
		<script type="text/html" id="tpl-field-<?= $ui_class; ?>">
		<?php static::render_field_template(); ?>
		</script>
	<?php
	}


	public function __construct($name, $title, FieldGroup $field_group = null)
	{
		parent::__construct($name, $title, $field_group);

		if ( ! self::$ajax_init)
		{
			self::$ajax_init = true;
			add_action('wp_ajax_a360_autocomplete', __CLASS__.'::autocomplete');
		}
	}
	
	public static function search($term = null, $args = array())
	{
		return array();
	}

	public static function autocomplete()
	{
		if (get_called_class() !== __CLASS__)
		{
			return array();
		}
		
		$results = array();
		
		if (isset($_GET['type']))
		{
			$method = __CLASS__.'\\'.$_GET['type'].'::search';
			if (is_callable($method))
			{
				$term = empty($_GET['search']) ? null : $_GET['search'];
				$args = $_GET;
				unset($args['action'], $args['type'], $args['search'], $args['_']);
				$results = call_user_func($method, $term, $args);
				empty($results) and $results = array();
			}
		}

		header('Content-type: application/json');
		echo json_encode($results);
		exit;
	}

	public static function render_field_template()
	{
		?>
		<div class="model" data-bind="attr: { 'data-model': $root.id, 'data-index': $index, 'data-type': $root.type }">
			<div data-bind="if: typeof display !== 'undefined'">
				<input type="hidden" data-bind="attr: { name: $root.field, value: ID }">
				<span data-bind="text: display"></span>
				<a href="#" data-bind="click: function() { $root.removeField($index) }">Clear</a>
			</div>
			<div data-bind="if: typeof display === 'undefined'">
				<input type="text" class="regular-text autocomplete" placeholder="Start typing...">
			</div>
		</div>
		<?php
	}
	
	public function validate_value(&$value)
	{
		return is_numeric($value) ? $value : false;
	}
	
	public function prepare_viewmodel_data($data)
	{
		return $data;
	}

	public function queue_viewmodel($element_id, $data)
	{
		// Knockout view model definition
		$data		= json_encode(static::prepare_viewmodel_data($data));
		$default	= json_encode(static::$default);
		$model		= $this->get_viewmodel($element_id);

		add_action('admin_footer', function() use($model, $element_id, $data, $default) {
			?>
			<script>
			a360Ready(['jquery', 'knockout', 'a360'], function($, ko, init) {
				var model = <?= $model; ?>;
				model.data = ko.observableArray(<?= $data; ?>);
				model.addField = function() {
					model.data.push(<?= $default; ?>);
				};
				model.removeField = function(index) {
					model.data.splice(index(), 1);
					if (model.data().length === 0)
					{
						model.addField();
					}
				};
				model.selectValue = function(select, value) {
					$(select).val(value);
				};
				model.setValue = function(index, ID, display)
				{
					model.data.splice(index, 1, {
						ID: ID,
						display: display
					});
				};
				ko.applyBindings(model, document.getElementById('<?= $element_id; ?>'));
				window['<?= $element_id; ?>_model'] = model;
				
				init();
				init.autoCompleter.bindInput('<?= $element_id; ?>', model.args);
			});
			</script>
			<?php
		});
	}
}