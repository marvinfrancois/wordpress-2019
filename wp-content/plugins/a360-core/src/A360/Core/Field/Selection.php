<?php
namespace A360\Core\Field;

use A360\Core\FieldGroup;
use A360\Core\Field;

class Selection extends Field
{
	const TOKEN_PLACEHOLDER = '_CHECKBOX_PLACEHOLDER_';
	
	public static function queue_templates()
	{
		$ui_class = static::get_ui_class();
		?>
		<script type="text/html" id="tpl-editor-<?= $ui_class; ?>">
		<th scope="row">
			<label data-bind="text: title"></label>
		</th>
		<td>
			<input type="hidden" value="<?= static::TOKEN_PLACEHOLDER; ?>" data-bind="if: $root.multiple, attr: { name: $root.field }">
			<div data-bind="template: { name: 'tpl-field-<?= $ui_class; ?>', foreach: options }"
		</td>
		</script>
		<script type="text/html" id="tpl-field-<?= $ui_class; ?>">
		<?php static::render_field_template(); ?>
		</script>
		<?php
	}
	
	public static function render_field_template()
	{
		?>
		<label style="display:block">
			<input type="checkbox" data-bind="attr: { type: $root.multiple ? 'checkbox' : 'radio', name: $root.field, value: $data.key, checked: $root.checked($data.key) }">
			<span data-bind="text: $data.value"></span>
		</label>
		<?php
	}
	
	
	protected $options = array();
	
	public function __construct($name, $title, FieldGroup $field_group = null, $options = array())
	{
		parent::__construct($name, $title, $field_group);
		
		$this->options = $options;
	}
	
	public function get_viewmodel($element_id)
	{
		$options = array();
		foreach ($this->options as $key => $value)
		{
			$options[] = array('key' => $key, 'value' => $value);
		}
		
		return json_encode(array(
			'id'		=> $element_id.'_model',
			'field'		=> $this->get_field_name(),
			'multiple'	=> $this->multiple,
			'title'		=> $this->title,
			'options'	=> $options,
		));
	}
	
	public function queue_viewmodel($element_id, $data)
	{
		// Knockout view model definition
		$data		= json_encode($data);
		$default	= json_encode(static::$default);
		$model		= $this->get_viewmodel($element_id);

		add_action('admin_footer', function() use($model, $element_id, $data, $default) {
			?>
			<script>
			a360Ready(['knockout'], function(ko) {
				var model = <?= $model; ?>;
				model.data = ko.observableArray(<?= $data; ?>);
				model.checked = function(value) {
					return model.data.indexOf(value) < 0 ? false : 'checked';
				};
				model.keys = function (map) {
					var keys = [];
					for (var i in map)
					{
						keys.push(map[i].key);
					}
					return keys;
				};
				model.values = function (map) {
					var values = [];
					for (var i in map)
					{
						values.push(map[i].value);
					}
					return values;
				};
				ko.applyBindings(model, document.getElementById('<?= $element_id; ?>'));
			});
			</script>
			<?php
		});
	}
	
	public function validate_value(&$value)
	{
		if ($value === static::TOKEN_PLACEHOLDER)
		{
			return false;
		}
		
		return $value;
	}
	
	
}