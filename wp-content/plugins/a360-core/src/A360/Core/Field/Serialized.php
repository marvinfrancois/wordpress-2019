<?php

namespace A360\Core\Field;

use \A360\Core\Field;

abstract class Serialized extends Field
{
	public function get_default_value($force_single = false)
	{
		$default = is_null($this->default_value) ? static::$default : $this->default_value;
		
		return ($this->multiple and ! $force_single) ? array((object)$default) : (object)$default;
	}
	
	public function serialize_value($value)
	{
		return json_encode($value);
	}

	public function deserialize_value($value = null)
	{
		if (is_array($value))
		{
			foreach ($value as &$v)
			{
				$v = $this->deserialize_value($v);
			}
			return $value;
		}
		
		return $value ? json_decode($value) : (object)static::get_default_value();
	}
}