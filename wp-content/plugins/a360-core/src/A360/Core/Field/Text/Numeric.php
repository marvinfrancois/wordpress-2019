<?php

namespace A360\Core\Field\Text;

use \A360\Core\Field\Text;

class Numeric extends Text
{
    public function validate_value(&$value) {
        return preg_replace('|\D|', '', $value);
    }
}