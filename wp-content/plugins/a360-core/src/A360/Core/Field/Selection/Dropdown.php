<?php
namespace A360\Core\Field\Selection;

use A360\Core\Field\Selection;

class Dropdown extends Selection
{
	public static function queue_templates()
	{
		$ui_class = static::get_ui_class();
		?>
		<script type="text/html" id="tpl-editor-<?= $ui_class; ?>">
		<th scope="row">
			<label data-bind="text: title"></label>
		</th>
		<td>
			<input type="hidden" value="<?= static::TOKEN_PLACEHOLDER; ?>" data-bind="if: $root.multiple, attr: { name: $root.field }">
			<div data-bind="template: { name: 'tpl-field-<?= $ui_class; ?>', data: options }"
		</td>
		</script>
		<script type="text/html" id="tpl-field-<?= $ui_class; ?>">
		<?php static::render_field_template(); ?>
		</script>
		<?php
	}
	
	public static function render_field_template()
	{
		?>
		<label style="display:block">
			<select data-bind="options: $data, optionsText: 'value', optionsValue: 'key', value: $root.data, attr: { name:$root.field, multiple: $root.multiple ? 'multiple' : false, size: $root.multiple ? 5 : false }" style="height:auto"></select>
		</label>
		<?php
	}
}