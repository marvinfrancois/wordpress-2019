<?php

namespace A360\Core\Field;

use \A360\Core\Field;

class Image extends Field
{
    public static function render_field_template()
	{
		?>
        <div class="a360-attachment">
            <input type="hidden" class="regular-text" data-bind="attr: { name: $root.field, value: $data }">
            <img data-bind="if: $data, attr: { src: $data }" alt="" style="vertical-align:top; max-width:240px; height:auto; "/>
            <input type="button" class="button" value="Upload Image" style="vertical-align:top; margin-left:10px; " data-bind="click: function() { $root.upload($index()) }"/>
            <input type="button" class="button" value="Remove" style="vertical-align:top; margin-left:10px; " data-bind="visible: $data || $root.data().length > 1, click: function() { $root.removeField($index) }"/>
        </div>
		<?php
	}
	
	public function queue_viewmodel($element_id, $data)
	{
		// Knockout view model definition
		$data		= json_encode($data);
		$default	= json_encode(static::$default);
		$model		= $this->get_viewmodel($element_id);

		add_action('admin_footer', function() use($model, $element_id, $data, $default) {
			?>
			<script>
			a360Ready(['knockout', 'field/image'], function(ko, initUploader) {
				var model = <?= $model; ?>,
                    element = document.getElementById('<?= $element_id; ?>');
				model.data = ko.observableArray(<?= $data; ?>);
				model.upload = initUploader(element, model);
                model.addField = function() {
					model.data.push(<?= $default; ?>);
                    if (model.data().length > 1)
                    {
                        model.upload(model.data().length - 1);
                    }
				};
				model.removeField = function(index) {
					model.data.splice(index(), 1);
					if (model.data().length === 0)
					{
						model.addField();
					}
				};
				ko.applyBindings(model, element);
			});
			</script>
			<?php
		});
	}
    
	public function editor($data)
	{
		wp_enqueue_media();
        return parent::editor($data);
		?>
		<tr class="<?= $this->get_ui_class(); ?>" data-multiple="<?= $this->multiple; ?>" data-fieldname="<?= $this->get_field_name(); ?>">
			<th scope="row">
				<label><?= $this->title; ?></label>
			</th>
			<td>
				<?php
				if ($data) :
					foreach ( (array) $data as $img) :
						?>
						<div class="a360-attachment">
							<input type="hidden" name="<?= $this->get_field_name(); ?>" value="<?= $img; ?>">
							<img src="<?= $img; ?>" width="400" alt="" style="vertical-align:top;max-width:240px;" />
							<a href="#" class="button" style="vertical-align:top; margin-left:10px; ">Remove</a>
						</div>
						<?php
					endforeach;
				endif;
				?>
				<input type="button" class="button" value="Upload Image" style="margin-top:10px;" />
			</td>
		</tr>
		<?php
	}
}