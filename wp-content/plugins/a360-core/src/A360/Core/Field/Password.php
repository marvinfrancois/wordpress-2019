<?php

namespace A360\Core\Field;

use \A360\Core\Field;

class Password extends Field
{
	public static function render_field_template()
	{
		?>
		<input type="password" data-bind="attr: { name: $root.field }" placeholder="Password">
		<input type="password" placeholder="Confirm">
		<?php
	}
	
	public function validate_value(&$value)
	{
		if (empty($value))
		{
			return null;
		}
		
		return true;
	}
	
	public function serialize_value($value)
	{
		return wp_hash_password($value);
	}
}