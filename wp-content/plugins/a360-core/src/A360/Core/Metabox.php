<?php

namespace A360\Core;

interface iMetabox
{
	public static function render();
}

abstract class Metabox implements iMetabox
{
	public static $attr = array();

	public static function register()
	{
		$attr = array_merge(array(
			'screen'		=> null,
			'context'		=> 'advanced',
			'priority'		=> 'default',
			'callback_args'	=> null,
		), static::$attr);

		$class = get_called_class();

		add_action('add_meta_boxes', function() use ($attr, $class) {
			add_meta_box($attr['name'], $attr['title'], function() use ($attr, $class) {
				call_user_func($class.'::render');
			}, $attr['screen'], $attr['context'], $attr['priority'], $attr['callback_args']);
		});
	}
}