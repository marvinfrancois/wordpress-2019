<?php

namespace A360\Core;

class Content extends Base
{

    const KEY_A = 'ARRAY';
    const KEY_ID = 'ID';
    const KEY_POST = 'POST';
    const KEY_FIELD = 'FIELD';
    const KEY_META = 'META';

    protected static $instances = array();

	protected $ID;
	protected $type;
	protected $post_data = array();
	protected $meta_data;
	protected $fields = array();
	protected $field_data;
	protected $changes = array();

    public static function forge($id = null, $type = null)
    {
        if (empty($id)) {
            $args = array();
            $type and $args['post_type'] = $type;
            return new static($args);
        }

        if (isset(static::$instances[$id])) {
            return static::$instances[$id];
        }

        if (!$post = get_post($id, ARRAY_A) or ( $type and $post['post_type'] != $type)) {
            return false;
        }

        return new static($post);
    }


    /**
     * Constructor
     * @param array $post Initial post data
     */
    public function __construct($post = array())
    {
        $this->init($post);
    }

    public function init($post = array())
    {
        $this->post_data = array();
        $this->meta_data = null;
        $this->field_data = null;

        if ($post) {
            empty($post['ID']) or $this->ID($post['ID']);
            empty($post['post_type']) or $this->type($post['post_type']);

            $this->post_data = $post;
        }

        return $this;
    }

    /**
     *
     * @param type $name
     * @param type $arguments
     * @return type
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (($ct = $this->get_content_type()) and isset($ct->methods[$name])) {
            array_unshift($arguments, $this);
            return call_user_func_array($ct->methods[$name], $arguments);
        }

        throw new Exception('Invalid method call: ' . $name . ' of content type: ' . $this->type);
    }

    /**
     * Magic alias of get()
     *
     * @param String $key
     * @return Mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Generic getter
     *
     * @param String $key
     * @return Mixed
     */
    public function get($key)
    {
        if (($post = $this->post($key))) {
            return $post;
        } elseif (($field = $this->field($key))) {
            return $field;
        } elseif (($meta = $this->meta($key))) {
            return $meta;
        }
    }

    public function __set($key, $value)
    {
        return $this->set($key, $value);
    }

    public function set($key, $value = null)
    {
        switch ($this->get_key_type($key)) {
            case static::KEY_A:
                $return = array();
                foreach ($key as $k => $v) {
                    $return[$k] = $this->set($k, $v);
                }
                return $return;

            case static::KEY_ID:
                return $this->ID($value);

            case static::KEY_POST:
                if ($key == 'post_type') {
                    return $this->type($value);
                }
                return $this->post($key, $value);

            case static::KEY_FIELD:
                return $this->field($key, $value);

            default:
            case static::KEY_META:
                return $this->meta($key, $value);
        }
    }

    protected function get_key_type($key)
    {
        if (is_array($key)) {
            return static::KEY_A;
        } elseif ($key == 'ID') {
            return static::KEY_ID;
        } elseif (substr($key, 0, 5) === 'post_') {
            return static::KEY_POST;
        } elseif (array_key_exists($key, $this->get_fields())) {
            return static::KEY_FIELD;
        } else {
            return static::KEY_META;
        }
    }

    public function exists()
    {
        return !empty($this->ID);
    }

    public function save()
    {
        if (!$this->changes) {
            return null;
        }

        if (!apply_filters($this->type . '.beforeSave', $this)) {
            return false;
        }

        // Update post data first
        if (($post_id = wp_insert_post($this->post_data))) {
            $this->ID($post_id);
        } else {
            return false;
        }

        $fields = $this->get_fields();
        $updates = array();
        foreach (array_keys($this->changes) as $key) {
            switch ($this->get_key_type($key)) {
                case static::KEY_FIELD:
                    if ($fields[$key]->save($this->ID, $this->field_data[$key])) {
                        $updates[] = $key;
                    }
                    break;

                case static::KEY_META:
                    if (update_post_meta($this->ID, $key, $this->meta_data[$key])) {
                        $updates[] = $key;
                    }
                    break;
            }
        }

        $this->refresh();

        if (!apply_filters($this->type . '.save', $this)) {
            return false;
        }

        return $updates;
    }

    /**
     * Re-initialize from database
     *
     */
    public function refresh()
    {
        if ($this->exists()) {
            $this->init(get_post($this->ID, ARRAY_A));
        } else {
            $this->init();
        }
    }

    /**
     * ID getter/setter
     *
     * @param type $new_id
     * @return type
     */
    public function ID($new_id = null)
    {
        if (!empty($new_id)) {
            $this->ID = $new_id;
            $this->post_data['ID'] = $new_id;

            static::$instances[$this->ID] = & $this;
        }

        return $this->ID;
    }

    /**
     * Post type getter/setter
     * @param type $new_type
     * @return type
     */
    public function type($new_type = null)
    {
        if (!empty($new_type)) {
            $this->type = $new_type;
            $this->post('post_type', $new_type);
        }

        return $this->get('post_type');
    }

    /**
     * Post data getter/setter
     *
     * @param type $key
     * @param type $value
     * @return null
     */
    public function post($key, $value = null)
    {
        // Array Setter
        if (is_array($key)) {
            $return = array();
            foreach ($key as $k => $v) {
                $return[$k] = $this->post($k, $v);
            }
            return $return;
        }

        // Getter
        elseif (is_null($value)) {
            if (isset($this->post_data[$key])) {
                return $this->post_data[$key];
            } elseif (isset($this->post_data['post_' . $key])) {
                return $this->post_data['post_' . $key];
            } else {
                return null;
            }
        }
        // Regular Setter
        else {
            if (!isset($this->changes[$key])) {
                $this->changes[$key] = $this->post($key);
            }

            $this->post_data[$key] = $value;

            return $this->changes[$key];
        }
    }

    /**
     * Custom field getter/setter
     *
     * @param type $key
     * @param type $value
     * @return type
     */
    public function field($key = null, $value = null)
    {
        // Have we already cached field values?
        if (is_null($this->field_data)) {
            // Start with an empty array
            $this->field_data = array();

            // Field values are stored in post's meta array
            $meta = $this->meta();
            foreach ($this->get_fields() as $field) {
                $this->field_data[$field->name] = $field->get_value($meta, false);
            }
        }

        // Setting a value
        if ($key and $value) {
            if (!isset($this->changes[$key])) {
                $this->changes[$key] = $this->field($key);
            }

            $this->field_data[$key] = $value;
        }

        // Getting a specific field
        elseif ($key) {
            return isset($this->field_data[$key]) ? $this->field_data[$key] : null;
        }

        // Return all fields (default)
        else {
            return $this->field_data;
        }
    }

    /**
     * Post meta getter/setter
     * @param type $key
     * @return type
     */
    public function meta($key = null)
    {
        if (is_null($this->meta_data)) {
            if (!($this->meta_data = get_post_meta($this->ID))) {
                $this->meta_data = array();
            }
        }

        if ($key) {
            return isset($this->meta_data[$key]) ? $this->meta_data[$key] : null;
        }

        return $this->meta_data;
    }

    /**
     * Helper method to get post's permalink
     *
     * @return type
     */
    public function get_link()
    {
        return get_permalink($this->ID);
    }

    /**
     *
     * @return type
     * @throws \BadMethodCallException
     */
    public function get_content_type()
    {
        if (empty($this->type)) {
            dd($this);
//			print_r($this->post_data);
//			exit;
            throw new \BadMethodCallException(__METHOD__ . ' called before setting $type property');
        }

        return ContentType::instance($this->type);
    }

    /**
     *
     * @return type
     */
    public function get_fields()
    {
        if (empty($this->fields)) {
        	if ($this->get_content_type())
            $this->fields = $this->get_content_type()->get_fields();
        }

        return $this->fields;
    }

}
