<?php

namespace A360\Core;


abstract class Plugin
{
	const VERSION = 1;
	const DEBUG = false;

	protected static $widgets = array();

	public static function init()
	{
		add_action('plugins_loaded', get_called_class().'::bootstrap');
	}

	public static function bootstrap()
	{
		if (static::DEBUG)
		{
			error_reporting(E_ALL);
			ini_set('display_errors', 'on');
		}

		static::register_content_types();
		static::register_taxonomies();
		static::register_shortcodes();
		static::register_metaboxes();
		static::register_widgets();
		static::_check_version();
	}

	public static function register_content_types() {}
	public static function register_taxonomies() {}
	public static function register_shortcodes() {}
	public static function register_metaboxes()	{}

	public static function install()
	{
		static::_set_version(static::VERSION);
	}

	public static function update()
	{
		static::_set_version(static::VERSION);
	}

	public static function disable_wordpress_updates($file = __FILE__)
	{
		add_filter('site_transient_update_plugins', function ($value) use ($file) {
			if ($value and is_object($value) and isset($value->response))
			{
				unset($value->response[ plugin_basename($file) ]);
			}
			return $value;
		});
	}

	protected static function _check_version()
	{
		if (static::_get_version() < static::VERSION)
		{
			static::update();
		}
	}

	protected static function _get_version()
	{
		$key = Util::declassify(get_called_class().'_version');
		return get_site_option( $key );
	}

	protected static function _set_version($version)
	{
		$key = Util::declassify( get_called_class() . '_version' );
		add_site_option( $key , $version);
	}

	public static function register_widgets()
	{
		if (static::$widgets)
		{
			$widgets = static::$widgets;
			add_action('widgets_init', function() use($widgets) {
				foreach ($widgets as $widget)
				{
					register_widget($widget);
				}
			});
		}
	}

    public static function register_autoloader($path, $namespace)
	{
		$path		= rtrim($path, '/').'/';
		$namespace	= rtrim($namespace, '\\').'\\';

		spl_autoload_register(function ($class) use ($path, $namespace) {
			if (substr($class, 0, strlen($namespace)) === $namespace)
			{
				$file = $path . str_replace('\\', '/', substr($class, strlen($namespace))) . '.php';
//				var_dump($file);
				is_file($file) and require_once($file);

//				var_dump($class);
//				var_dump(class_exists($class, false)); exit;
			}
		});
	}

	public static function register_post_status($name, $label, $plural_label = null, $args = array())
	{
		empty($plural_label) and $plural_label = $label;

		$args = array_merge(array(
			'label'                     => _x( $label, 'post' ),
			'public'                    => false,
			'show_in_admin_all_list'    => false,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( $label.' <span class="count">(%s)</span>', $plural_label.' <span class="count">(%s)</span>' )
		), $args);

		// "Archived" post status
		register_post_status( $name, $args);

		add_action('admin_footer-post.php', function() use ($name, $label) {
			global $post;
			$selected = '';
			$spanlabel = '';
			if ($post->post_type == 'post') :
				 if ($post->post_status == $name) :
					  $selected = ' selected=\"selected\"';
					  $spanlabel = '<span id=\"post-status-display\"> '.$label.'</span>';
				 endif;
				 ?>
				 <script>
				 jQuery(document).ready(function($){
					  $("select#post_status").append("<option value=\"archive\"<?= $selected; ?>><?= $label; ?></option>");
					  $(".misc-pub-section label").append("<?= $spanlabel; ?>");
				 });
				 </script>
				 <?php
			endif;
	   });

		add_filter( 'display_post_states', function( $states ) use($name, $label) {
			global $post;
			$arg = get_query_var( 'post_status' );
			if ($arg != $name)
			{
				if ($post->post_status == '')
				{
					return array($label);
				}
			}
			return $states;
		});

	   add_action( 'admin_footer-edit.php', function() use($name, $label) {
		   ?>
		   <script>
		   jQuery(document).ready(function($){
			   $(".inline-edit-status select ").append("<option value=\"<?= $name; ?>\"><?= $label; ?></option>");
		   });
		   </script>
		   <?php
	   });
	}

	public static function register_archived_status()
	{
		static::register_post_status('archive', 'Archived');
	}
}