<?php

namespace A360\Core;

use A360\Core;

abstract class Field extends Base
{

    protected static $attr_reader = array('name', 'title');
    protected static $attr_accessor = array('multiple');

    protected static $default = '';

    public static function forge()
    {
        $args = func_get_args();
        $ref = new \ReflectionClass(get_called_class());
        return $ref->newInstanceArgs($args);
    }

    public static function queue_templates()
    {
        $ui_class = static::get_ui_class();
        ?>
        <script type="text/html" id="tpl-editor-<?= $ui_class; ?>">
            <th scope="row">
                <label data-bind="text: title"></label>
            </th>
            <td>
                <ul style="margin:0;">
                    <!-- ko foreach: data -->
                    <li data-bind="template: { name: 'tpl-field-<?= $ui_class; ?>' }"></li>
                    <!-- /ko -->
                    <li data-bind="if: multiple">
                        <a href="#" data-bind="click: addField">+ Add Another</a>
                    </li>
                </ul>
            </td>
        </script>
        <script type="text/html" id="tpl-field-<?= $ui_class; ?>">
        <?php static::render_field_template(); ?>
        </script>
        <?php
    }

    protected static function get_ui_class()
    {
        return Util::declassify(get_called_class());
    }

    protected $field_group;
    protected $title;
    protected $name;
    protected $multiple = false;
    protected $default_value;
    protected $args = array(
        'allow_unescaped' => false
    );

    public function __construct($name, $title, FieldGroup $field_group = null)
    {
        $this->name = $name;
        $this->title = $title;

        if ($field_group) {
            $this->field_group = $field_group;
            $this->field_group->add_field($this);
        }
    }

    // GETTERS/SETTERS

    public function multiple($value = null)
    {
        if (!is_null($value)) {
            $this->multiple = $value;
            return $this;
        }

        return $this->multiple;
    }

    public function arg($key, $value = null)
    {
        // Getter
        if (is_null($value) && !is_array($key)) {
            return isset($this->args[$key]) ? $this->args[$key] : null;
        }
        // Setter
        else {
            if (is_array($key)) {
                foreach ($key as $k => $v) {
                    $this->arg($k, $v);
                }
            } else {
                $this->args[$key] = $value;
            }

            return $this;
        }
    }

    public function default_value($value)
    {
        $this->default_value = $value;

        return $this;
    }

    public function get_default_value()
    {
        $default = is_null($this->default_value) ? static::$default : $this->default_value;

        return $this->multiple ? array($default) : $default;
    }

    public function serialize_value($value)
    {
        return $value;
    }

    public function deserialize_value($value)
    {
        if ($value) {
            is_serialized($value) and $value = unserialize($value);
            if (is_string($value) and !$this->arg('allow_unescaped')) {
                $value = esc_attr($value);
            }
        }

        if ($this->multiple and !is_array($value)) {
            $value = array($value);
        }

        return $value;
    }

    // CRUD

    public function validate_value(&$value)
    {
        return $value;
    }

    public function get_post_values()
    {
    	global $typenow, $post;
    	switch ($typenow ?: $post->post_type) {
       		case 'listing':
   				$target = Core::BGPREFIX;
   				break;
    		default:
    			$target = Core::PREFIX;
    	}
        if (isset($_POST[$target][$this->name])) {
            return $_POST[$target][$this->name];
        }
    }

    public function save($post_id, $values = null)
    {
        is_null($values) and $values = $this->get_post_values();

        if (is_null($values)) {
            return;
        }

        $key = $this->get_key();
        $values = $this->multiple ? $values : array($values);

        $deleted = false;
        $updated = false;

        foreach ($values as $value) {
            $value = $this->validate_value($value);

            // Returning null skips without deleting or adding any values
            if ($value === null)
                continue;

            // Otherwise, we want to delete values before adding anything new
            if (!$deleted) {
                delete_post_meta($post_id, $key);
                $deleted = true;
            }

            // Returning false skips value after deleting old values
            if ($value === false)
                continue;

            // Create post meta
            add_post_meta($post_id, $key, $this->serialize_value($value));

            $updated = true;
        }

        return $updated;
    }

    // IDENTIFIERS

    public function get_key()
    {
    	global $typenow, $post;
    	switch ($typenow ?: $post->post_type) {
    		case 'listing':
    			$prefix = Core::BGPREFIX;
    			break;
    		default:
    			$prefix = Core::PREFIX;
    	}
        return $prefix . '_' . $this->field_group->content_type->name . '_' . $this->name;
    }

    public function get_field_name()
    {
    	global $typenow, $post;
    	switch ($typenow ?: $post->post_type) {
       	    case 'listing':
    			$prefix = Core::BGPREFIX;
    			break;
    		default:
    			$prefix = Core::PREFIX;
    	}
        return $prefix . '[' . $this->name . ']' . ($this->multiple ? '[]' : '');
    }

    public function get_value($post_meta, $use_default = true)
    {
        $prefix = $this->get_key();

        if (isset($post_meta[$prefix])) {
            if ($this->multiple) {
                $value = $post_meta[$prefix];
            } else {
                $value = $post_meta[$prefix][0];
            }

            return $this->deserialize_value($value);
        }

        return $use_default ? $this->get_default_value() : null;
    }

    public function set_value($value, $validate = true)
    {
        // @todo?
    }

    public function get_viewmodel($element_id)
    {
        return json_encode(array(
            'id' => $element_id . '_model',
            'field' => $this->get_field_name(),
            'multiple' => $this->multiple,
            'title' => $this->title,
            'args' => $this->args,
        ));
    }

    public function queue_viewmodel($element_id, $data)
    {
        // Knockout view model definition
        $data = json_encode($data);
        $default = json_encode(static::$default);
        $model = $this->get_viewmodel($element_id);

        add_action('admin_footer', function() use($model, $element_id, $data, $default) {
            ?>
            <script>
                a360Ready(['jquery', 'knockout'], function($, ko) {
                    var model = <?= $model; ?>;
                    model.data = ko.observableArray(<?= $data; ?>);
                    model.addField = function() {
                        model.data.push(<?= $default; ?>);
                    };
                    model.removeField = function(index) {
                        model.data.splice(index(), 1);
                        if (model.data().length === 0)
                        {
                            model.addField();
                        }
                    };
                    model.selectValue = function(select, value) {
                        $(select).val(value);
                    };
                    ko.applyBindings(model, document.getElementById('<?= $element_id; ?>'));
                });
            </script>
            <?php
        });
    }

    // TEMPLATES

    public function editor($data)
    {
        // Queue templates
        static $queued = array();
        $class = get_called_class();
        if (!isset($queued[$class])) {
            $queued[$class] = $class . '::queue_templates';
            add_action('admin_footer', $queued[$class]);
        }

        // Make sure data is an array
        is_array($data) or $data = array($data);

        // Create TR placeholder and bind to template
        $element_id = $this->get_key();
        $ui_class = static::get_ui_class();
        ?>
        <tr id="<?= $element_id; ?>" class="<?= $ui_class; ?>" data-bind="template: 'tpl-editor-<?= $ui_class; ?>'"></tr>
        <?php
        $this->queue_viewmodel($element_id, $data);
    }

    public function field_template()
    {

    }

    // @deprecated
    public function get_fields($values)
    {
        if (!$this->multiple) {
            return $this->field_editor($values);
        }
        ?>
        <ul class="multi-fields">
        <?php foreach ($values as $value) : ?>
                <li><?php $this->field_editor($value); ?></li>
        <?php endforeach; ?>
        <?php $this->add_another_field_editor(); ?>
        </ul>
        <?php
    }

    // @deprecated
    public function add_another_field_editor()
    {
        ?>
        <li><a href="#">+ Add Another</a></li>
        <?php
    }

}
