<?php

namespace A360\Core;

class Util
{
	public static function attr(array $attributes = array())
	{
		foreach ($attributes as $attribute => &$data)
		{
			$data = implode(' ', (array) $data);
			//$data = $attribute . '="' . htmlspecialchars($data, ENT_QUOTES, 'UTF-8') . '"';
			$data = $attribute . '="' . $data . '"';
		}
		return $attributes ? ' ' . implode(' ', $attributes) : '';
	}

	public static function form_select($attr, $options, $selected = '')
	{
		is_array($attr) or $attr = array('name' => $attr);

		$return = '<select'.static::attr($attr).'>';
		foreach($options as $value => $label)
		{
			$attr = array('value' => $value);
			($selected == $value) and $attr['selected'] = 'selected';
			$return .= '<option'.static::attr($attr).'>'.$label.'</option>';
		}
		return $return.'</select>';
	}
	
	public static function declassify($class)
	{
		return str_replace('\\', '-', strtolower($class));
	}
}