<?php
namespace A360\Core;

class Theme extends Plugin
{
	public static function init()
	{
		$class = get_called_class();
		
		add_action('after_setup_theme', $class.'::bootstrap');
		add_action('widgets_init', $class.'::register_widgets');
		add_action('wp_enqueue_scripts', $class.'::register_assets');
		add_action('admin_init', $class.'::register_admin');
	}
	
	public static function bootstrap()
	{
		parent::bootstrap();
		
		static::register_support();
		static::register_actions();
		static::register_filters();
		static::register_menus();
		static::register_sidebars();
	}	
	
	public static function register_support() {}
	public static function register_actions() {}
	public static function register_filters() {}
	public static function register_menus() {}
	public static function register_sidebars() {}
	public static function register_assets() {}
	public static function register_admin() {}
}