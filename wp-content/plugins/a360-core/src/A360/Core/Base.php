<?php

namespace A360\Core;

class Base
{
	protected static $attr_accessor	= array();
	protected static $attr_reader	= array();
	protected static $attr_writer	= array();
	public static $string_mode = "csv";
	public static $showheader = true;
	public static $csvHeader = ",";

	public function __get($name)
	{
		if ( ! ( in_array($name, static::$attr_reader) or in_array($name, static::$attr_accessor) ))
		{
			throw new Exception('Tried to read inaccessible property: '.$name.' from '.get_called_class());
		}

		return isset($this->$name) ? $this->$name : null;
	}

	public function __set($name, $value)
	{
		if ( ! ( in_array($name, static::$attr_writer) or in_array($name, static::$attr_accessor) ))
		{
			throw new Exception('Tried to write inaccessible property: '.$name.' from '.get_called_class());
		}

		if (method_exists($this, 'set_'.$name))
		{
			call_user_func(array($this, 'set_'.$name), $value);
		}
		else
		{
			$this->$name =& $value;
		}
	}

	protected $visible = true;

	public function visible($value = null)
	{
		if ( ! is_null($value))
		{
			$this->visible = $value;
			return $this;
		}

		if (is_callable($this->visible))
		{
			$func = $this->visible;
			return $func();
		}

		return $this->visible;
	}

	public function __toString() {
		$upload_dir = wp_upload_dir();
		$fname = get_class($this)."_".rand(100,999).".".static::$string_mode;
		$fh = fopen($upload_dir['path']."/".$fname, w);
		$out = new ReflectionClass($this);
		$props = $out->getProperties();
		$row = array();
		foreach ($props as $prop) {
			$row[$prop->getName()] = $prop->getValue($this);
		}
		switch (static::$string_mode) {
			case "csv":
			default:
				if(static::$showheader){
					fputcsv($fh, array_keys($row), $delimiter);
					static::$showheader=false;
				}
				fputcsv($fh, $row, static::$csvDelimiter);
				echo $upload_dir['url'] . "/" . $fname;
			break;
		}
	}
}
