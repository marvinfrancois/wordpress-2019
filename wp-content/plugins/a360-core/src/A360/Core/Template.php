<?php
namespace A360\Core;

class Template extends Base
{
    /**
     * Helper method to locate a template by path array
     * 
     * @param type $template_names
     * @param type $paths
     * @param type $load
     * @param type $require_once
     * @return string
     */
    public static function locate($template_names, $paths = array(), $load = false, $require_once = true, $vars = array())
    {
        is_array($paths) or $paths = (array)$paths;
        array_unshift($paths, STYLESHEETPATH, TEMPLATEPATH);
        
        $located = '';
        foreach ( (array) $template_names as $template_name ) {
            
            if ( !$template_name )
                continue;
            
            foreach ($paths as $path) {
                if ( file_exists($path . '/' . $template_name)) {
                    $located = $path . '/' . $template_name;
                    break(2);
                }
            }
        }

        if ( $load && '' != $located )
            static::load_template( $located, $require_once, $vars );

        return $located;
    }
    
    public static function load( $_template_file, $require_once = true, $vars = array() )
    {
        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

        if ( is_array( $wp_query->query_vars ) )
            extract( $wp_query->query_vars, EXTR_SKIP );
        
        if ( is_array($vars) )
            extract( $vars, EXTR_SKIP );

        if ( $require_once )
            require_once( $_template_file );
        else
            require( $_template_file );
    }

}