<?php
namespace A360\Core;

abstract class Widget extends \WP_Widget
{
	public static function register()
	{
		$class = get_called_class();
		add_action('widgets_init', function () use ($class) {
			register_widget($class);
		});
	}
	
	public function __construct($id_base = null, $name = null, $widget_options = array(), $control_options = array())
	{
		if (empty($id_base))
		{
			$id_base = preg_replace( '/(wp_)?widget_/', '', strtolower(str_replace('\\', '_', get_class($this))));
		}
		
		if (empty($name) and defined('static::NAME'))
		{
			$name = static::NAME;
		}
        
//        dd($id_base, $name, $widget_options, $control_options);
		
		parent::__construct($id_base, $name, $widget_options, $control_options);
	}
}