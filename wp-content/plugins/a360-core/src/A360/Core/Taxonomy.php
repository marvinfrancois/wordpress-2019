<?php

namespace A360\Core;

class Taxonomy extends Base
{
	protected static $attr_reader = array('id');
	protected static $instances = array();

	protected static function default_labels($labels = array())
	{
		$singular	= 'Tag';
		$plural		= 'Tags';

		is_string($labels) and $labels = array('singular_name' => $labels);
		if (isset($labels['singular_name']))
		{
			$singular	= $labels['singular_name'];
			$plural		= isset($labels['name']) ? $labels['name'] : Util\Inflector::pluralize($singular);
		}
		elseif ( ! is_array($labels) or ! isset($labels['name']))
		{
			// Use WordPress defaults
			return array();
		}

		$lower = strtolower($plural);

		return array_merge(array(
			'name'							=> _x( $plural, 'taxonomy general name' ),
			'singular_name'					=> _x( $singular, 'taxonomy singular name' ),
			'search_items'					=> __( 'Search '.$plural ),
			'all_items'						=> __( 'All '.$plural ),
			'parent_item'					=> null,
			'parent_item_colon'				=> null,
			'edit_item'						=> __( 'Edit '.$singular ),
			'update_item'					=> __( 'Update '.$singular ),
			'add_new_item'					=> __( 'Add New '.$singular ),
			'new_item_name'					=> __( 'New '.$singular.' Name' ),
			'separate_items_with_commas'	=> __( 'Separate '.$lower.' with commas' ),
			'add_or_remove_items'			=> __( 'Add or remove '.$lower.'' ),
			'choose_from_most_used'			=> __( 'Choose from the most used '.$lower.'' ),
			'not_found'						=> __( 'No '.$lower.' found.' ),
			'menu_name'						=> __( $plural ),
		), $labels);
	}

	protected static function default_args($args = array())
	{
		return $args;
	}

	protected $id;
	protected $types	= array();
	protected $labels	= array();
	protected $args		= array();

	public function __construct($id, $types, $labels = array(), $args = array())
	{
		if (isset(static::$instances[$id]))
		{
			throw new Exception('Taxonomy "'.$id.'" already defined');
		}
		elseif (in_array($id, array('post', 'page', 'attachment', 'revision', 'nav_menu_item', 'action')))
		{
			throw new Exception('Cannot use reserved post type: '.$id);
		}

		$this->id		= $id;
		$this->types	= is_array($types) ? $types : array($types);
		$this->labels	= static::default_labels($labels);
		$this->args		= static::default_args($args);

		static::$instances[$id] =& $this;

		add_action('init', array($this, 'register'));
	}

	public function register()
	{
		$args = $this->args;
		empty($args['labels']) and $args['labels'] = $this->labels;

		register_taxonomy($this->id, $this->types, $args);
	}

}