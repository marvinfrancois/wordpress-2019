
define('a360', ['module', 'jquery', 'backbone'], function(module, $, Backbone) {
	
	/**
	 * Autocompletion helper class
	 */
	var autoCompleter = function() {

		// Declare variables
		var self	= this,
			xhr		= null,
			timer	= null,
			input	= null;

		// Results function
		self.searchCallback = function(response) {
			self.results = new Backbone.Collection(response);
			self.view.render();
		};

		// Search function
		self.search = function(args, callback) {

			if (xhr)
			{
				xhr.abort();
			}

			clearTimeout(timer);

			timer = setTimeout(function() {
				
				args = $.extend({
					action: 'a360_autocomplete',
					search: input ? input.val() : ''
				}, args);
				
				xhr = $.ajax({
					url: module.config().ajax_url,
					data: args,
					cache: false,
					success: callback || self.searchCallback,
					dataType: 'json'
				});
			}, 200);
		};

		// Results
		self.results = new Backbone.Collection([]);

		// Results view
		self.view = new (Backbone.View.extend({
			container: null,
			render: function() {

				var list;
				if ( ! this.container)
				{
					this.container = $('<div style="display:none;position:absolute;height:160px;border:1px solid #aaa;background:#fff;z-index:2;overflow:auto;padding:.5em 0;"></div>');
					list = $('<ul></ul>').appendTo(this.container);
					$('body').on('click', 'a.a360-autocomplete-option', function(e) {
						e.stopPropagation();
						self.select($(this).data('index'));
						return false;
					});
				}
				else
				{
					list = this.container.find('ul').empty();
					this.container.detach();
				}
				
				this.container.insertAfter(input);

				self.results.each(function(result, i) {
					var item = $('<li/>').appendTo(list);
					$('<a href="#" data-index="'+i+'" class="a360-autocomplete-option" style="display:block; padding:.1em .5em; ">'+result.get('display')+'</a>').appendTo(item);
				});

				this.container.width(input.outerWidth()).fadeIn();
			}
		}))();
		
		self.select = function(index) {

			var m = input.closest('.model'),
				d = m.data(),
				result = self.results.models[index];

			window[d.model].setValue(d.index, result.get('ID'), result.get('display'));
		};
		
		$('.a360-field-group :input.autocomplete').attr('autocomplete', 'off');
		
		self.bindInput = function(id, args) {
			$('#'+id).on({
				keydown: function() {
					input = $(this);
					self.search(args);
				},
				focus: function() {
					input = $(this);
					input.attr('autocomplete', 'off');
					input.parent().css('position', 'relative');
					if (self.view.container)
					{
						self.view.container.detach().insertAfter(input).fadeIn();
					}
				},
				blur: function() {
					if (self.view.container)
					{
						setTimeout(function() {
							self.view.container.fadeOut();
						}, 10);
					}
				}
				
			}, ':input.autocomplete');
		};
	};

	var exports, loaded = false;
	exports = function() {
		if (loaded) return;
		exports.autoCompleter = new autoCompleter();
		loaded = true;
	};
	
	return exports;
});