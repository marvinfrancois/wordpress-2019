define('media', ['jquery'], function($) {
    
    console.log('media module is deprecated');
    return function() { };
		
	var Chooser;

	$('body').on('click', '.a360-attachment .button', function(e) {
		e.stopPropagation();
		$(this).closest('.a360-attachment').remove();
		return false;
	});

	return function(el, options) {
		
		el.each(function() {

			var $t = $(this);

			options = $.extend({}, $t.data(), options || {});

			var button    = $t.find('input.button');
			var multiple  = $t.data('multiple') || options.multiple || false;
			var fieldname = $t.data('fieldname') || 'a360_media';

			button.click(function() {

				if ( ! Chooser)
				{
					// Initialize dialog
					Chooser = wp.media.frames.file_frame = wp.media({
						title : multiple ? 'Choose Images' : 'Choose Image',
						button : {
							text : multiple ? 'Add Image(s)' : 'Set Image'
						},
						multiple : multiple
					});

					// Post-selection callback
					Chooser.on('select', function() {

						if ( ! multiple)
						{
							$t.find('a360-attachment').remove();
						}

						Chooser.state().get('selection').forEach(function(s) {
							if (s.get('type') !== 'image')
							{
								alert('"'+s.get('title')+'" is not an image');
								return;
							}

							var url = s.get('url');
							var row = $('<div class="a360-attachment"></div>');
							row.append('<input type="hidden" name="'+fieldname+'" value="'+url+'"/>');
							row.append('<img src="'+url+'" alt="" style="vertical-align:top;max-width:240px;"/>');
							row.append('<a href="#" class="button" style="vertical-align:top; margin-left:10px;">Remove</a>');
							button.before(row);
						});	
					});
				}

				// Launch dialog
				Chooser.open();
			});
		});
	};
});