define('field/image', ['jquery'], function($) {

	return function(el, model) {
        
		var binding = function(index) {
                
                // Initialize dialog
                var Chooser = wp.media.frames.file_frame = wp.media({
                    title : model.multiple ? 'Choose Images' : 'Choose Image',
                    button : {
                        text : model.multiple ? 'Add Image(s)' : 'Set Image'
                    },
                    multiple : model.multiple
                });

                Chooser.open();

                // Post-selection callback
                Chooser.on('select', function() {

                    Chooser.state().get('selection').forEach(function(s) {
                        if (s.get('type') !== 'image')
                        {
                            alert('"'+s.get('title')+'" is not an image');
                            return;
                        }

                        if ( ! model.multiple)
                        {
                            model.data.removeAll();
                            model.data.push(s.get('url'));
                        }
                        else
                        {
                            model.data.splice(index, 1, s.get('url'));
                        }
                        
                    });	
                });

				// Launch dialog
				Chooser.open();
		};
        
        return binding;
	};
});