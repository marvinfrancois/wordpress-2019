
define('field/address', ['jquery', 'knockout'], function($, ko) {
	
	return function(el, model, data, defaultData) {

		el = $(el);
		
		var setupModelData = function(a) {
			a.country = ko.observable(a.country);
			a.domestic = ko.computed(function() {
				return a.country() === 'United States';
			});
			a.getCityLabel = function() {
				return a.domestic() ? 'City' : 'Locality';
			};
			a.getStateLabel = function() {
				return a.domestic() ? 'State' : 'Region';
			};
			a.getZipLabel = function() {
				return a.domestic() ? 'Zip Code' : 'Post Code';
			};
			return a;
		};
		
		model.data = ko.observableArray(data);
		model.addField = function() {
			var data = $.extend({}, defaultData);
			setupModelData(data);
			model.data.push(data);
		};
		model.removeField = function(index) {
			model.data.splice(index(), 1);
			if (model.data().length === 0)
			{
				model.addField();
			}
		};
		model.selectValue = function(select, value, index) {
			var s = $(select).val(value);
			if (s.data('bind').indexOf('[country]') > -1)
			{
				model.setCountry(value, index);
			}
		};
		model.domestic = ko.observable();
		model.setCountry = function(value, index) {
			model.data()[index].country(value);
		};
		
		for (var i=0; i<model.data().length; i++)
		{
			setupModelData(model.data()[i]);
		}
		
		ko.applyBindings(model, el[0]);

	};
});