module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		requirejs: {
			compile: {
				options: {
					name: "main",
					baseUrl: "assets/js",
					mainConfigFile: "assets/js/main.js",
					out: "assets/js/compiled.js",
					preserveLicenseComments: false
				}
			}
		},
		bower: {
			target: {
				rjsConfig: "assets/js/main.js"
			}
		},
		uglify: {
			dist: {
				files: {
					"assets/js/require.min.js": "assets/js/components/requirejs/require.js"
				}
			}
		},
		cssmin: {
			combine: {
				files: {
					"assets/css/style.min.css": [
						"assets/js/components/pickadate/lib/themes/default.css",
						"assets/js/components/pickadate/lib/themes/default.date.css",
						"assets/js/components/pickadate/lib/themes/default.time.css"
					]
				}
			}
		}
	});

	grunt.loadNpmTasks("grunt-bower-requirejs");
	grunt.loadNpmTasks("grunt-contrib-requirejs");
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	
	grunt.registerTask("default", ["bower","cssmin","uglify","requirejs"]);

	grunt.registerTask("install", "install frontend dependencies", function() {
		var exec = require("child_process").exec;
		var cb = this.async();
		exec("bower install", {}, function(err, stdout, stderr) {
			console.log(stdout);
			cb();
		});
	});
	
	grunt.registerTask("update", "update frontend dependencies", function() {
		var exec = require("child_process").exec;
		var cb = this.async();
		exec("bower update", {}, function(err, stdout, stderr) {
			console.log(stdout);
			cb();
		});
	});

};
