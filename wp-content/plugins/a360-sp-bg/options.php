<?php
namespace A360\spbg;

class spbgSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Buyers Guide Silverpop Settings',
            'BG Silverpop Settings',
            'manage_options',
            'spbg-setting-admin',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'a360_spbg_options' );
		$handler = "options.php";
		echo<<<EOT
        <div class="wrap">
            <h2>Silverpop Settings</h2>
            <form method="post" action="$handler" enctype="multipart/form-data">
EOT;
		// This prints out all hidden setting fields
		settings_fields( 'a360_spbg' );
		do_settings_sections( 'spbg-setting-admin' );
		submit_button();
		echo<<<EOT
            </form>
        </div>
EOT;
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'a360_spbg', // Option group
            'a360_spbg_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'a360_spbg_auth', // ID
            'Silverpop Authentication settings', // Title
            array( $this, 'print_auth_info' ), // Callback
            'spbg-setting-admin' // Page
        );

        add_settings_section(
        	'a360_spbg_db',
        	'Silverpop Table settings',
        	array($this, 'print_table_info'),
        	'spbg-setting-admin'
        );

        add_settings_field(
            'a360_spbg_userid',
            'Silverpop user ID',
            array( $this, 'userid_callback' ),
            'spbg-setting-admin',
            'a360_spbg_auth'
        );

        add_settings_field(
        	'a360_spbg_password',
        	'Silverpop Password',
        	array($this, 'password_callback'),
        	'spbg-setting-admin',
        	'a360_spbg_auth'
        );

        add_settings_field(
        	'a360_spbg_engage',
        	'Select Engage server',
        	array($this, 'engage_callback'),
        	'spbg-setting-admin',
        	'a360_spbg_db'
        );
/*
        add_settings_field(
        	'a360_spbg_sync',
        	'Keep categories synced with Silverpop',
        	array($this, 'sync_callback'),
        	'spbg-setting-admin',
        	'a360_spbg_db'
        );
*/
//        add_settings_field(
//        	'a360_spbg_folder',
//        	'Select Folder',
//        	array($this, 'folderlist_callback'),
//        	'spbg-setting-admin',
//        	'a360_spbg_db'
//        );

     	add_settings_field(
     		'a360_spbg_db',
     		'Select table',
     		array($this, 'dblist_callback'),
     		'spbg-setting-admin',
     		'a360_spbg_db'
     	);
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['a360_spbg_userid'] ) )
            $new_input['a360_spbg_userid'] = sanitize_text_field( $input['a360_spbg_userid'] );

        if( isset( $input['a360_spbg_password'] ) )
        	$new_input['a360_spbg_password'] = sanitize_text_field( $input['a360_spbg_password'] );

        if( isset( $input['a360_spbg_engage']))
        	$new_input['a360_spbg_engage'] = sanitize_text_field( $input['a360_spbg_engage']);

        if( isset( $input['a360_spbg_db']))
        	$new_input['a360_spbg_db'] = sanitize_text_field($input['a360_spbg_db']);

		if( isset( $input['a360_spbg_sync']))
			$new_input['a360_spbg_sync'] = sanitize_text_field($input['a360_spbg_sync']);

		return $new_input;

    }

    /**
     * Print the metaslider section text
     */
    public function print_auth_info()
    {
        echo 'Enter the authentication for Silverpop below:';
    }

    public function print_table_info() {
    	echo 'Select the Silverpop table to use below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function userid_callback()
    {
        printf(
            '<input type="text" id="a360_spbg_userid" name="a360_spbg_options[a360_spbg_userid]" value="%s" />',
            isset( $this->options['a360_spbg_userid'] ) ? esc_attr( $this->options['a360_spbg_userid']) : ''
        );
    }

    public function password_callback()
    {
        printf(
            '<input type="password" id="a360_spbg_password" name="a360_spbg_options[a360_spbg_password]" value="%s" />',
            isset( $this->options['a360_spbg_password'] ) ? esc_attr( $this->options['a360_spbg_password']) : ''
        );
    }

    public function engage_callback() {
    	$sel = array_fill(1, 5, "");
    	$sel[$this->options['a360_spbg_engage']] = "SELECTED";
    	echo '<select name="a360_spbg_options[a360_spbg_engage]">';
    	for ($i=1;$i<6;$i++) {
    		echo "<option value=$i $sel[$i]>Engage $i</option>";
    	}
    	echo "</select>";
    }

    public function dblist_callback() {
    	printf(
    		'<input type="text" id="a360_spbg_db" name="a360_spbg_options[a360_spbg_db]" value="%s" />',
    		isset($this->options['a360_spbg_db']) ? esc_attr($this->options['a360_spbg_db']) : ''
    	);
    }

    public function sync_callback() {
    	$checked = (isset($this->options['a360_spbg_sync']) && $this->options['a360_spbg_sync'] == "active") ? "checked" : "";
    	echo<<<EOT
    		<input type="checkbox" id="a360_spbg_sync" name="a360_spbg_options[a360_spbg_sync]" value="active" $checked />
EOT;
    }
}

if( is_admin() )
    $spbgSettings = new spbgSettings();
