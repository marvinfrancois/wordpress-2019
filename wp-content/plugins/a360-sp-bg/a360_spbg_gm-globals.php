<?php
session_start();
//if (session_status() == PHP_SESSION_NONE) {
//	session_start();
//}

$spbg_un = $spbg_pw = $spbg_server = $spbg_wsdl = $spbg_SoapClient = $spbg_location = false;
$spbg_options = array();
$spbg_options = get_option('a360_spbg_options');
$spbg_un = $spbg_options['a360_spbg_userid'];
$spbg_pw = $spbg_options['a360_spbg_password'];
$spbg_server = $spbg_options['a360_spbg_engage'];
$spbg_db = $spbg_options['a360_spbg_db'];
$spbg_sync = $spbg_options['a360_spbg_sync'];
$spbg_cols = false;
$spbg_options = array(
		"exceptions" => true,
		"trace" => true,
		"keep_alive" => true,
		"features" => SOAP_SINGLE_ELEMENT_ARRAYS,
		"compression" => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 9
);
$spbg_wsdl = false;
$spbg_SoapClient = false;

add_action('admin_init', 'spbg_connect');

function spbg_connect() {
	global $spbg_wsdl, $spbg_options, $spbg_SoapClient, $spbg_un, $spbg_pw, $spbg_server, $spbg_dblist;
	if (is_plugin_active("a360-sp-bg/a360-sp-bg.php") && isset($spbg_server)) {
		if (isset($spbg_server)) {
			$spbg_wsdl = "http://api{$spbg_server}.silverpop.com/SoapApi?wsdl";
			$spbg_SoapClient = new \A360\spbg\spbg_base( $spbg_wsdl, $spbg_options);
			if (isset($spbg_un) && isset($spbg_pw) && isset($spbg_server) && isset($spbg_db) && \A360\spbg\spbg_login()) {
				$spbg_cols = \A360\spbg\spbg_get_list_meta_data($spbg_db);
			}
		}
	}
}