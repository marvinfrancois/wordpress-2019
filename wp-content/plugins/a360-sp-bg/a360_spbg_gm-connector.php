<?php
namespace A360\spbg;
//add_action('spbg_init', 'spbg_login');

function spbg_set_session_hdr() {
	global $spbg_SoapClient;
	$soaphdr = new \SoapHeader('SilverpopApi:EngageService.SessionHeader', 'sessionHeader', array("sessionid"=>$_SESSION["spbg_jsessionid"]), true);
	$spbg_SoapClient->__setSoapHeaders(NULL);
	$spbg_SoapClient->__setSoapHeaders($soaphdr);
}

function spbg_login(){
	global $spbg_SoapClient, $spbg_un, $spbg_pw;
	session_start();
	if (isset($_SESSION['spbg_jsessionid']) && $_SESSION['spbg_jsessionid']) {
		//
	} else {
		if ($spbg_SoapClient) {
			$spbg_SoapClient->__setSoapHeaders(null);
			$response = $spbg_SoapClient->Login(array('USERNAME' => $spbg_un, 'PASSWORD' => $spbg_pw));
			$_SESSION['spbg_jsessionid'] = $response->SUCCESS ? $response->SESSIONID : false;
		}
	}
	return $_SESSION['spbg_jsessionid'];
}

function spbg_logout() {
	global $spbg_SoapClient;
	spbg_set_session_hdr();
	$response = $spbg_SoapClient->Logout();
	$_SESSION['spbg_jsessionid'] = $response->SUCCESS ? false : $_SESSION['spbg_jsessionid'];
	return $response->SUCCESS;
}

function spbg_add_recipient($list_id, $created_from = 1, $send_autoreply = false, $update_if_found = false, $allow_html = false, $visitor_key = false, $sync_fields = array(), $column = array()){
	global $spbg_SoapClient;
	$parms = array(
			"LIST_ID" => $list_id,
			"CREATED_FROM" => $created_from
	);
	if ($send_autoreply){
		$parms["SEND_AUTOREPLY"] = $send_autoreply;
	}
	if ($update_if_found){
		$parms["UPDATE_IF_FOUND"] = $update_if_found;
	}
	if ($allow_html){
		$parms["ALLOW_HTML"] = $allow_html;
	}
	if ($visitor_key){
		$parms["VISITOR_KEY"] = $visitor_key;
	}
	if ($sync_fields){
		$parms["SYNC_FIELDS"] = $sync_fields;
	}
	if ($column){
		$parms["COLUMN"] = $column;
	}
	$response = $spbg_SoapClient->AddRecipient($parms);
	return $response->SUCCESS ? $response->RecipientId : false;
}

function spbg_double_opt_in_recipient($list_id, $send_autoreply = false, $allow_html = false, $column = array()){
	global $spbg_SoapClient;
	$parms = array(
			"LIST_ID" => $list_id
	);
	if ($send_autoreply){
		$parms["SEND_AUTOREPLY"] = $send_autoreply;
	}
	if ($allow_html){
		$parms["ALLOW_HTML"] = $send_autoreply;
	}
	if ($column){
		$parms["COLUMN"] = $column;
	}

	$response = $spbg_SoapClient->DoubleOptinRecipient($parms);
	return $response->SUCCESS ? $response->RecipientId : false;
}

function spbg_update_recipient($list_id, $old_email = false, $recipient_id = false, $send_autoreply = false, $allow_html = false, $visitor_key = false, $sync_fields = false){
	global $spbg_SoapClient;
	$parms = array(
			"LIST_ID" => $list_id
	);
	if ($old_email){
		$parms["OLD_EMAIL"] = $old_email;
	}
	if ($recipient_id){
		$parms["RECIPIENT_ID"] = $recipient_id;
	}
	if ($send_autoreply){
		$parms["SEND_AUTOREPLY"] = $send_autoreply;
	}
	if ($allow_html){
		$parms["ALLOW_HTML"] = $allow_html;
	}
	if ($visitor_key){
		$parms["VISITOR_KEY"] = $visitor_key;
	}
	if ($sync_fields){
		$parms["SYNC_FIELDS"] = $sync_fields;
	}

	$response = $spbg_SoapClient->UpdateRecipient($parms);
	return $response->SUCCESS ? $response->RecipientId : false;
}

function spbg_opt_out_recipient($list_id, $email, $mailing_id, $recipient_id, $job_id, $column){
	global $spbg_SoapClient;
	$parms = array(
			"LIST_ID" => $list_id,
			"EMAIL" => $email,
			"MAILING_ID" => $mailing_id,
			"RECIPIENT_ID" => $recipient_id,
			"JOB_ID" => $job_id,
			"COLUMN" => $column
	);
}

function spbg_select_recipient_data($list_id, $email, $recipient_id = false, $encoded_recipient_id = false, $return_contact_lists = false, $column = false){
	global $spbg_SoapClient;
	$parms = array(
			"LIST_ID" => $list_id,
			"EMAIL" => $email
	);
	if ($recipient_id){
		$parms["RECIPIENT_ID"] = $recipient_id;
	}
	if ($encoded_recipient_id){
		$parms["ENCODED_RECIPIENT_ID"] = $encoded_recipient_id;
	}
	if ($return_contact_lists){
		$parms["RETURN_CONTACT_LISTS"] = $return_contact_lists;
	}
	if ($column){
		$parms["COLUMN"] = $column;
	}

	$response = $spbg_SoapClient->SelectRecipientData($parms);
	return $response->SUCCESS ? $response : false;
}

function spbg_get_lists($visibility = 1, $list_type = 2, $folder_id = false, $include_all_lists = true, $include_tags = true){
	global $spbg_SoapClient;
	spbg_set_session_hdr();
	$parms = array(
			"VISIBILITY" => $visibility,
			"LIST_TYPE" => $list_type
	);
	if ($include_all_lists){
		$parms["INCLUDE_ALL_LISTS"] = true;
	} else {
		if ($folder_id) {
			$parms["FOLDER_ID"] = $folder_id;
		}
	}
	if ($include_tags){
		$parms["INCLUDE_TAGS"] = true;
	}
	$response = $spbg_SoapClient->GetLists($parms);
	return $response->SUCCESS ? $response : false;
}

function spbg_get_list_meta_data($list_id){
	global $spbg_SoapClient;
	spbg_set_session_hdr();
	$parms = array(
			"LIST_ID" => $list_id
	);
	$response = $spbg_SoapClient->GetListMetaData($parms);
	return $response->SUCCESS ? $response : false;
}

function spbg_add_list_select_column($list_id, $column_name, $values, $default = null) {
	global $spbg_SoapClient;
	spbg_set_session_hdr();
	$selvalues = array();
	$parms = array(
		"LIST_ID" => $list_id,
		"COLUMN_NAME" => 'zzct ' . str_replace(array(',','/','&amp;','&','-'), array(' ',' ','and','and',' '), $column_name),
		"COLUMN_TYPE" => 20,
		"DEFAULT" => $default,
		"SELECTION_VALUES" => array("VALUE" => array())
	);
	foreach($values as $value) {
		$parms["SELECTION_VALUES"]["VALUE"][] = str_replace(array(',','/','&amp;','&','-'), array(' ',' ','and','and',' '), $value);
//		$selvalues[] = array("VALUE" => $value);
	}
	$response = $spbg_SoapClient->AddListColumn($parms);
	return $response->SUCCESS ? $response : false;
}




