<?php
namespace A360\spbg;

class spbg_base extends \SoapClient {
	public function __call($func, $args) {
		$retry = true;
		$count = 0;
		$max = 2;
		while($retry && $count < $max) {
			try {
				$response = $this->processResponse(parent::__call($func, $args));
				$retry = false;
			} catch(spSessionFault $e) {
				$_SESSION['spbg_jsessionid'] = false;
				if(\A360\spbg\spbg_login()) {
					\A360\spbg\spbg_set_session_hdr();
				} else {
					throw new \A360\spbg\spLoginFault($e->getMessage(), $e->getCode());
				}
			} catch(SoapFault $e) {
				throw $e;
			}
			$count++;
		}
		return $response;
	}

	private function processResponse($response) {
		if ($response && isset($response->Fault)) {
			$message = $response->Fault->detail->error->errorid;
			$code = $response->Fault->FaultString;
			$soapFault = null;
			switch($response->Fault->detail->error->errorid) {
				case 145:
					$soapFault = new \A360\spbg\spSessionFault($message, $code);
					break;
				case 201:
					$response->SUCCESS = true;
					break;
				default:
					$soapFault = new \SoapFault($message, $code);
					break;
			}
		}
		if ($soapFault) {
			throw $soapFault;
		} else {
			return $response;
		}
	}
}

class spSessionFault extends \SoapFault {}

class spLoginFault extends \SoapFault {}
