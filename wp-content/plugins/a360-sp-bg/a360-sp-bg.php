<?php
/*
Plugin Name: A360 Silverpop Buyers Guide
Plugin URI: http://www.whatsyouranthem.com
Description: Provides core class and helpers as well as initalize composer autoloader.
Version: 0.1
Author: Chris Caviness
Author URI: http://www.whatsyouranthem.com
*/

namespace A360\spbg;

$state = false;
//return;
require_once(__DIR__ . "/a360_spbg_inc.php");

register_activation_hook(__FILE__, '\A360\spbg\activate');
register_deactivation_hook(__FILE__, '\A360\spbg\deactivate');
register_uninstall_hook(__FILE__, '\A360\spbg\uninstall');

function activate() {}

function deactivate() {}

function uninstall() {}

function posts_results($posts) {
	return $posts;
}

function get_sp_bg() {}

function update() {
	add_action('wp_head', '\A360\spbg\enableSilverpopCORS');
	$data = (object) $_REQUEST;
	// objects are passed by reference
	do_action('spbg_pre_update', $data);
	do_action('spbg_update', $data);
	do_action('spbg_post_update', $data);
}

function enableSilverpopCORS() {
	echo "Access-Control-Allow-Origin: http://anthemmedia.mkt5097.com";
}

function spbg_pre_update($data){
	global $spbg_db, $spbg_cols;
	//  modify any request vars in data here

	//  perform any pre update processing
	//  setup silverpop column map
	if ($spbg_db) {
		$spbg_cols = $spbg_cols ?: \A360\spbg\get_list_meta_data($spbg_db);
		$columns = array_fill(0,1,null);
		foreach($response->COLUMNS as $key=>$val) {
			if (strstr($val->NAME, 'cat ')) {
				$catid = 0;
				$catname = null;
				$catnice = null;
				$catdesc = null;
				$catparent = null;
				$tax = 'category';
				$cat = array(
					'cat_ID' => &$catid,
					'cat_name' => &$catname,
					'category_description' => &$catesc,
					'category_nicename' => &$catnice,
					'category_parent' => &$catparent,
					'taxonomy' => &$tax
				);
				$catname = ucwords(array_pop($explode('zzct ', strtolower($val->NAME))));
				$catnice = str_replace(
					array('&','/',' ','-'),
					array('_and','_','_','__'),
					$catname
				);
				$catid = get_cat_ID($nicename);
				$catparent = wp_update_term($cat);
				foreach($val->SELECTION_VALUES as $tkey=>$tval) {
					$catname = ucwords(strtolower($tval));
					$catnice = str_replace(
						array('&','/',' ','-'),
						array('_and','_','_','__'),
						$catname
					);
					wp_update_term($cat);
				}
			}
			$column = array(
				"name" => $val->NAME,
				"value" => null
			);
			if (isset($val->DEFAULT_VALUE)) {
				$column['default'] = $val->DEFAULT_VALUE;
				$column['value'] = $val->DEFAULT_VALUE;
			}
			$columns[] = $column;
		}
		$newdata = array();
		foreach($data as $key=>$val) {
			if(stristr($key, 'COLUMN')) {
				$colNum = array_pop(explode('COLUMN', $key));
				$colname = $columns[$colNum]['name'];
			} else {
				$colname = $key;
				foreach($columns as $k=>$v) {
					if ($v['name'] == $colname) {
						$colNum = $k;
					}
				}
			}
			$data->$colname = $val ?: (array_key_exists("default", $columns[$colNum]) ? $columns[$colNum]['default'] : null);
		}
	}
	return $data;
}

function spbg_update($data) {
	//  perform the update
}

function spbg_post_update($data) {
	// post update processing
}

add_filter('spbg_pre_update', '\A360\spbg\spbg_pre_update');
add_action('spbg_update', '\A360\spbg\spbg_update');
add_action('spbg_post_update', '\A360\spbg\spbg_post_update');
add_action('posts_results', '\A360\spbg\posts_results');
add_action('wp_ajax_nopriv_update_spbg', '\A360\spbg\update');


function init() {}

function get_short_sitename_from_url($url= false) {
	$shortnames = array(
		"24x7mag" => "24x7",
		"clpmag" => "CLP",
		"hearingreview" => "HR",
		"imagingeconomics" => "AXIS",
		"axisimagingnews" => "AXIS",
		"orthodonticproductsonline" => "OP",
		"plasticsurgerypractice" => "PSP",
		"ptproductsonline" => "PTP",
		"rehabpub" => "RM",
		"rtmagazine" => "RT",
		"sleepreviewmag" => "SR"
	);
	$url = ($url) ?: $_SERVER['SERVER_NAME'];
	$url = explode('.', $url);
	foreach ($url as $key=>$val) {
		if (array_key_exists(strtolower($val), $shortnames)) {
			return $shortnames[strtolower($val)];
		}
	}
}