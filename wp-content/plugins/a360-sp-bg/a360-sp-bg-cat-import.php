<?php
namespace A360\spbg;

//add_action('create_directory', '\A360\spbg\update_sp_term');
//add_action('edit_directory', '\A360\spbg\update_sp_term');
add_action('op_category_import', '\A360\spbg\update_sp_term');

function update_sp_term($term_id = null, int $tt_id = null) {
	global $spbg_db, $spbg_sync;
//	if ($spbg_sync == 'active') {
		$childless = array();
//		$childless = get_terms('directory', array(
//			'parent' => 0,
//			'childless' => 1,
//			'fields' => 'names',
//			'hide_empty' => 0
//		));
//		\A360\spbg\spbg_add_list_select_column($spbg_db, 'general', $childless);

		$parent_terms = get_terms('directory', array(
			'parent' => 0,
//			'childless' => 0,
			'hide_empty' => 0
		));

		foreach($parent_terms as $column) {
			$values = get_terms('directory', array(
				'child_of' => $column->term_id,
				'fields' => 'names'
			));
			if ($values) {
				\A360\spbg\spbg_add_list_select_column($spbg_db, $column->name, $values);
			} else {
				$childless[] = $column->name;
			}
		}
		\A360\spbg\spbg_add_list_select_column($spbg_db, 'general', $childless);
//	}
}