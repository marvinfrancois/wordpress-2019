<?php
//force git update
$true = true;

class TorchSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
        add_filter('upload_mimes', array($this, 'custom_upload_xml'));
    }

    /**
     * Add xml to allowable upload types only for torch
     * @param unknown $mimes
     * @return multitype:
     */
    function custom_upload_xml($mimes) {
    	if (isset($_FILES) && isset($_FILES['a360_torch_xml'])) {
	    	$mimes = array_merge($mimes, array('xml' => 'text/xml'));
    	}
    	return $mimes;
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Torch Settings',
            'Torch Settings',
            'manage_options',
            'torch-setting-admin',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'a360_torch_options' );
		$handler = "options.php";
		echo<<<EOT
        <div class="wrap">
            <h2>Torch Settings</h2>
            <form method="post" action="$handler" enctype="multipart/form-data">
EOT;
		// This prints out all hidden setting fields
		settings_fields( 'a360_torch' );
		do_settings_sections( 'torch-setting-admin' );
		submit_button();
		echo<<<EOT
            </form>
        </div>
EOT;
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'a360_torch', // Option group
            'a360_torch_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'a360_torch_metaslider', // ID
            'MetaSlider settings', // Title
            array( $this, 'print_msid_info' ), // Callback
            'torch-setting-admin' // Page
        );

        add_settings_section(
        	'a360_torch_uploads',
        	'Data settings',
        	array($this, 'print_XML_info'),
        	'torch-setting-admin'
        );

        add_settings_field(
            'a360_torch_msid',
            'MetaSlider ID',
            array( $this, 'msid_callback' ),
            'torch-setting-admin',
            'a360_torch_metaslider'
        );

        add_settings_field(
        	'a360_torch_xml',
            'Upload XML file',
            array( $this, 'xml_callback' ),
            'torch-setting-admin',
			'a360_torch_uploads'
        );

        add_settings_field(
        	'a360_torch_active',
        	'Activate torch widget',
        	array( $this, 'torch_active_callback'),
        	'torch-setting-admin',
        	'a360_torch_metaslider'
        );

        add_settings_field(
        	'a360_torch_display_index',
        	'Set display index position',
        	array( $this, 'torch_display_index_callback'),
        	'torch-setting-admin',
        	'a360_torch_metaslider'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['a360_torch_msid'] ) )
            $new_input['a360_torch_msid'] = sanitize_text_field( $input['a360_torch_msid'] );

        $keys = array_keys($_FILES);
        $i = 0;
        foreach ( $_FILES as $xmlfile) {
			if ($xmlfile['size']) {
				if ($xmlfile['type'] == 'text/xml') {
					$override = array('test_form' => false, 'action' => 'wp_handle_upload');
					if ($file = wp_handle_upload( $xmlfile, $override )) {
					} else {
						wp_die($file['error']);
					}
				} else {
					wp_die('No xml file was uploaded.');
				}
			}
		}
        if( isset( $input['a360_torch_xml'] ) )
        	$new_input['a360_torch_xml'] = sanitize_text_field( $input['a360_torch_xml'] );
        if( isset( $input['a360_torch_active'] ) )
        	$new_input['a360_torch_active'] = sanitize_text_field( $input['a360_torch_active']);
        if( isset( $input['a360_torch_display_index']))
        	$new_input['a360_torch_display_index'] = sanitize_text_field( $input['a360_torch_display_index']);
        return $new_input;
    }

    /**
     * Print the metaslider section text
     */
    public function print_msid_info()
    {
        print 'Enter the metaslider id for your widget below:';
    }

    /**
     * Print the Upload section text
     */
    public function print_XML_info() {
		print 'Use this section to import Torch data files:';
	}

    /**
     * Get the settings option array and print one of its values
     */
    public function msid_callback()
    {
        printf(
            '<input type="text" id="a360_torch_msid" name="a360_torch_options[a360_torch_msid]" value="%s" />',
            isset( $this->options['a360_torch_msid'] ) ? esc_attr( $this->options['a360_torch_msid']) : ''
        );
    }

    public function xml_callback()
    {
        echo<<<EOT
            <input type="file" id="a360_torch_xml" name="a360_torch_xml"/>
EOT;
    }

    public function torch_active_callback()
    {
    	$checked = (isset($this->options['a360_torch_active']) && $this->options['a360_torch_active'] == "active") ? "checked" : "";
    	echo<<<EOT
    	<input type="checkbox" id="a360_torch_active" name="a360_torch_options[a360_torch_active]" value="active" $checked />
EOT;
    }

    public function torch_display_index_callback() {
    	printf(
	    	'<input type="number" id="a360_torch_display_index" name="a360_torch_options[a360_torch_display_index]" value="%s" />',
    		isset( $this->options['a360_torch_display_index']) ? esc_attr( $this->options['a360_torch_display_index']) : false
    	);
    }
}

if( is_admin() )
    $torch_settings = new TorchSettings();
