<?php
/*
 Plugin Name: A360 Torch
Plugin URI: http://www.whatsyouranthem.com
Description: Provides core class and helpers as well as initalize composer autoloader.
Version: 0.1
Author: Chris Caviness
Author URI: http://www.whatsyouranthem.com
*/

namespace A360\torch;

use FilesystemIterator;

$state = false;

register_activation_hook(__FILE__, '\A360\torch\activate');
register_deactivation_hook(__FILE__, '\A360\torch\deactivate');
register_uninstall_hook(__FILE__, '\A360\torch\uninstall');

function activate() {

}

function deactivate() {

}

function uninstall() {

}

add_action('init', '\A360\torch\init');
add_action('admin_init', '\A360\torch\check_uploaded');
add_filter('upload_dir', function($updir) use (&$state) {
	return \A360\torch\upload_dir($updir, $state);
});
add_filter('wp_handle_upload_prefilter', '\A360\torch\custom_upload_filter' );
require_once(__DIR__ . "/options.php");

function init() {
}

function upload_dir($updir, $state = false) {
	if (isset($_FILES) || $state) {
		if (isset($_FILES['a360_torch_xml']) || $state == 'xml') {
			$torchdir = "/torch/xml/";
			$torchupload = true;
		}
		if (isset($_FILES['torch_upload_image'])|| $state == 'img') {
			$torchdir = "/torch/img/";
			$torchupload = true;
		}
		if ($torchupload) {
			$updir['path'] = $updir['basedir'] . $torchdir;
			$updir['url'] = $updir['baseurl'] . $torchdir;
		}
	}
	return $updir;
}

function custom_upload_filter( $file ){
	if (isset($_FILES['a360_torch_xml']) && isset($file['name'])) {
		$file['name'] = "0.xml";
		$updir = wp_upload_dir();
		if ($files = glob($updir['path'] . "*.xml")) {
			array_walk($files, function(&$val, $key, $ext){
				$val = basename($val, $ext);
			}, ".xml");
			rsort($files, SORT_NUMERIC);
			$file['name'] = ++$files[0] . ".xml";
		}
	}
	return $file;
}

function check_uploaded() {
	if (is_admin()) {
		if (isset($_FILES)) {
			if (isset($_FILES['a360_torch_xml']) && $_FILES['a360_torch_xml']['size'] > 0) {
				do_action('a360_torch_xml', $_FILES);
			}
		}
	}
}

function torch_upload_xml($files) {
}

function torch_display_data_by_day()
{
	if(!$xml = get_last_xml_file()){
		//throw some error
	}

	//Send Back the Last Day
	return  $xml->day[$xml->count()-1];
}

/** torch_display_data_by_site
 * @Author - Matthew Moore
 * @Description - Returns a SimpleXMLElement with the site specific data. Expected XML file input below.
 *  <Sites>
 *      <Site>
 *          <SiteName>SR</SiteName>
 *              <Winners>
 *                  .....
 *              </Winners>
 *              <Losers>
 *                  .....
 *              </Losers>
 *      </Site>
 *      <Site>
 *          .....
 *      </Site>
 * </Sites>
 *
 *
 * @return \SimpleXMLElement
 */
function torch_display_data_by_site()
{
	$sitename = get_short_sitename_from_url();

	if(!$xml = get_last_xml_file()) {
		//throw some error
	}

	//Only bring back the node with the site name we want
	$site_xml = $xml->xpath('//Site[SiteName[.="'. $sitename.'"]]');

	return $site_xml[0];
}

function get_last_xml_file($file = false)
{
	global $state;
	//Upload Directory
	$state = 'xml';
	$dir = wp_upload_dir();
	$state = false;

	if ($file) {
		//do nothing
	} else {
		if ($files = glob($dir['path'] . "*.xml")) {
			array_walk(
				$files,
				function(&$val, $key, $ext){
					$val = basename($val, $ext);
				},
				".xml"
			);
			rsort($files, SORT_NUMERIC);
			$file = $files[0] . ".xml";
		}
	}

	//All the Files Uploaded to that directory
//	$files = scandir($dir['path'],1);

	//Double check we have a file.
//	if(sizeOf($files) < 1) {
		//Throw Some Error
//	}
	return simplexml_load_string(preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', file_get_contents($dir['path'] . $file)));
}

function get_short_sitename_from_url($url= false) {

	$shortnames = array(
		"24x7mag" => "24x7",
		"clpmag" => "CLP",
		"hearingreview" => "HR",
		"imagingeconomics" => "AXIS",
		"axisimagingnews" => "AXIS",
		"orthodonticproductsonline" => "OP",
		"plasticsurgerypractice" => "PSP",
		"ptproductsonline" => "PTP",
		"rehabpub" => "RM",
		"rtmagazine" => "RT",
		"sleepreviewmag" => "SR"
	);
	$url = ($url) ?: $_SERVER['SERVER_NAME'];
	$url = explode('.', $url);
	foreach ($url as $key=>$val) {
		if (array_key_exists(strtolower($val), $shortnames)) {
			return $shortnames[strtolower($val)];
		}
	}
}

function get_torch_data($site = false) {
	$site = $site ?: \A360\torch\get_short_sitename_from_url();
	$torch_options = get_option('a360_torch_options');
	$msid = $torch_options['a360_torch_msid'];
	$xmldata = \A360\torch\get_last_xml_file();
	$torch_data = false;
	if ($xmldata) {
		$keyword = "$xmldata->keyword";
		$dow = ((date('w') + 4) % 7) + 1; // Sun - 0 thru Sat - 6 shifted to Wed - 1 thru Tue - 7
		$torch_data = array(
			'key' => $site,
			'keyword' => $keyword,
			'lose-title' => "Today's Market Laggers",
			'msid' => $msid,
			'data' => array()
		);
		$xmlday = $xmldata->xpath("//day[@dow=\"$dow\"]");
		$xmlday = $xmlday[0];
		$winners = array();
		$losers = array();
		foreach($xmlday->winners->item as $key=>$value) {
			$winners[] = array(
				'name'=>"$value->product",
				'rank'=>abs($value->movement),
				'url'=>	"$value->url"
			);
		}
		foreach($xmlday->losers->item as $key=>$value) {
			$losers[] = array(
					'name'=>"$value->product",
					'rank'=>abs($value->movement),
					'url'=>	"$value->url"
			);
		}
		usort($winners, '\A360\torch\torch_rev_sort');
		usort($losers, '\A360\torch\torch_sort');
		$torch_data['data'] = array_merge(array_slice($winners, 0, 5, true), array_slice($losers, 0, 5, true));
	}
	return $torch_data;
}

function torch_admin_options_html(){

}

function torch_sort($a, $b) {
	$a_move = $a['rank'];
	$b_move = $b['rank'];
	return ($a_move == $b_move) ? 0 : (($a_move < $b_move) ? -1 : 1);
}

function torch_rev_sort($a, $b) {
	$a_move = $a['rank'];
	$b_move = $b['rank'];
	return ($a_move == $b_move) ? 0: (($a_move < $b_move) ? 1 : -1);
}