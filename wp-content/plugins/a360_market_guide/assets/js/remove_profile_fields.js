jQuery(document).ready(function() {
    jQuery('input#nickname').parents('tr').hide();
    jQuery('select#display_name').parents('tr').hide();
    jQuery('input#url').parents('tr').hide();
    jQuery('textarea#description').parents('tr').hide();
    jQuery('input#comment_shortcuts').parents('tr').hide();
    jQuery('input#admin_bar_front').parents('tr').hide();
    jQuery('input#admin_color_classic').parents('tr').hide();
    jQuery('input#rich_editing').parents('tr').hide();
    jQuery('h3:contains("Personal Options")').hide();
});