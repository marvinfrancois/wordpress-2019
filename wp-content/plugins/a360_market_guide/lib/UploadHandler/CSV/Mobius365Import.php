<?php

namespace Guide\UploadHandler\CSV;

use Guide\UploadHandler\CSV;

//use A360\Visitor;

class Mobius365Import extends CSV {

    const MYSQL_DATEFORMAT = 'Y-m-d H:i:s';

    public function process() {

        global $wp;
        global $wpdb;


        //////////////////////////////////////////////////////

        $newListings = 0;
        $updatedListings = 0;

        $newListingCompanies = array();
        $updatedListingCompanies = array();

        //////////////////////////////////////////////////////

        $csv_file = $this->filepath;

        $fh = fopen($csv_file, 'r');
        $header = fgetcsv($fh);

        $data = array();
        while ($line = fgetcsv($fh)) {
            $data[] = array_combine($header, $line);
        }

        foreach ($data as $datarow) {

            $custom_post = array();
            $post_name = "";

            if (isset($datarow['Company Name']) && !empty($datarow['Company Name'])) {
                $custom_post['post_title'] = $datarow['Company Name'];
                $post_name = sanitize_title_with_dashes($datarow['Company Name']);
                $custom_post['post_name'] = $post_name;
            }

            if (isset($datarow['Address 1']) && isset($datarow['Address 2']) && isset($datarow['Address 3']) && isset($datarow['Address City']) && isset($datarow['Address State']) && isset($datarow['Address Country']) && isset($datarow['Address Zip'])) {
                // build the address json object
                $address_to_encode = $datarow['Address 1'];
                if ($datarow['Address 2'] != "") {
                    $address_to_encode . ", " . $datarow['Address 2'];
                }
                if ($datarow['Address 3'] != "") {
                    $address_to_encode . ", " . $datarow['Address 3'];
                }
                $array_txt = array('street' => $address_to_encode, 'locality' => $datarow['Address City'],
                    'region' => $datarow['Address State'], 'country' => $datarow['Address Country'], 'postcode' => $datarow['Address Zip']);
                $array_json = json_encode($array_txt);
            }

            // Prepare post meta data
            $meta_keys = array();
            if (isset($datarow['Company Code']) && !empty($datarow['Company Code'])) {
                $meta_keys['_a360_listing_company_code'] = $datarow['Company Code'];
            }
            if (isset($datarow['Company Name']) && !empty($datarow['Company Name'])) {
                $meta_keys['_a360_listing_companyname'] = $datarow['Company Name'];
            }

            $meta_keys['_a360_listing_address'] = $array_json;

            if (isset($datarow['Phone']) && !empty($datarow['Phone'])) {
                $meta_keys['_a360_listing_phone'] = $datarow['Phone'];
            }
            if (isset($datarow['Toll Free Phone']) && !empty($datarow['Toll Free Phone'])) {
                $meta_keys['_a360_listing_tollfree'] = $datarow['Toll Free Phone'];
            }
            if (isset($datarow['Fax']) && !empty($datarow['Fax'])) {
                $meta_keys['_a360_listing_fax'] = $datarow['Fax'];
            }
            if (isset($datarow['Email']) && !empty($datarow['Email'])) {
                $meta_keys['_a360_listing_email'] = $datarow['Email'];
            }
            if (isset($datarow['Website']) && !empty($datarow['Website'])) {
                $meta_keys['_a360_listing_website'] = $datarow['Website'];
            }
            if (isset($datarow['Description']) && !empty($datarow['Description'])) {
                $meta_keys['_a360_listing_description'] = $datarow['Description'];
            }
            if (isset($datarow['Internal Contact Name']) && !empty($datarow['Internal Contact Name'])) {
                $meta_keys['_a360_listing_contact_name'] = $datarow['Internal Contact Name'];
            }
            if (isset($datarow['Internal Contact Title']) && !empty($datarow['Internal Contact Title'])) {
                $meta_keys['_a360_listing_contact_title'] = $datarow['Internal Contact Title'];
            }
            if (isset($datarow['Internal Contact Phone']) && !empty($datarow['Internal Contact Phone'])) {
                $meta_keys['_a360_listing_contact_phone'] = $datarow['Internal Contact Phone'];
            }
            if (isset($datarow['Internal Contact Email']) && !empty($datarow['Internal Contact Email'])) {
                $meta_keys['_a360_listing_contact_email'] = $datarow['Internal Contact Email'];
            }
            if (isset($datarow['Trade/Brand Names']) && !empty($datarow['Trade/Brand Names'])) {
                $meta_keys['_a360_listing_trade_brand_names'] = $datarow['Trade/Brand Names'];
            }
            if (isset($datarow['Product Lines']) && !empty($datarow['Product Lines'])) {
                $meta_keys['_a360_listing_product_lines'] = $datarow['Product Lines'];
            }
            if (isset($datarow['Personnel Title 1']) && !empty($datarow['Personnel Title 1'])) {
                $meta_keys['_a360_listing_personnel_title_1'] = $datarow['Personnel Title 1'];
            }
            if (isset($datarow['Personnel Firstname 1']) && !empty($datarow['Personnel Firstname 1']) && isset($datarow['Personnel Lastname 1']) && !empty($datarow['Personnel Lastname 1'])) {
                $meta_keys['_a360_listing_personnel_name_1'] = $datarow['Personnel Firstname 1'] . " " . $datarow['Personnel Lastname 1'];
            }
            if (isset($datarow['Personnel Title 2']) && !empty($datarow['Personnel Title 2'])) {
                $meta_keys['_a360_listing_personnel_title_2'] = $datarow['Personnel Title 2'];
            }
            if (isset($datarow['Personnel Firstname 2']) && !empty($datarow['Personnel Firstname 2']) && isset($datarow['Personnel Lastname 2']) && !empty($datarow['Personnel Lastname 2'])) {
                $meta_keys['_a360_listing_personnel_name_2'] = $datarow['Personnel Firstname 2'] . " " . $datarow['Personnel Lastname 2'];
            }
            if (isset($datarow['Personnel Title 3']) && !empty($datarow['Personnel Title 3'])) {
                $meta_keys['_a360_listing_personnel_title_3'] = $datarow['Personnel Title 3'];
            }
            if (isset($datarow['Personnel Firstname 3']) && !empty($datarow['Personnel Firstname 3']) && isset($datarow['Personnel Lastname 3']) && !empty($datarow['Personnel Lastname 3'])) {
                $meta_keys['_a360_listing_personnel_name_3'] = $datarow['Personnel Firstname 3'] . " " . $datarow['Personnel Lastname 3'];
            }
            if (isset($datarow['Personnel Title 4']) && !empty($datarow['Personnel Title 4'])) {
                $meta_keys['_a360_listing_personnel_title_4'] = $datarow['Personnel Title 4'];
            }
            if (isset($datarow['Personnel Firstname 4']) && !empty($datarow['Personnel Firstname 4']) && isset($datarow['Personnel Lastname 4']) && !empty($datarow['Personnel Lastname 4'])) {
                $meta_keys['_a360_listing_personnel_name_4'] = $datarow['Personnel Firstname 4'] . " " . $datarow['Personnel Lastname 4'];
            }
            if (isset($datarow['Personnel Title 5']) && !empty($datarow['Personnel Title 5'])) {
                $meta_keys['_a360_listing_personnel_title_5'] = $datarow['Personnel Title 5'];
            }
            if (isset($datarow['Personnel Firstname 5']) && !empty($datarow['Personnel Firstname 5']) && isset($datarow['Personnel Lastname 5']) && !empty($datarow['Personnel Lastname 5'])) {
                $meta_keys['_a360_listing_personnel_name_5'] = $datarow['Personnel Firstname 5'] . " " . $datarow['Personnel Lastname 5'];
            }
            if (isset($datarow['Personnel Title 6']) && !empty($datarow['Personnel Title 6'])) {
                $meta_keys['_a360_listing_personnel_title_6'] = $datarow['Personnel Title 6'];
            }
            if (isset($datarow['Personnel Firstname 6']) && !empty($datarow['Personnel Firstname 6']) && isset($datarow['Personnel Lastname 6']) && !empty($datarow['Personnel Lastname 6'])) {
                $meta_keys['_a360_listing_personnel_name_6'] = $datarow['Personnel Firstname 6'] . " " . $datarow['Personnel Lastname 6'];
            }
            if (isset($datarow['Personnel Title 7']) && !empty($datarow['Personnel Title 7'])) {
                $meta_keys['_a360_listing_personnel_title_7'] = $datarow['Personnel Title 7'];
            }
            if (isset($datarow['Personnel Firstname 7']) && !empty($datarow['Personnel Firstname 7']) && isset($datarow['Personnel Lastname 7']) && !empty($datarow['Personnel Lastname 7'])) {
                $meta_keys['_a360_listing_personnel_name_7'] = $datarow['Personnel Firstname 7'] . " " . $datarow['Personnel Lastname 7'];
            }
            if (isset($datarow['Research URL']) && !empty($datarow['Research URL'])) {
                $meta_keys['_a360_listing_research_url'] = $datarow['Research URL'];
            }
            if (isset($datarow['Remarks']) && !empty($datarow['Remarks'])) {
                $meta_keys['_a360_listing_remarks'] = $datarow['Remarks'];
            }
            if (isset($datarow['Last Updated']) && !empty($datarow['Last Updated'])) {
                $meta_keys['_a360_listing_last_updated'] = $datarow['Last Updated'];
            }
            if (isset($datarow['Updated By']) && !empty($datarow['Updated By'])) {
                $meta_keys['_a360_listing_updated_by'] = $datarow['Updated By'];
            }
            if (isset($datarow['DBName']) && !empty($datarow['DBNamee'])) {
                $meta_keys['_a360_listing_DBName'] = $datarow['DBName'];
            }

            $post_status = "";
            $wp_ID = 0;

//             try to find the company code -- get the post id from the company code
            if (isset($datarow['Company Code'])) {
            	$company_code = $datarow['Company Code'];
                $results = $wpdb->get_results("SELECT post_id as ID FROM $wpdb->postmeta where meta_value = $company_code");
            }
            if (! $results && isset($datarow['WP ID'])) {
				$wp_ID = $datarow['WP ID'];
				$results = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE ID = $wp_ID");
            }
//			try to locate by company name
			if (! $results && isset($datarow['Company Name'])) {
				$company_name = $datarow['Company Name'];
				$results = $wpdb->get_results("SELECT ID FROM $wpdb->posts where post_title = '$company_name'");
			}

			if ($results) {
				$post_id = $wp_ID = $custom_post['ID'] = $results[0]->ID;
//				$post_id = wp_update_post($custom_post);
				$company_name = get_post_field('post_title', $post_id);
				$updatedListingCompanies[$post_id] = $company_name;
				// loop to check it meta keys aleady exist for this post
				foreach ($meta_keys as $key => $value) {
					// check if value exists in postmeta
					$my_post_meta = get_post_meta($post_id, $key, true);
					if (!empty($my_post_meta)) {
						// we found a value in the meta table -- update it and remove it from the array
						$id = update_post_meta($post_id, $key, $value);
						unset($meta_keys[$key]);
					}
				}
				$updatedListings++;
			} else {
				$custom_post['post_status'] = 'new_listing';
				$custom_post['post_author'] = 1;
				$custom_post['comment_status'] = 'close';
				$custom_post['ping_status'] = 'open';
				$custom_post['post_parent'] = 0;
				$custom_post['menu_order'] = 0;
				$custom_post['post_type'] = 'listing';
				$custom_post['comment_count'] = 0;
				$post_id = wp_insert_post($custom_post);
				$company_name = get_post_field('post_title', $post_id);
				$newListingCompanies[$post_id] = $company_name;
				$newListings++;
			}

            // insert remaining meta_keys values
            $custom_fields = array();
            $place_holders = array();
            $query_string = "INSERT INTO $wpdb->postmeta ( post_id, meta_key, meta_value) VALUES ";

            foreach ($meta_keys as $key => $value) {
                array_push($custom_fields, $post_id, $key, $value);
                $place_holders[] = "('%d', '%s', '%s')";
            }

            $query_string .= implode(', ', $place_holders);
            $wpdb->query($wpdb->prepare("$query_string ", $custom_fields));
        }

        fclose($fh);

        ///////////////////////////////////////////////////////////////////////
        // Get the list of editor email addresses from the options table
        $email_addresses = get_option('editor_email_addresses');
        $email_array = explode(',', $email_addresses);

        if (!empty($email_array)) {
            $defaultNewListingBody = '<html><p>' . $newListings . ' new listings imported</p>';
            $newListingBodyOpening = "";
            $newListingBody = array();
            $newListingBodyClosing = "";

            if (!empty($newListingCompanies)) {
                $newListingBodyOpening = '<ol>';
                $counter1 = 0;
                foreach ($newListingCompanies as $key => $value) {
                    $newListingBody[$counter1] = '<li>' . $value . '<br/><a href="' . site_url('/wp-admin/post.php?post=' . $key . '&action=edit') . '">' . site_url('/wp-admin/post.php?post=' . $key . '&action=edit') . '</a></li><br /><br />';
                    $counter1++;
                }
                $newListingBodyClosing = '</ol><hr>';
            }

            $defaultCompanyBody = '<p>' . $updatedListings . ' companies updated</p>';
            $companyBodyOpening = "";
            $companyBody = array();
            $companyBodyClosing = "";

            if (!empty($updatedListingCompanies)) {
                $companyBodyOpening = '<ol>';
                $counter2 = 0;
                foreach ($updatedListingCompanies as $key => $value) {
                    $companyBody[$counter2] = '<li>' . $value . '<br/><a href="' . site_url('/wp-admin/post.php?post=' . $key . '&action=edit') . '">' . site_url('/wp-admin/post.php?post=' . $key . '&action=edit') . '</a></li><br /><br />';
                    $counter2++;
                }
                $companyBodyClosing = '</ol>';
            }

            $body = $defaultNewListingBody . $newListingBodyOpening;
            $newListingBodyString = "";
            if (!empty($newListingBody)) {
                foreach ($newListingBody as $key=>$value) {
                    $newListingBodyString .= $value;
                }
            }

            $body .= $newListingBodyString . $defaultCompanyBody . $companyBodyOpening;

            $companyListingBodyString = "";
            if(!empty($companyBody)){
                foreach($companyBody as $key=>$value){
                    $companyListingBodyString .= $value;
                }
            }

            $body .= $companyListingBodyString . $companyBodyClosing;
//

            $subject = 'Mobius 365 Data Imported';
            $headers[] = "Content-type: text/html";
            $result = 0;

            foreach ($email_array as $email) {
                // Send the email
                $result = wp_mail($email, $subject, $body, $headers);
            }

            // Verify that the email sent
            if ($result == 1) {
                $cMessage = $updatedListings . " business listings updated (" . $newListings . " new listings).";
                return $cMessage;
            }
        }

        //////////////////////////////////////////////////////////////////////
    }

    public function processLine($data) {

    }

}
