<?php
namespace Guide\UploadHandler;

use Guide\UploadHandler;
use Exception;

abstract class CSV extends UploadHandler
{
    protected static $allowed_types = array(
        'text/csv'
    );
    
    public function process()
    {
       
        ini_set("auto_detect_line_endings", true);
        
        if (!($handle = fopen($this->filepath, "r"))) {
            throw new Exception("Failed to read input file for processing");
        }
        
        header('Content-type: text/plain');
        
        $rows = 0;
        $headers = null;
        while (($data = fgetcsv($handle)) !== false) {
            if (empty($headers)) {
                $headers = $data;
            } elseif (count($data) === count($headers)) {
                $this->processLine(array_combine($headers, $data));
                $rows++;
            }
        }
        
        fclose($handle);
        
        var_dump($rows);
        exit;
    }

    abstract public function processLine($data);
}
