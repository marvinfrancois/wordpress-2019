<?php
namespace Guide;

use Exception;

abstract class UploadHandler
{
    protected static $allowed_types = array(
        'text/csv'
    );
    
    protected $filepath;

    public function __construct($files_index)
    {
        set_time_limit(0);
        ignore_user_abort(1);
        error_reporting(E_ALL);
        ini_set('display_errors', 'on');
        ini_set('memory_limit', '256M');
        
        if ($this->verifyFilesIndex($files_index)) {
            $this->filepath = $_FILES[$files_index]['tmp_name'];
        }
    }
    
    public function verifyFilesIndex($index)
    {
        if (empty($_FILES[$index])) {
            throw new Exception('File upload missing');
        }
        
        if (!empty($_FILES[$index]['error'])) {
            throw new Exception('Error uploading file');
        }
        
        if (!empty(static::$allowed_types) && !in_array($_FILES[$index]['type'], static::$allowed_types)) {
            throw new Exception('Invalid file type');
        }
        
        return true;
    }

    abstract public function process();
}
