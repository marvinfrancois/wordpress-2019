<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();
?>
<div class="market-guide">
    <div class="breadcrubms">
        <?php A360\Guide::buildBreadCrumbs(); ?>
    </div>
    <div class="search-guide">
        <form method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) . get_option('market_guide_slug', 'market-guide') ); ?>" role="search">
            <label for="s" class="screen-reader-text"><?php _ex( 'Search', 'assistive text', 'allied' ); ?></label>
            <input type="search" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'allied' ); ?>" />
            <input type="submit" class="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'allied' ); ?>" />
        </form>
    </div>
    <section id="primary" class="site-content">
        <div id="content" role="main">
            <?php
            global $wp_query;
            global $query_string;
            global $wp;

            $someObj = get_queried_object();

            if (!is_search()) {
                $terms = \A360\Guide::get_mg_categories();
            }

            // Category Title
            if( ! is_search() && is_object($someObj)):
                ?><h1 class="category-title"><?=$someObj->name; ?></h1><?php
            endif;

            if( !empty($terms) ) {
                ?><div class="cats"><?php
                echo $terms;
                ?></div><?php
            }

            $promotedPostIDs = \A360\Guide::$promotedPostIDs;
            if ( empty($terms) && have_posts() ) : ?>
                <div class="guide-listing">

                    <?php /* Promoted Posts */ ?>

                    <?php foreach($promotedPostIDs as $promotedPostID) {
                        $company = A360\Core\Content::forge($promotedPostID);

                        extract( A360\Guide::parseCompany($company) , EXTR_PREFIX_ALL, "company");


                        $address = array();
                        $company_street and $address[] = $company_street;
                        $company_locality and $address[] = $company_locality;
                        $company_region and $address[] = $company_region;
                        $company_postcode and $address[] = $company_postcode;

                        $phone = array();
                        $company_phone and $phone[] = $company_phone;
                        $company_fax and $phone[] = "Fax: " . $company_fax;

                        $internetContact = array();
                        $company_website and $internetContact[] = '<a href="' . $company_website . '">' . $company_website . '</a>' ;
                        $company_email and $internetContact[] = '<a class="email" href="mailto:' . $company_email . '">' . $company_email . '</a>';

                        $sectionClasses = "";
                        if($company_listing_type == 'promoted'): ?>
                            <div class="promoted">
                        <?php endif; ?>
                        <section class="listing">
                            <h3><a href="<?php echo home_url(get_option('market_guide_slug', 'market-guide') . '/listing/' . $company->post_name); ?>"><?php echo $company_name; ?></a></h3>
							
							 <?php if ($company_listing_type == 'promoted' && $company_logo) : ?>
								<div class="logo">
									<a href="<?php echo home_url(get_option('market_guide_slug', 'market-guide') . '/listing/' . $company->post_name); ?>">
										<img src="<?php echo $company_logo; ?>" alt="<?php echo $company_name; ?>"/>
									</a>
								</div>
							<?php endif; ?>
							
                            <?php /* if(empty($company->owner_id)) : ?>
                            <a href="/<?php echo $claimSlug . "/" . $company->ID; ?>">Claim Listing</a>
                        <?php endif; // */ ?>
                            <address>
                                <div class="address">
                                    <?php echo implode(', ', $address); ?>
                                </div>
                                <div class="phone">
                                    <?php echo implode('<span class="separator">|</span>', $phone); ?>
                                </div>
                                <div class="internet">
                                    <?php echo implode('<span class="separator">|</span>', $internetContact); ?>
                                </div>
                            </address>
                        </section>
                        <?php if($company_listing_type == 'promoted'): ?>
                            </div>
                        <?php endif; ?>
                    <?php
                    } ?>

                    <?php /* Non-Promoted Posts */ ?>

                    <?php while ( have_posts() ) : the_post();

                        $company = A360\Core\Content::forge(get_the_ID());

                        extract( A360\Guide::parseCompany($company) , EXTR_PREFIX_ALL, "company");


                        $address = array();
                        $company_street and $address[] = $company_street;
                        $company_locality and $address[] = $company_locality;
                        $company_region and $address[] = $company_region;
                        $company_postcode and $address[] = $company_postcode;

                        $phone = array();
                        $company_phone and $phone[] = $company_phone;
                        $company_fax and $phone[] = "Fax: " . $company_fax;

                        $internetContact = array();
                        $company_website and $internetContact[] = '<a href="' . $company_website . '">' . $company_website . '</a>' ;
                        $company_email and $internetContact[] = '<a class="email" href="mailto:' . $company_email . '">' . $company_email . '</a>';

                        $sectionClasses = "";
                        if($company_listing_type == 'promoted'): ?>
                            <div class="promoted">
                        <?php endif; ?>
                        <section class="listing">
                            <h3><a href="<?php echo home_url(get_option('market_guide_slug', 'market-guide') . '/listing/' . $company->post_name); ?>"><?php echo $company_name; ?></a></h3>
                            <?php /* if(empty($company->owner_id)) : ?>
                            <a href="/<?php echo $claimSlug . "/" . $company->ID; ?>">Claim Listing</a>
                        <?php endif; // */ ?>
                            <address>
                                <div class="address">
                                    <?php echo implode(', ', $address); ?>
                                </div>
                                <div class="phone">
                                    <?php echo implode('<span class="separator">|</span>', $phone); ?>
                                </div>
                                <div class="internet">
                                    <?php echo implode('<span class="separator">|</span>', $internetContact); ?>
                                </div>
                            </address>
                        </section>
                        <?php if($company_listing_type == 'promoted'): ?>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                    <?php
                    if (function_exists('wp_paginate')):
                        wp_paginate(false);
                    endif;
                    ?>
                </div>
            <?php endif; ?>
            <?php if(!have_posts() && is_search()): ?>
                <div class="noresults">
                    <h3>
                        No Search Results Found.
                    </h3>
                </div>
            <?php endif; ?>
        </div><!-- #content -->
    </section><!-- #primary -->
</div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>