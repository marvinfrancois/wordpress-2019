<?php

/**
 * The Template for displaying all single posts.
 *
 * @package Allied
 */

?>
<?php get_header(); ?>
<div class="market-guide">
    <div class="breadcrubms">
        <?php A360\Guide::buildBreadCrumbs(); ?>
    </div>
    <section id="primary" class="site-content">
        <div id="content" role="main">
        <div class="single-market-guide-listing">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php
                $company = A360\Core\Content::forge(get_the_ID());
                $thisCompany = A360\Guide::parseCompany($company);
                extract( $thisCompany , EXTR_PREFIX_ALL, "company");

                $address = array();
                $company_street and $address[] = $company_street;
                $company_locality and $address[] = $company_locality;
                $company_region and $address[] = $company_region;
                $company_postcode and $address[] = $company_postcode;

                $phone = array();
                $company_phone and $phone[] = $company_phone;
                $company_fax and $phone[] = "Fax: " . $company_fax;

                ?>
                <section>

                    <header>
                        <?php if($company_logo): ?>
                            <?php $alt = ""; $company_name && $alt = $company_name; ?>
                            <img src="<?php echo $company_logo; ?>" alt="<?php echo $alt; ?>"/>
                        <?php endif; ?>
                        <?php if($company_name): ?><h3><?php echo $company_name; ?></h3><?php endif; ?>
                        <?php /* if(empty($company->owner_id)) : ?>
                            <?php $claimSlug = get_option('claim_slug', 'claim'); ?>
                            <a href="/<?php echo $claimSlug; ?>">Claim Listing</a>
                        <?php endif; // */ ?>
                    </header>

                    <?php if($company_description): ?><article><?php echo $company_description; ?></article><?php endif; ?>

                    <address>
                        <?php if( $company_website ): ?><a class="website" href="<?php echo $company_website ?>"><?php echo $company_website; ?></a><?php endif; ?>
                        <div class="address">
                            <?php echo implode(', ', $address); ?>
                        </div>
                        <div class="phone">
                            <?php echo implode('<span class="separator">|</span>', $phone); ?>
                        </div>
                        <?php if( $company_email ): ?><a class="email" href="mailto:<?php echo $company_email; ?>"><?php echo $company_email; ?></a><?php endif; ?>
                    </address>

                </section>

            <?php endwhile; ?>
        </div>
        </div><!-- #content -->
    </section><!-- #primary -->
</div>
<?php get_footer(); ?>