<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Allied
 */

function assignPageTitle(){
    return get_option('claim_header_text', 'Claim Listing');
}
add_filter('wp_title', 'assignPageTitle');

global $wp;



$businessID = explode('/', $wp->request);



/*
$user_id = username_exists( $user_name );
if ( !$user_id and email_exists($user_email) == false ) {
    $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
    $user_id = wp_create_user( $user_name, $random_password, $user_email );
} else {
    $random_password = __('User already exists.  Password inherited.');
}
*/

function checkPostVars($vars = array()) {
    if(count($vars) < 1) return false;
    foreach($vars as $var) {
        if($_POST[$var] && count($_POST[$var]) > 0 ) {
            return true;
        } else {
            return false;
        }
    }
}

get_header(); ?>
    <style type="text/css">
        .form-table th {
            padding-right: 15px;
        }
        .form-table tr {
            padding-top: 10px
        }
    </style>
	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
            <h1><?php echo get_option('claim_header_text', 'Claim Listing'); ?></h1><br />
            <a href="<?php echo wp_login_url( $redirect ); ?>">Log In</a> to claim, or Register:<br /><br />
            <?php

            if($_POST){
                $fields = array(
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'username',
                    'password'
                );
                if(checkPostVars($fields)) {
                    $username = sanitize_user($_POST['username']);
                    if(NULL == username_exists($username)) {
                        // Make new user
                        $email = sanitize_email($_POST['email']);
                        if(email_exists($email) == false) {
                            $pw = mysql_real_escape_string($_POST['pasword']);
                            $uID = wp_create_user( $username, $password, $email );
                            if(NULL != $uID) {
                                update_user_meta($uID, 'first_name', mysql_real_escape_string($_POST['first_name']));
                                update_user_meta($uID, 'last_name', mysql_real_escape_string($_POST['last_name']));
                                update_user_meta($uID, 'phone', mysql_real_escape_string($_POST['phone']));
                                update_user_meta($uID, 'show_admin_bar_front', 'false');
                            } else {
                                $errorMessage = "Error.";
                            }


                        } else {
                            $errorMessage = "That email is already used";
                        }
                    } else {
                        $errorMessage = "That username is taken.";
                    }
                } else {
                    $errorMessage = "Please fill in all fields";
                }
            }

            ?>
            <h3>Register</h3>
            <form method="post">
                <br />
                <span>All Fields are REQUIRED.</span><br />
                <?php if(isset($errorMessage)): ?><span class="red"><?php echo $errorMessage; ?></span><?php endif; ?>
                <br /><br />
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">First Name</th>
                        <td><input type="text" name="first_name"/><br /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Last Name</th>
                        <td><input type="text" name="last_name"/><br /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">E-Mail</th>
                        <td><input type="text" name="email"/><br /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Phone</th>
                        <td><input type="text" name="phone"/><br /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Username</th>
                        <td><input type="text" name="username"/><br /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Password</th>
                        <td><input type="password" name="password"/><br /></td>
                    </tr>

                    <input type="hidden" name="businessID" value="<?php echo $businessID[1]; ?>"/>
                </table>
                <input type="submit" value="<?php echo get_option('claim_btn_text', 'Claim Listing'); ?>"/>
            </form>
            <?php
            $company = A360\Core\Content::forge($businessID[1]);
            ?>
            <br />
            <section>
                <h3><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo $company->companyname; ?></a></h3>
                <address>
                    <?php if($company->address->street != null) { echo $company->address->street . "<br />"; } ?>
                    <?php echo $company->address->locality; ?>,
                    <?php echo $company->address->region; ?>,
                    <?php echo $company->address->postcode; ?>
                    <?php if( $company->address->locality != null || $company->address->region != null || $company->address->postcode != null) { ?>
                        <br />
                    <?php } ?>
                    <?php echo $company->email . "<br />"; ?>
                    <?php if( $company->phone != null ) { echo $company->phone . "<br />"; } ?>
                    <?php if( $company->fax != null ) { echo $company->fax . "<br />"; } ?>
                    <a href="<?php echo $company->website; ?>"><?php echo $company->website; ?></a><br />
                </address>
            </section>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>