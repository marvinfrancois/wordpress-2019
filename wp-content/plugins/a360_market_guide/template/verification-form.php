<?php
get_header();

global $wp;
global $wpdb;

//Verify that we have a Guid as and query string parameter named 'id'
if (isset($wp->query_vars['id'])) {
    if (empty($_POST)) {
        // start building the form
        ?>
        <form action = '' id = 'verification' method = 'POST' >
            <h3>Buyer's Guide -- Business Listing Verification</h3>
            <p>
                Please verify your business listing information below.<br/>
                Fields marked with a (*) are required.<br/>
                Make any necessary changes, then click Submit.
            </p>
            <hr>
            <?php
            // We have a Guid -- look up the Guid in the meta table and determine if the user is editing, or saving
            $linkGuid = $wp->query_vars['id'];

            $clientChoice = $wpdb->get_results("select meta_key from $wpdb->postmeta where meta_value = '" . $linkGuid . "'", ARRAY_A);

            if (!empty($clientChoice)) {
                if ($clientChoice[0]['meta_key'] === '_a360_edit_listing_link_guid') {
                    // we are in edit mode
                    //
                    //Get the post_id associated with the guid stored in postmeta
                    $results = $wpdb->get_results("select post_id from $wpdb->postmeta where meta_value = '" . $linkGuid . "'", ARRAY_A);
                    $post_id = $results[0]['post_id'];

                    //Get remaining company details from meta
                    $company_name = get_post_meta($post_id, '_a360_listing_companyname', True);
                    $phone = get_post_meta($post_id, '_a360_listing_phone', True);
                    $fax = get_post_meta($post_id, '_a360_listing_fax', True);
                    $tollfree = get_post_meta($post_id, '_a360_listing_tollfree', True);
                    $email = get_post_meta($post_id, '_a360_listing_email', True);
                    $website = get_post_meta($post_id, '_a360_listing_website', True);
                    $facebook = get_post_meta($post_id, '_a360_listing_facebook', True);
                    $logo_url = get_post_meta($post_id, '_a360_listing_logo', True);
                    $description = get_post_meta($post_id, '_a360_listing_description', True);
                    ?>
                    <input type="hidden" name="post_id" value='<?php echo $post_id ?>'>
                    <label><strong>Company Name</strong></label><label style='color: red;'> *</label><br/>
                    <input name="company_name" type="text" style='width: 100%;
                           ' value='<?php echo $company_name ?>' required="true"><br/><br/>
                           <?php
                           //Retrieve the address json object from the DB
                           $address_json = get_post_meta($post_id, '_a360_listing_address', True);
                           //Decode the oject into an array
                           $address_array = json_decode($address_json, True);
                           //Set address values
                           ?>
                    <label><strong>Street</strong></label><label style='color: red;'> *</label><br/>
                    <input name="street" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['street'] ?>' required="true"><br/><br/>
                    <label><strong>Street 2</strong></label><br/>
                    <input name="street2" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['street2'] ?>'><br/><br/>
                    <label><strong>Street 3</strong></label><br/>
                    <input name="street3" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['street3'] ?>'><br/><br/>
                    <label><strong>City</strong></label><label style='color: red;'> *</label><br/>
                    <input name="city" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['locality'] ?>' required="true"><br/><br/>
                    <label><strong>State</strong></label><label style='color: red;'> *</label><br/>
                    <input name="state" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['region'] ?>' required="true"><br/><br/>
                    <label><strong>Country</strong></label><label style='color: red;'> *</label><br/>
                    <input name="country" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['country'] ?>' required="true"><br/><br/>
                    <label><strong>Zip Code</strong></label><label style='color: red;'> *</label><br/>
                    <input name="zip_code" type="text" style='width: 100%;
                           ' value='<?php echo $address_array['postcode'] ?>' required="true"><br/><br/>
                    <label><strong>Phone Number</strong></label><label style='color: red;'> *</label><br/>
                    <input name="phone" type="tel" style='width: 100%;
                           ' value='<?php echo $phone ?>' required="true"><br/><br/>
                    <label><strong>Fax</strong></label><br/>
                    <input name="fax" type="tel" style='width: 100%;
                           ' value='<?php echo $fax ?>'><br/><br/>
                    <label><strong>Toll-free</strong></label><br/>
                    <input name="tollfree" type="tel" style='width: 100%;
                           ' value='<?php echo $tollfree ?>'><br/><br/>
                    <label><strong>Contact Email</strong></label><label style='color: red;'> *</label><br/>
                    <input name="email" type="email" style='width: 100%;
                           ' value='<?php echo $email ?>' required="true"><br/><br/>
                    <label><strong>Website</strong></label><label style='color: red;'> *</label><br/>
                    <input name="website" type="text" style='width: 100%;
                           ' value='<?php echo $website ?>' required="true"><br/><br/>
                    <label><strong>Facebook</strong></label><br/>
                    <input name="facebook" type="text" style='width: 100%;
                           ' value='<?php echo $facebook ?>'><br/><br/>
                    <label><strong>Logo URL</strong></label><br/>
                    <input name="logo_url" type="text" style='width: 100%;
                           ' value='<?php echo $logo_url ?>'><br/><br/>
                    <label><strong>Description</strong></label><br/>
                    <textarea name="description" form="verification" rows="8"><?php echo $description ?></textarea><br/><br/>
                    <?php
                } else {
                    // we are in save mode -- update the date only
                    //==============================================================================
                    $results = $wpdb->get_results("select post_id from $wpdb->postmeta where meta_value = '" . $linkGuid . "'", ARRAY_A);
                    $postID = $results[0]['post_id'];

                    $custom_post = array();
                    $custom_post['ID'] = $postID;
                    $post_id = wp_update_post($custom_post);

                    $meta_keys = array();
                    $meta_keys['_a360_listing_verified_date'] = date('m/d/Y');
                    $meta_keys['_a360_listing_verified_type'] = 'email';

                    // try to locate the verified date and type in meta table
                    foreach ($meta_keys as $key => $value) {
                        // check if value exists in postmeta
                        $my_post_meta = $wpdb->get_results("SELECT meta_value FROM $wpdb->postmeta where post_id = $postID AND meta_key = '$key'");
                        if (isset($my_post_meta)) {
                            // we found a value in the meta table -- update it and remove it from the array
                            $id = update_post_meta($postID, $key, $value);
                            unset($meta_keys[$key]);
                        }
                    }

                    if (count($meta_keys) > 0) {
                        $custom_fields = array();
                        $place_holders = array();
                        $query_string = "INSERT INTO $wpdb->postmeta ( post_id, meta_key, meta_value) VALUES ";

                        foreach ($meta_keys as $key => $value) {
                            array_push($custom_fields, $postID, $key, $value);
                            $place_holders[] = "('%d', '%s', '%s')";
                        }

                        $query_string .= implode(', ', $place_holders);
                        $wpdb->query($wpdb->prepare("$query_string ", $custom_fields));
                    }


                    wp_redirect(site_url('/verification-thank-you'), 302);
                    //return;
                }
            }

            // get all categories and sub-categories
            //====================================================
            // get the number of levels allowed from the wp_options table
            $category_depth = get_option('category_depth');

            $all_categories = $wpdb->get_results("SELECT * FROM $wpdb->term_taxonomy WHERE taxonomy = 'directory'", ARRAY_A);
            $parentsArray = array();
            $childrenArray = array();

            // sort the $terms into parents and children
            foreach ($all_categories as $category) {
                if ($category['parent'] > 0) {
                    // add to child array
                    $childrenArray[] = $category;
                } else {
                    // add to parent array
                    $parentsArray[] = $category;
                }
            }

            $parent_details = array();
            $child_details = array();

            foreach ($parentsArray as $parent) {
                $parents_details[] = get_term_by('term_id', $parent['term_id'], 'directory', ARRAY_A);
            }

            foreach ($childrenArray as $child) {
                $child_details[] = get_term_by('term_id', $child['term_id'], 'directory', ARRAY_A);
            }

            $results = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = '" . $linkGuid . "'", ARRAY_A);
            $postID = $results[0]['post_id'];
            $taxonomy_ids = $wpdb->get_results("SELECT term_taxonomy_id FROM $wpdb->term_relationships where object_id = $postID", ARRAY_A);
            $terms = array();

            // get the details for each taxonomy id
            foreach ($taxonomy_ids as $taxonomy_id) {
                $terms[] = get_term_by('term_taxonomy_id', $taxonomy_id['term_taxonomy_id'], 'directory', ARRAY_A);
            }

            // need the names of terms
            $term_names = array();

            foreach ($terms as $term) {
                $term_names[] = $term['name'];
            }

            if (!empty($parents_details)) {
                ?>
                <label><strong>Categories</strong></label><br/>
                <label>Select the categories that apply to your business listing:</label><br/><br/>
                <ul><?php
                    foreach ($parents_details as $parent) {
                        // list parent
                        ?>
                        <li>
                            <label>
                                <input name="<?php echo $parent['slug'] ?>" type="hidden" value="0" >
                                <input name="<?php echo $parent['slug'] ?>" type="checkbox" value="1"<?php
                                if (in_array($parent['name'], $term_names)) {
                                    ?>checked="checked"<?php }
                                ?>>
                                       <?php echo $parent['name'] ?>
                            </label>
                        </li><?php
                        if ($category_depth > 1 && !empty($child_details)) {
                            ?><ul><?php
                                foreach ($child_details as $child) {
                                    if ($parent['term_id'] == $child['parent']) {
                                        // add the child as a sub of the parent
                                        ?>
                                        <li>
                                            <label>
                                                <input name="<?php echo $child['slug'] ?>" type="hidden" value="0" >
                                                <input name="<?php echo $child['slug'] ?>" type="checkbox" value="1"
                                                <?php if (in_array($child['name'], $term_names)) {
                                                    ?>checked="checked"<?php } ?>>
                                                       <?php echo $child['name'] ?>
                                            </label>
                                        </li><?php
                                        if ($category_depth > 2) {
                                            foreach ($child_details as $subA) {
                                                if ($child['term_id'] == $subA['parent']) {
                                                    ?><ul>
                                                        <li>
                                                            <label>
                                                                <input name="<?php echo $subA['slug'] ?>" type="hidden" value="0" >
                                                                <input name="<?php echo $subA['slug'] ?>" type="checkbox" value="1"
                                                                <?php if (in_array($subA['name'], $term_names)) {
                                                                    ?>checked='checked'<?php } ?>>
                                                                       <?php echo $subA['name'] ?>
                                                            </label>
                                                        </li><?php
                                                        if ($category_depth > 3) {
                                                            foreach ($child_details as $subB) {
                                                                if ($subA['term_id'] == $subB['parent']) {
                                                                    ?><ul>
                                                                        <li>
                                                                            <label>
                                                                                <input name="<?php echo $subB['slug'] ?>" type="hidden" value="0" >
                                                                                <input name="<?php echo $subB['slug'] ?>" type='checkbox' value="1"
                                                                                <?php if (in_array($subB['name'], $term_names)) {
                                                                                    ?>checked='checked'<?php } ?>>
                                                                                       <?php echo $subB['name'] ?>
                                                                            </label>
                                                                        </li>
                                                                    </ul><?php
                                                                }
                                                            }
                                                        }
                                                        ?></ul><?php
                                                }
                                            }
                                        }
                                    }
                                }
                                ?></ul><?php
                            }
                        }
                    }
                    ?>
            </ul>
            <input name='Submit' type='submit' value='Submit' style="width: 50%; font-size: 1em; font-weight: bold;">
        </form>
        <?php
    } else {
        // the form has been posted -- save the edits / categories selections
        $company = array();
        foreach ($_POST as $key => $value) {
            if ($value != '1' && $value != '0' && $value != 'Submit') {
                $company[$key] = $value;
            } else {
                // get all of the categories that the user has selected
                if ($value === '1') {
                    $category_array[] = $key;
                }
            }
        }

        $postID = $company['post_id'];

        if (isset($company['street']) && isset($company['street2']) && isset($company['street3']) && isset($company['city']) && isset($company['state']) && isset($company['country']) && isset($company['zip_code'])) {
            // build the address json object
            $address_to_encode = $company['street'];
            if ($company['street2'] != "") {
                $address_to_encode . ", " . $company['street2'];
            }
            if ($company['street3'] != "") {
                $address_to_encode . ", " . $company['street3'];
            }
            $array_txt = array('street' => $address_to_encode, 'locality' => $company['city'],
                'region' => $company['state'], 'country' => $company['country'], 'postcode' => $company['zip_code']);
            $array_json = json_encode($array_txt);
        }

        $meta_keys = array();

        if (isset($datarow['company_name'])) {
            $meta_keys['_a360_listing_companyname'] = $datarow['company_name'];
        }

        $meta_keys['_a360_listing_address'] = $array_json;

        if (isset($company['phone'])) {
            $meta_keys['_a360_listing_phone'] = $company['phone'];
        }
        if (isset($company['tollfree'])) {
            $meta_keys['_a360_listing_tollfree'] = $company['tollfree'];
        }
        if (isset($company['fax'])) {
            $meta_keys['_a360_listing_fax'] = $company['fax'];
        }
        if (isset($company['email'])) {
            $meta_keys['_a360_listing_email'] = $company['email'];
        }
        if (isset($company['website'])) {
            $meta_keys['_a360_listing_website'] = $company['website'];
        }
        if (isset($company['facebook'])) {
            $meta_keys['_a360_listing_facebook'] = $company['facebook'];
        }
        if (isset($company['logo_url'])) {
            $meta_keys['_a360_listing_logo'] = $company['logo_url'];
        }
        if (isset($company['description'])) {
            $meta_keys['_a360_listing_description'] = $company['description'];
        }


        $meta_keys['_a360_listing_verified_date'] = date('m/d/Y');
        $meta_keys['_a360_listing_verified_type'] = 'form';

        // loop to check if meta keys aleady exist for this post
        foreach ($meta_keys as $key => $value) {
            // check if value exists in postmeta
            $my_post_meta = $wpdb->get_results("SELECT meta_value FROM $wpdb->postmeta where post_id = $postID AND meta_key = '$key'");
            if (isset($my_post_meta)) {
                // we found a value in the meta table -- update it and remove it from the array
                $id = update_post_meta($postID, $key, $value);
                unset($meta_keys[$key]);
            }
        }

        if (count($meta_keys) > 0) {
            // insert remaining meta_keys values
            $custom_fields = array();
            $place_holders = array();
            $query_string = "INSERT INTO $wpdb->postmeta ( post_id, meta_key, meta_value) VALUES ";

            foreach ($meta_keys as $key => $value) {
                array_push($custom_fields, $postID, $key, $value);
                $place_holders[] = "('%d', '%s', '%s')";
            }

            $query_string .= implode(', ', $place_holders);
            $wpdb->query($wpdb->prepare("$query_string ", $custom_fields));
        }

        // Save the category selections
        $termID_array = array();
        $counter = 0;

        if (!empty($category_array)) {
            foreach ($category_array as $category) {
                // Get term_id of each category
                $term_array = get_term_by('slug', $category, 'directory');
                $termID = 0;
                $taxonomyID = 0;

                if (isset($term_array->term_id)) {
                    $termID = (int) ($term_array->term_id);
                    $termID_array[$counter] = $termID;
                }

                $counter++;
            }

            // Save category selections in term
            $term_taxonomy_ids = wp_set_object_terms($postID, $termID_array, 'directory');
        }

        // Update the listing
        $custom_post = array();
        $custom_post['ID'] = $postID;
        $custom_post['post_title'] = $company['company_name'];
        $post_id = wp_update_post($custom_post);

        // save the values in Ninja Forms table for Mobius 365 export
        //1.  Create a new post and get the post id
        $ninja_post = array();
        $ninja_post['post_status'] = 'publish';
        $ninja_post['comment_status'] = 'closed';
        $ninja_post['ping_status'] = 'open';
        $ninja_post['post_type'] = 'nf_sub';
        $sub_id = wp_insert_post($ninja_post);

        //2. Create a new meta key array and push to meta table
        $n_meta_keys = array();

        // retrieve the last seq number
        $last_seq_num = $wpdb->get_results("select meta_value from $wpdb->postmeta where meta_key = '_seq_num'");
        if (!empty($last_seq_num)) {
            $last_num = end($last_seq_num);
            $new_seq_num = (int) $last_num->meta_value;
            $new_seq = $new_seq_num + 1;
        } else {
            $new_seq = 0;
        }

        $n_meta_keys['_form_id'] = 1;
        $n_meta_keys['_seq_num'] = $new_seq;
        $n_meta_keys['_action'] = 'submit';
        $n_meta_keys['_field_15'] = '&lt;h3&gt;Buyer&#039;s Guide -- Business Listing Verification&lt;/h3&gt;';
        $n_meta_keys['_field_17'] = 'Please verify your business listing information below.
Fields marked with a (*) are required.
Make any necessary changes, then click Submit.';
        $n_meta_keys['_field_9'] = '';
        $n_meta_keys['_field_10'] = $company['company_name'];
        $n_meta_keys['_field_13'] = $company['street'];
        $n_meta_keys['_field_14'] = $company['street2'];
        $n_meta_keys['_field_16'] = $company['street3'];
        $n_meta_keys['_field_18'] = $company['city'];
        $n_meta_keys['_field_19'] = $company['state'];
        $n_meta_keys['_field_20'] = $company['zip_code'];
        $n_meta_keys['_field_21'] = $company['country'];
        $n_meta_keys['_field_22'] = $company['phone'];
        $n_meta_keys['_field_24'] = $company['fax'];
        $n_meta_keys['_field_23'] = $company['tollfree'];
        $n_meta_keys['_field_25'] = $company['email'];
        $n_meta_keys['_field_26'] = $company['website'];
        $n_meta_keys['_field_27'] = $company['facebook'];
        $n_meta_keys['_field_28'] = $company['logo_url'];
        $n_meta_keys['_field_29'] = $company['description'];
        $n_meta_keys['_field_32'] = $postID;
        $n_meta_keys['_sub_id'] = $sub_id;

        // insert meta_keys values
        $custom_fields = array();
        $place_holders = array();
        $query_string = "INSERT INTO $wpdb->postmeta ( post_id, meta_key, meta_value) VALUES ";

        foreach ($n_meta_keys as $key => $value) {
            array_push($custom_fields, $sub_id, $key, $value);
            $place_holders[] = "('%d', '%s', '%s')";
        }

        $query_string .= implode(', ', $place_holders);
        $wpdb->query($wpdb->prepare("$query_string ", $custom_fields));

        //send the user to the thank-you page
        wp_redirect(site_url('verification-thank-you'));
    }
} else {
    // no parameter set -- user tried to go directly to the business-listing-verification page
    status_header(404);
    include(get_404_template());
    die();
}
?>
<?php get_footer();?>