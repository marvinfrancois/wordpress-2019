<?php
/*
  Plugin Name: A360 Market Guide
  Plugin URI: http://example.com/
  Description: Description
  Version: 0.1
  Author: Steven Kohlmeyer
  Author URI: http://www.allied360.com/
 */

namespace A360;

use A360\Core\Plugin;
use A360\Core\Content;
use A360\Core\ContentType;
use A360\Core\FieldGroup;
use A360\Core\Field;
use A360\Core\Shortcode;
use A360\Core\Metabox;
use A360\Core\Taxonomy;
use A360\Core\User;
use A360\Core\Rewrite;
use Walker;

require_once(WP_PLUGIN_DIR . '/a360_market_guide/lib/UploadHandler.php');
require_once(WP_PLUGIN_DIR . '/a360_market_guide/lib/UploadHandler/CSV.php');
require_once(WP_PLUGIN_DIR . '/a360_market_guide/lib/UploadHandler/CSV/Mobius365Import.php');
use Guide\UploadHandler\CSV\Mobius365Import;

if (!class_exists('\A360\Core\Plugin'))
    return;

class Guide extends Plugin
{

    public static $promotedPostIDs = array();
    // Give subscriber role some extra permissions on custom content type and custom taxonomy
    public static $subScriberArgs = array(
        'capabilities' => array(
            /* 'publish_posts' => 'read', */
            'edit_posts' => 'read',
            'edit_post' => 'read',
            'delete_post' => 'read',
            'read_post' => 'read',
        /* 'assign_terms' => 'read' */
        ),
    	'menu_position' => 5
    );
    public static $listingContactCapabilities = array(
        'edit_posts' => 'read',
        'edit_post' => 'read',
        'delete_post' => 'read',
        'read_post' => 'read',
    );
    protected static $active = false;
    protected static $requestString;
    protected static $is404 = false;
    protected static $marketGuideSlug = "";
    protected static $marketGuideTitle = "";
    protected static $marketGuideSeparator = "";
    protected static $breadcrumbsURI = "";
    protected static $breadcrumbs = "";

    public static function bootstrap()
    {
        parent::bootstrap();

        static::cookieMessage(false);

        self::$marketGuideSlug = get_option('market_guide_slug', 'market-guide');
        self::$marketGuideTitle = get_option('market_guide_title', 'Market Guide');
        self::$marketGuideSeparator = get_option('market_guide_breadcrumbs_separator', '/');


        // Actions
        add_action('save_post', __CLASS__ . '::custom_post_save');
        add_action("template_redirect", __CLASS__ . '::a360_rewrites');

        add_action('login_enqueue_scripts', __CLASS__ . '::login_logo');

        // Remove any UI Elements on listing post page for everyone
        add_action('do_meta_boxes', __CLASS__ . '::UI_everyone_meta_boxes');

        // Search to include custom fields -- This is currently not working - AND statemtent between post-title/content search and meta field searches instead of OR, not sure how to manipulate wp to do this right.
        add_action('pre_get_posts', __CLASS__ . '::a360_custom_search');

        add_action('widgets_init', __CLASS__ . '::a360_widgets');

        add_action('admin_init', __CLASS__ . '::a360_action_admin_init');

        add_action('admin_head', __CLASS__ . '::a360_admin_css');

        add_action('pre_get_posts', __CLASS__ . '::a360_get_business_listings');



        // Filters
        add_filter('term_link', __CLASS__ . '::term_link', 10, 3);
        add_filter('rewrite_rules_array', function($rules) {
            $rules = array_merge(array(
                get_option('market_guide_slug', 'market-guide') . "/page/([0-9]+)/?$" => "index.php?paged=\$matches[1]&post_type=listing",
                get_option('market_guide_slug', 'market-guide') . "/listing/([^/]+)/?$" => "index.php?listing=\$matches[1]",
                get_option('market_guide_slug', 'market-guide') . "/(.*/)?([^/]+)/page/([0-9]+)/?$" => "index.php?directory=\$matches[2]&paged=\$matches[3]&post_type=listing",
                get_option('market_guide_slug', 'market-guide') . "/.*/([^/]+)/?$" => "index.php?directory=\$matches[1]&post_type=listing",
                get_option('market_guide_slug', 'market-guide') . "/([^/]+)/?$" => "index.php?directory=\$matches[1]&post_type=listing",
                get_option('market_guide_slug', 'market-guide') => "index.php?post_type=listing",
                ), $rules);
            return $rules;
        });
        // Add / Remove fields from user profile page
        add_filter('user_contactmethods', __CLASS__ . '::custom_profile_fields');


        add_filter('redirect_canonical', function ($redirect_url, $requested_url) {
            if (strpos($requested_url, get_option('market_guide_slug', 'market-guide')) !== false) {
                $redirect_url = $requested_url;
            }
            return $redirect_url;
        }, 10, 2);

        // Subscriber Only Actions / Filters
        if (self::is_subscriber()) {

            // Actions
            // Remove any UI Elements on listing post page for subscriber
            add_action('admin_menu', __CLASS__ . '::UI_subscriber_meta_boxes');

            // Remove Dashboard Menu Link
            add_action('admin_menu', __CLASS__ . '::admin_menu_remove_dashboard');

            // add_action('admin_menu', __CLASS__.'::marketGuideSubscriberMenu');
            // Filters
            // Only allow user to see own posts
            add_filter('parse_query', __CLASS__ . '::only_view_own_posts');

            // Filter out custom fields to not show - This does not work
            //add_filter('is_protected_meta', __CLASS__.'::hide_custom_fields', 10, 2);
            // Redirect subscribers to their post listing after save/update
            add_filter('redirect_post_location', __CLASS__ . '::redirect_after_post_save');
            add_action('wp_enqueue_scripts', array(get_called_class(), remove_profile_fields));
        }

        if (is_admin()) {

            add_action('admin_menu', __CLASS__ . '::marketGuideSettingsMenu');
        }
    }

    ///////////////////////       Plugin Hooks       ///////////////////////

    public static function a360_market_guide_activate()
    {
        // Plugin Activation hook, maybe assign new role / capabilities here
        // Add custom role for media guide users:
        add_role('listing_contact', 'Listing Contact', self::$listingContactCapabilities);
    }

    public static function a360_market_guide_deactivate()
    {
        // Plugin deactivation hook
    }

    public static function a360_market_guide_uninstall()
    {
        // Plugin Uninstallation hook, delete table(s) that were created with plugin?
        error_log('uninstalled');
    }

    public static function optionsPage()
    {
        ?>

        <div class="wrap">
        <?php screen_icon(); ?>
            <h2>Buyer's Guide Plugin</h2>
        <?php
        if (isset($_GET['settings-updated']) && $_GET['settings-updated']) {
            global $wp_rewrite;
            $wp_rewrite->flush_rules();
            clean_term_cache('', 'directory');
            ?>
                <span>Settings Saved.</span>
            <?php
            if (strlen(static::cookieMessage()) > 0) {
                echo static::cookieMessage();
                static::cookieMessage('', 1);
            }
        }
        ?>
            <form method="post" action="options.php" enctype="multipart/form-data">
                <input type="hidden" name="mg_save" value="saved"/>
        <?php
        settings_fields('a360marketguidesettings');
        do_settings_sections('a360marketguidesettings');
        ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Buyer's Guide Slug</th>
                        <td><input type="text" name="market_guide_slug" value="<?php echo get_option('market_guide_slug', 'buyers-guide'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Buyer's Guide Title</th>
                        <td><input type="text" name="market_guide_title" value="<?php echo get_option('market_guide_title', 'Buyer\'s Guide'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Buyer's Guide Breadcrumbs Separator</th>
                        <td><input type="text" name="market_guide_breadcrumbs_separator" value="<?php echo get_option('market_guide_breadcrumbs_separator', '/'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">DFP Leaderboard Definition</th>
                        <td><input type="text" class="large-text" name="market_guide_dfp_leaderboard" value="<?php echo get_option('market_guide_dfp_leaderboard'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">DFP Medium Rectangle Definition</th>
                        <td><input type="text" class="large-text" name="market_guide_dfp_mediumrectangle1" value="<?php echo get_option('market_guide_dfp_mediumrectangle1'); ?>" /></td>
                    </tr>
                    <!--<tr valign="top">
                        <th scope="row">Claim Listing Slug</th>
                        <td><input type="text" name="claim_slug" value="<?php echo get_option('claim_slug', 'claim'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Claim Listing Button Text</th>
                        <td><input type="text" name="claim_btn_text" value="<?php echo get_option('claim_btn_text', 'Claim This Listing'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Claim Listing Button Text</th>
                        <td><input type="text" name="claim_header_text" value="<?php echo get_option('claim_header_text', 'Claim Listing'); ?>" /></td>
                    </tr>//-->
                    <tr valign="top">
                        <th scope="row">Depth of Categories</th>
                        <td><input type="text" name="category_depth" value="<?php echo get_option('category_depth', '2'); ?>" /></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Business Listing Import XML File</th>
                        <td>
                            <input type="file" name="business_import" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Business Listing Import CSV File</th>
                        <td>
                            <input type="file" name="business_import_csv" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Category Listing Import XML File</th>
                        <td>
                            <input type="file" name="business_category_import" />
                        </td>
                    </tr>
                    <tr valign="top">
                    	<th scope="row">O&P Category import (txt)</th>
                    	<td>
                    		<input type="file" name="op_category_import" />
                    	</td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Remove and reset all business listings</th>
                        <td>
                            <select name="reset_business_listings" id="">
                                <option value="0" selected="selected">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Remove and reset all categories for listings</th>
                        <td>
                            <select name="reset_business_categories" id="">
                                <option value="0" selected="selected">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </td>
                    </tr>

                </table>

        <?php submit_button(); ?>
            </form>
        </div>

        <?php
    }

    public static function a360_action_admin_init()
    { // whitelist options
        if (is_admin()) {
            register_setting('a360marketguidesettings', 'market_guide_slug');
            register_setting('a360marketguidesettings', 'market_guide_title');
            register_setting('a360marketguidesettings', 'market_guide_dfp_leaderboard');
            register_setting('a360marketguidesettings', 'market_guide_dfp_mediumrectangle1');
            register_setting('a360marketguidesettings', 'claim_slug');
            register_setting('a360marketguidesettings', 'claim_btn_text');
            register_setting('a360marketguidesettings', 'claim_header_text');
            register_setting('a360marketguidesettings', 'market_guide_breadcrumbs_separator');
            register_setting('a360marketguidesettings', 'category_depth');

            // If XML File Upload:
            if (isset($_POST['mg_save']) && $_POST['mg_save'] == 'saved') {

                // Raise time and memory limits
                set_time_limit(0);
                ini_set('memory_limit', '256M');

                // Business listings xml file
                if (isset($_FILES) && isset($_FILES['business_import']) && $_FILES['business_import']['size'] > 0) {
                    static::a360_upload_business_xml();
                }
                if (isset($_FILES) && isset($_FILES['business_import_csv']) && $_FILES['business_import_csv']['size'] > 0) {
                	static::a360_upload_business_csv();
                }
                // Category listing xml file
                if (isset($_FILES) && isset($_FILES['business_category_import']) && $_FILES['business_category_import']['size'] > 0) {
                    static::a360_upload_category_xml();
                }
                if (isset($_FILES) && isset($_FILES['op_category_import']) && $_FILES['op_category_import']['size'] > 0) {
                	$filename = $_FILES['op_category_import']['tmp_name'];
            		static::op_category_import($filename);
                	do_action('op_category_import', $filename);
                }
			}


            // If delete all business listinings:
            if (isset($_POST['reset_business_listings']) && $_POST['reset_business_listings'] == "1") {
                // Remove and reset all business listings.
                static::a360_delete_all_business_listings();
            }
            if (isset($_POST['reset_business_categories']) && $_POST['reset_business_categories'] == "1") {
                // Remove and reset all business listings.
                static::a360_delete_all_business_terms();
            }
        }
    }

    ///////////////////////       Custom Content Types / Fields / Taxonomies       ///////////////////////

    public static function register_content_types()
    {
        $listing = new ContentType('listing', 'Listings', 'Listing', self::$subScriberArgs);
        add_action( 'init', function() {
        	register_post_status(
        		'new_listing', array(
    			'label'                     => _x( 'New Listings', 'listing' ),
    			'public'                    => true,
    			'exclude_from_search'       => false,
    			'show_in_admin_all_list'    => true,
    			'show_in_admin_status_list' => true,
    			'label_count'               => _n_noop( 'New Listings <span class="count">(%s)</span>', 'New Listings <span class="count">(%s)</span>' ),
    		));
        });

        $group = new FieldGroup('listing-info', 'Listing Info', $listing);

        $listing->add_label('menu_name', 'Buyer\'s Guide');
//        $listing->add_arg('exclude_from_search', true);
        $listing->add_arg('show_in_admin_bar', true);
        $listing->add_arg('public', true);
        //$listing->add_arg('show_in_menu', 'edit.php?post_type=listing');
        //$listing->add_arg('show_in_menu', true);
        $listing->add_arg('show_in_admin_bar', true);
        $listing->add_arg('menu_position', 5);
        $listing->add_arg('supports', array('revisions'));
        $listing->add_arg('rewrite', array(
            'slug' => get_option('market_guide_slug', 'buyers-guide').'/listing'
        ));

        new Field\Image('logo', 'Logo', $group);
        new Field\Text('companyname', 'Company Name', $group);
        $address = new Field\Address('address', 'Address', $group);
        $address->multiple(false);
        $address->arg('international', true);
        $address->arg('max_streets', 3);
        new Field\Text('phone', 'Phone Number', $group);
        new Field\Text('fax', 'Fax Number', $group);
        new Field\Text('tollfree', 'Toll-free Number', $group);
        new Field\Email('email', 'Contact Email', $group);
        new Field\Url('website', 'Website', $group);
        new Field\Url('facebook', 'Facebook', $group);
        new Field\Editor('description', 'Description', $group);
        new Field\Editor('shortdescription', 'Short Description', $group);
        new Field\Reference\User('owner_id', 'Owner ID', $group, array('subscriber'));
        $dropdown = new Field\Selection\Dropdown('listing_type', 'Listing Type', $group, array(
            'regular' => 'Regular',
            'promoted' => 'Promoted'
        ));
        $dropdown->default_value('regular');
    }

    public static function register_taxonomies()
    {
        new Taxonomy\Hierarchical('directory', 'listing', array('name' => 'Directory', 'singular_name' => 'Category', 'hiearchical' => 1), self::$subScriberArgs);
    }

    public static function custom_profile_fields($fields)
    {
        $fields['phone'] = "Phone Number";

        unset($fields['nickname']);
        if (self::is_subscriber()) {

        }
        return $fields;
    }

    public static function custom_post_save($post_id)
    {
        // Automatically make post title the business name

        global $wpdb;
        global $pagenow;


        // Ensure code only executes when submitting new post or submitting edited post.
        // @todo:  Maybe add edit.php to this for the quick edit view in admin listings.
        if ($pagenow === 'post.php') {

            if ($listingObject = Content::forge($post_id, 'listing')) {

                remove_action('save_post', __CLASS__ . '::custom_post_save');

                $terms = get_the_terms($post_id, 'directory');
                $termText = " ";
                foreach ($terms as $term) {
                    $termText .= $term->name . " ";
                }

                $listing = array(
                    'ID' => $post_id,
                    'post_title' => $listingObject->companyname,
                    'post_content' =>
                    $listingObject->address->street . ' ' .
                    $listingObject->address->locality . ' ' .
                    $listingObject->address->region . ' ' .
                    $listingObject->address->postcode . ' ' .
                    $listingObject->phone . ' ' .
                    $listingObject->fax . ' ' .
                    $listingObject->tollfree . ' ' .
                    $listingObject->email . ' ' .
                    $listingObject->website . ' ' .
                    $listingObject->facebook . ' ' .
                    $listingObject->description . ' ' .
                    $termText,
                    'post_name' => sanitize_title_with_dashes($listingObject->companyname)
                );
                $postID = wp_update_post($listing);
                add_action('save_post', __CLASS__ . '::custom_post_save');
                // */

                wp_redirect(admin_url('/edit.php?post_type=listing', 'http'), 301);
            }
        }
    }

    public static function a360_get_business_listings($query)
    {

        if ($query->is_main_query() && $query->get('post_type') === 'listing' && !$query->is_single()) {
            $query->set('orderby', 'post_title');
            $query->set('order', 'asc');
//            $query->set('posts_per_page', 40);
        }


        return $query;

        $isMain = $query->is_main_query();
        $postType = $query->get('post_type');
        if (($query->is_main_query()) && ($query->get('post_type') == 'listing')) {

            $currentTerm = $query->get('directory');

            $promotedPosts = new \WP_Query(array(
                'post_type' => 'listing',
                'meta_query' => array(
                    array(
                        'key' => '_a360_listing_listing_type',
                        'value' => 'promoted',
                        'compare' => '='
                    )
                ),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'directory',
                        'field' => 'slug',
                        'terms' => $currentTerm,
                        'operator' => 'IN'
                    )
                )
            ));
            $promotedPostIDs = array();
            foreach ($promotedPosts->posts as $thisPost) {
                $promotedPostIDs[] = $thisPost->ID;
            }
            static::$promotedPostIDs = $promotedPostIDs;
            $query->set('post__not_in', $promotedPostIDs);
            $query->set('order', 'ASC');

            /*
              $query->set('posts_per_page', '10');
              $query->set('meta_key', '_a360_listing_listing_type');
              $query->set('orderby', 'meta_value');
              $query->set('order', 'DESC');
              /*
              $query->set('meta_query', array(
              'relation'  => 'or',
              array(
              'key'       =>  '_a360_listing_listing_type',
              'value'     =>  'promoted',
              'compare'   =>  '='
              )
              ));
              // */
        }
    }

    ///////////////////////       Rewrite Handling       ///////////////////////
    // Handles custom rewrites and routing to custom template files inside plugin/template directory
    public static function a360_rewrites()
    {
        global $wp;

        self::$requestString = $wp->request;

        // Claim Form
        self::a360_rewrite(get_option('claim_slug', 'claim'), function() {

            status_header(200);

            Guide::a360_custom_template('claim.php');
        });

        // Market Guide Listing
        self::a360_rewrite(get_option('market_guide_slug', 'buyers-guide'), function() {

            global $wp, $wp_query;

            if (\A360\Guide::test_404()) {
                status_header(404);
                include(get_404_template());
                die();
            }

            is_404() and add_filter('wpseo_title', 'A360\Guide::assignPageTitle');

            $single = is_single();
            $archive = is_archive();
            $error404 = is_404();
            $cat = is_category();
            $directorySet = ($wp->query_vars['directory'] ? $wp->query_vars['directory'] : false );
            $directoryStuff = (
                (isset($wp->query_vars['directory']) == true) and
                ( ($wp->query_vars['directory'] !== get_option('market_guide_slug', 'market-guide')) == true )
                );

            status_header(200);

            // Add parent categories to title
            if (is_archive() && ($obj = get_queried_object()) && $obj->taxonomy == 'directory' && !empty($obj->parent)) {
                add_filter('wpseo_title', function($title) use($obj) {
                    while ($obj->parent && ($term = get_term($obj->parent, 'directory'))) {
                        $title = explode(' | ', $title);
                        array_splice($title, count($title)-1, 0, $term->name);
                        $title = implode(' | ', $title);
                        $obj = $term;
                    }
                    return $title;
                });
            }

            if (is_single()) {
                Guide::a360_custom_template('single-listing.php');
                die();
            }

            // Search function
            if (isset($wp->query_vars["s"]) && $wp->query_vars["s"] != null) {

                add_filter('get_pagenum_link', function($link) {
                    $url = parse_url($link);
                    parse_str($url['query'], $query);
                    if (preg_match('#page/(\d+)#', $link, $matches)) {
                        $link = str_replace('/page/'.$matches[1], '', $link);
                        $query['page'] = $matches[1];
                    } else {
                        unset($query['page']);
                    }
                    $link = explode('?', $link);
                    return $link[0] . '?' . http_build_query($query);
                });

                global $wp_query, $wp_the_query;

                $page = $wp->query_vars['paged'];
                if (preg_match('#/page/(\d+)#', $_SERVER['REQUEST_URI'], $matches)) {
                    list(,$page) = $matches;
                } elseif (isset($_GET['page'])) {
                    $page = intval($_GET['page']);
                }

                $wp_query = $wp_the_query = new \WP_Query(array(
                    'post_type' => 'listing',
                    's' => $wp->query_vars['s'],
                    'paged' => $page
                ));

                Guide::a360_custom_template('archive-listing.php');
            } else {

                Guide::a360_custom_template('archive-listing.php');
            }
        }, true);


        global $wp;

        //A Specific Custom Post Type
        if (isset($wp->query_vars['post_type']) and $wp->query_vars["post_type"] == 'listing') {

            self::$active = true;

            status_header(200);

            if (is_archive()) {

                $templatefilename = 'archive-listing.php';
            }
            if (is_single()) {

                $templatefilename = 'single-listing.php';
            }

            Guide::a360_custom_template($templatefilename);
        }
    }

    public static function test_404()
    {

        global $wp;

        if (
            (get_queried_object() == null) and
            (is_single() == false) and
            (is_search() == false) and
            (is_category() == false) and
            (is_404() == true) and
            (
            (isset($wp->query_vars['directory']) == true) and
            ( ($wp->query_vars['directory'] !== get_option('market_guide_slug', 'market-guide')) == true )
            )
        ) {

            self::$is404 = true;

            return true;
        }

        return false;
    }

    public static function a360_custom_template($fileName, $forcePlugin = false)
    {

        $plugindir = dirname(__FILE__);

        if ($forcePlugin) {

            // Allow plugin to skip theme check
            $return_template = $plugindir . '/template/' . $fileName;
        } else {

            if (file_exists(STYLESHEETPATH . '/' . $fileName)) {

                // Child theme path file
                $return_template = STYLESHEETPATH . '/' . $fileName;
            } elseif (file_exists(TEMPLATEPATH . '/' . $fileName)) {

                // Parent Theme Path File
                $return_template = TEMPLATEPATH . '/' . $fileName;
            } else {

                // Plugin Template Path
                $return_template = $plugindir . '/template/' . $fileName;
            }
        }

        include($return_template);

        die();
    }

    public static function a360_rewrite($slug, $callback, $starts_with = false)
    {

        global $wp;
        if (!($requestString = (string) $wp->request) or empty($slug)) {
            return false;
        }

        if ($starts_with) {
            $matches = (strpos($requestString, $slug) === 0);
        } else {
            $matches = (strpos($requestString, $slug) !== false);
        }

        if ($matches) {
            self::$active = true;
            $callback();
        }
    }

    ///////////////////////       UI Changes       ///////////////////////
    // Remove Dashboard Link in Admin Menu
    public static function admin_menu_remove_dashboard()
    {
        remove_menu_page('index.php');
    }

    // Icon for Market Guide inside wordpress Admin Menu
    public static function a360_admin_css()
    {
        ?>
        <style type="text/css">
            #adminmenuwrap #adminmenu #menu-posts-listing a div.wp-menu-image {
                background-image: url('/wp-content/plugins/a360-market-guide/menu_icon.png');
                background-size: 20px 20px;
                background-position: center center;
            }
        </style>
        <?php
    }

    // Widget for promoted Listings
    public static function a360_widgets()
    {
        register_widget('A360\Promoted_Listings_Widget');
    }

    // Customize Login Form via: CSS / JS
    public static function login_logo()
    {
        ?>

        <link rel='stylesheet' id='login_page'  href='<?php echo get_stylesheet_directory_uri(); ?>/style.css' type='text/css' media='all' />

    <?php
    }

    public static function UI_subscriber_meta_boxes()
    {
        // Remove Slug box from UI
        remove_meta_box('slugdiv', 'listing', 'normal');
    }

    public static function UI_everyone_meta_boxes()
    {
        remove_meta_box('_a360[owner_id]', 'listing', 'normal');
        //remove_meta_box('slugdiv', 'listing', 'normal');
    }

    public static function marketGuideSettingsMenu()
    {
        //add_menu_page('Market Guide Settings', 'Buyer\'s Guide Settings', 'administrator', __FILE__, __CLASS__.'::optionsPage');
        add_submenu_page('options-general.php', 'Buyer\'s Guide Settings', 'Buyer\'s Guide Settings', 'administrator', __FILE__, __CLASS__ . '::optionsPage');
//        add_submenu_page('edit.php?post_type=listing', 'Buyer\'s Guide Settings', 'Buyer\'s Guide Settings', 'administrator', __FILE__, __CLASS__ . '::optionsPage');
    }

    public static function marketGuideSubscriberMenu()
    {
        // Add custom menu for subscribers
        //add_submenu_page('edit.php?post_type=listing', 'Buyer\'s Guide Settings', 'Buyer\'s Guide Settings', 'subscriber', __FILE__, __CLASS__.'::optionsPage');
    }

    public static function remove_profile_fields()
    {
    	wp_enqueue_script('remove_profile_fields', plugins_url('assets/js/remove_profile_fields.js', __FILE__));
    }

    public static function only_view_own_posts($wp_query)
    {
        // Allow subscriber users to only view their own posts:
        if (strpos($_SERVER['REQUEST_URI'], '/wp-admin/edit.php?post_type=listing') !== false) {
            global $current_user;
            $wp_query->set('author', $current_user->id);
        }
    }

    ///////////////////////       Database Hooks       ///////////////////////
    // Delete's all listing posts - business listings
    public static function a360_delete_all_business_listings($failures = 0)
    {

        $failures = 0;
        $success = 0;

        $listings = get_posts(array(
            'post_type' => 'listing',
            'posts_per_page' => 2000
        ));
        foreach ($listings as $listing) {
            $ID = $listing->ID;

            if (wp_delete_post($ID) === false) {
                $failures++;
            } else {
                $success++;
            }
        }
        static::cookieMessage($success . " business listings deleted (" . $failures . " failed).");
    }

    // Delete all custom category terms for listing post_type
    public static function a360_delete_all_business_terms()
    {

        $failures = 0;
        $success = 0;

        $terms = get_terms('directory', array(
            'hide_empty' => false
        ));
        foreach ($terms as $term) {
            $ID = $term->term_id;
            if (wp_delete_term($ID, 'directory') === false) {
                $failures++;
            } else {
                $success++;
            }
        }
        static::cookieMessage($success . " category listings deleted (" . $failures . " failed).");
    }

    ///////////////////////       Data Handling / Manipulation       ///////////////////////

    protected static function file_get_contents_utf8($fn)
    {
        $content = file_get_contents($fn);

        return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
    }

    public static function a360_upload_category_xml()
    {
//        register_shutdown_function(function() {
//            var_dump(error_get_last());
//        });

        header('Content-type: text/plain');
        if (!static::is_File_XML('business_category_import')) {
            static::cookieMessage('Failed XML Validation');
            return false;
        }
        static::cookieMessage('Passed XML Validation');
        $error = "";
        $filename = $_FILES['business_category_import']['tmp_name'];
        if ($data = static::file_get_contents_utf8($filename)) {

            $data = static::a360_xml_setup($data, 'category');

            libxml_use_internal_errors(true);
            $data = iconv('UTF-8', 'UTF-8//IGNORE', $data);
            $sxml = simplexml_load_string($data);
            if (!$sxml) {
                var_dump(libxml_get_errors());
                exit;
            }
            if ($sxml) {

                global $wpdb;
                $message = "";
                $totalCompanies = 0;
                $termsClearedSuccessfully = 0;

                $posts = get_posts(array('post_type' => 'listing', 'posts_per_page' => 2000));
                $companies = array();
                foreach ($posts as $c) {
                    $companies[static::trimmedString($c->post_title)] = $c->ID;
                    $status = wp_set_post_terms($c->ID, array(), 'directory', false);

                    $message .= static::parse_term_insertion_response($status, $c->ID, $termsClearedSuccessfully);
                    $totalCompanies++;
                }
                if ($termsClearedSuccessfully) {
                    static::cookieMessage($totalCompanies . " total companies read from database");
                    static::cookieMessage($termsClearedSuccessfully . " posts have been cleared of their terms");
                }
                unset($posts);

                $companiesCategorized = 0;
                $categoriesParsed = 0;
                $parentCategoriesInserted = 0;
                $categoriesInserted = 0;

                foreach ($sxml as $row) {

                    if ($row->getName() == 'category') {

                        $thisTerm = static::trimmedString($row);

                        // Is there a subcategory?
                        if (strpos($thisTerm, ':') != false) {

                            // Sub category exists
                            $terms = explode(':', $thisTerm, 2);
                            $parentTermString = $terms[0];
                            $thisTerm = ucwords($terms[1]);
                            if (strpos($parentTermString, '|') != false) {
                                $parentTermString = str_replace('|', ':', $parentTermString);
                            }

                            // Add Parent Term if doesn't exist
                            if(!($parentTermObj = term_exists(htmlentities($parentTermString), 'directory', 0))) {
                                $parentTermObj = wp_insert_term( $parentTermString, 'directory');
								$message .= static::parse_term_insertion_response($parentTermObj, -1, $parentCategoriesInserted) . " - parent Category: - " . $parentTermString . '(' . $parentCatID . "), thisTerm: " . $thisTerm . "(" . $thisTermID . ")";
                            } else {
								$message .= static::parse_term_insertion_response($parentTermObj);
							}
							if(is_wp_error($parentTermObj)) {

								static::cookieMessage('Error: parent Category: - ' . $parentTermString . '(' . $parentCatID . "), thisTerm: " . $thisTerm . "(" . $thisTermID . ")");
								static::parse_wp_errors($parentTermObj);

							} else {
								$parentCatID = $parentTermObj['term_id'];
							}

                            unset($parentTermObj);
                        } else {
                            $parentCatID = 0;
                        }

                        //if(!($currentTerm = get_term_by('name', $thisTerm, 'directory', ARRAY_A))) {
                        if (!($currentTerm = term_exists(htmlentities($thisTerm), 'directory', $parentCatID))) {
                            $currentTerm = wp_insert_term($thisTerm, 'directory', array('parent' => $parentCatID));
                            $message .= static::parse_term_insertion_response($parentTermObj, -1, $categoriesInserted);
                        }


                        //}
                        if (is_wp_error($currentTerm)) {
                            $thisTermID = $currentTerm->error_data['term_exists'];
                        } else {
                            $thisTermID = $currentTerm['term_id'];
                        }
                    } elseif ($row->getName() == 'company') {
                        $postID = $companies[static::trimmedString($row)];
                        if ($postID) {
                            $status = wp_set_post_terms($postID, $thisTermID, 'directory', true);
                            $message .= static::parse_term_insertion_response($status, $postID, $companiesCategorized);
                        }
                    }
                }
            } else {
                $error .= "Failed loading xml string into SimpleXML Object \n";
                foreach (libxml_get_errors() as $err) {
                    $error .= $err->message . "\n";
                }
                static::cookieMessage($error);
            }
        }
        static::cookieMessage($message);
        clean_term_cache('', 'directory');
    }

    public static function cookieMessage($message = null, $expire = false)
    {
        empty($expire) and $expire = (time() + 600);

		if($message === false && !headers_sent()) {
			setcookie("a360_mg_message", null, (time()-6000));
		}

		if($message === null) {
			return $_COOKIE["a360_mg_message"];
		}

        if ($message === null) {
            return $_COOKIE["a360_mg_message"];
        }

        if ($message) {
            if (strlen($_COOKIE["a360_mg_message"]) > 0) {
                setcookie("a360_mg_message", $_COOKIE["a360_mg_message"] . $message . "\n<br />\n", time() + 600);
                return true;
            } else {
                setcookie("a360_mg_message", "<p>" . $message . "</p>", $expire);
                return true;
            }
        }

        return false;
    }

    public static function a360_upload_business_xml()
    {
//        header('Content-type: text/plain');

        if (!static::is_File_XML('business_import')) {
            static::cookieMessage('Not Valid XML');
            return false;
        }

        static::cookieMessage('Passed XML Validation');

        $message = "";
        $companyCount = 0;
        $error = "";

        $filename = $_FILES['business_import']['tmp_name'];
        if ($data = static::file_get_contents_utf8($filename)) {


//			$data = htmlentities($data, ENT_XML1, 'UTF-8');
//			echo $data; exit;

            $data = static::a360_xml_setup($data);

            libxml_use_internal_errors(true);
            $data = iconv('UTF-8', 'UTF-8//IGNORE', $data);
            $sxml = simplexml_load_string($data);
            if (!$sxml) {
                var_dump(libxml_get_errors());
                exit;
            }
            if ($sxml) {
                foreach ($sxml as $company) {

//					if (strpos($company->companyname, "Aerocrine") !== false)
//					{
//						var_dump((string)$company->description);
//						var_dump($company); exit;
//					}


                    if (($current = get_page_by_title(static::trimmedString($company->companyname), OBJECT, 'listing'))) {
                        $ID = $current->ID;
                    } else {
                        $ID = 0;
                    }


                    $record = Content::forge($ID, 'listing');
                    $record->companyname = static::trimmedString($company->companyname);
                    $address = array();

                    $street = explode(PHP_EOL, static::trimmedString($company->address->street));

                    $address['street'] = static::trimmedString($street[0]);
                    !empty($street[1]) && $address['street2'] = static::trimmedString($street[1]);
                    !empty($street[2]) && $address['street3'] = static::trimmedString($street[2]);
                    $address['locality'] = static::trimmedString($company->address->locality);
                    $address['region'] = static::trimmedString($company->address->region);
                    $address['country'] = static::trimmedString($company->address->country);
                    $address['postcode'] = static::trimmedString($company->address->postcode);
                    $record->address = $address;
                    $record->phone = static::trimmedString($company->phone);
                    $record->fax = static::trimmedString($company->fax);
                    $record->tollfree = static::trimmedString($company->tollfree);
                    $record->email = static::trimmedString($company->email);
                    $record->website = static::trimmedString($company->website);
                    $record->description = static::trimmedString($company->description);
                    $record->post_title = $record->companyname;
                    $record->post_slug = sanitize_title_with_dashes($record->companyname);
                    $record->post_status = "publish";
                    $asdf = $record->save();
                    $fdsa = $asdf;
                    $companyCount++;
                }
                $message .= "<p>" . $companyCount . " Total Companies Processed</p>";
            } else {
                $error .= "Failed loading xml string into SimpleXML Object \n";
                foreach (libxml_get_errors() as $err) {
                    $error .= $err->message . "\n";
                }
            }
        } else {
            $error .= "Failed getting contents of xml\n";
        }
        if ($message) {
            static::cookieMessage($message);
        }

        if ($error) {
            static::cookieMessage('Errors:');
            static::cookieMessage($error);
        }
    }

    public static function a360_upload_business_csv()
    {
    	$error = '';
//        header('Content-type: text/plain');
		try {
			$foo = new Mobius365Import('business_import_csv');
			$foo->process();
	    } catch(Exception $e) {
			$error = $e->getMessage();
		}
        if ($error) {
            static::cookieMessage('Errors:');
            static::cookieMessage($error);
        } else {
            static::cookieMessage('File import completed');
        }
	}

	public static function op_category_import($filename) {
		$error = '';
		try {
			$fhandle = fopen($filename, 'r');
			$topcat = "";
			$top_id = 0;
			while (($buffer = fgets($fhandle)) !== false) {
				$cats = split(": ", $buffer);
				if ($topcat == "" || $topcat != $cats[0]) {
					// new top level category
					$topcat = $cats[0];
					$termdata = term_exists($topcat, 'directory', 0);
					if ($termdata) {
						$top_id = $termdata['term_id'];
					} else {
						$top_id = wp_insert_term($topcat, 'directory');
					}
				}
				if (count($cats)>1) {
					$ctermdata = term_exists($cats[1], 'directory');
					if ($ctermdata) {
						wp_update_term($ctermdata['term_id'], 'directory', array(
							'cat_name' => $cats[1],
							'parent'=>$top_id
						));
					} else {
						wp_insert_terms($cats[1], 'directory', array('parent'=>$top_id));
					}
				}
			}
			fclose($fhandle);
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
		if ($error) {
			static::cookieMessage('Errors:');
			static::cookieMessage($error);
		} else {
			static::cookieMessage('File import completed');
		}
	}

    ///////////////////////       Helper Functions       ///////////////////////

    public static function is_active()
    {
        if (self::$active) {
            return true;
        }
        if (get_queried_object() && get_queried_object()->name == "listing") {
            return true;
        }
        return false;
    }

    public static function is_subscriber()
    {
        // Test if role is subscriber
        $cu = wp_get_current_user();
        if (!empty($cu->roles) && is_array($cu->roles)) {
            if (in_array('subscriber', $cu->roles)) {
                return true;
            }
        }
        return false;
    }

    public static function trimmedString($string)
    {
        return trim((string) static::decode_xml_characters($string));
    }

    public static function is_File_XML($field)
    {
        // Ensure filetype is xml
        if ($_FILES[$field]['type'] != 'text/xml') {
            //$error .= "Wrong file type\n";
            return false;
        }
        // Ensure no errors when processing file
        if ($_FILES[$field]['error'] != 0) {
            //$error .= "error pulling in file\n";
            return false;
        }
        if ($_FILES[$field]['size'] < 1) {
            //$error .= "File size is less than 1\n";
            return false;
        }
        return true;
    }

    public static function a360_xml_setup($data, $type = 'business')
    {

        // Remove strange data that can only be seen with hex editor
        $data = preg_replace('#^[^<]*<#', '<', $data);

        // Prepend xml header and wrap data in object
        $data = '<?xml version="1.0" ?><data>' . $data . '</data>';

        switch ($type) {
            case 'category':
                $acceptedTags = array(
                    'company',
                    'category',
                );
                break;
            default:
                $acceptedTags = array(
                    'companyname',
                    'street',
                    'street2',
                    'street3',
                    'type',
                    'locality',
                    'region',
                    'country',
                    'postcode',
                    'tollfree',
                    'phone',
                    'fax',
                    'email',
                    'website',
                    'description',
                    'contactname',
                    'contactphone',
                    'contactemail',
                );
                break;
        }

        foreach ($acceptedTags as $tag) {
            if (preg_match_all('#<' . $tag . '>([\s\S]*?)</' . $tag . '>#', $data, $matches)) {
                foreach ($matches[1] as $i => $match) {
                    //$data = str_replace($matches[0][$i], '<'.$tag.'><![CDATA['.preg_replace('/[^!-%\x27-;=?-~ ]/eu', '"&#".ord("$0").";"', $match).']]></'.$tag.'>', $data);
                    $data = str_replace($matches[0][$i], '<' . $tag . '><![CDATA[' . htmlentities($match) . ']]></' . $tag . '>', $data);
                }
            }
        }

        //$data = static::xml_fix_characters($data);

		//		header("Content-type:text/plain");
		//		die($data);
		//		$data = static::encode_xml_characters($data);


        return $data;
    }

    public static function term_link($permalink, $term, $leavename)
    {

        $oterm = $term;
        $path = array($term->slug);
        while ($term->parent && ( $term = get_term($term->parent, $term->taxonomy) )) {
            $path[] = $term->slug;
        }

        if ('directory' == $oterm->taxonomy) {
            $path[] = get_option('market_guide_slug', 'market-guide');
        } elseif ('directory' != $oterm->taxonomy) {
            $taxonomy = get_taxonomy($oterm->taxonomy);
            $path[] = $taxonomy->rewrite['slug'];
        }
        return home_url(implode('/', array_reverse($path)));
    }

    public static function get_mg_categories()
    {

        $someObj = get_queried_object();

        if (is_object(get_queried_object())) {

            $parentId = $someObj->term_id;
        } else {

            $parentId = 0;
        }

        $catArgs = array(
            'taxonomy' => 'directory',
            'title_li' => '',
            'hierarchical' => 1,
            'hide_empty' => 1,
            'show_count' => 1,
            'depth' => get_option('category_depth', '2'),
            'child_of' => $parentId,
            'show_option_none' => '',
            'echo' => 0
        );

        return wp_list_categories($catArgs);
    }

    ///////////////////////       Breadcrumb Helper Functions       ///////////////////////

    public static function breadcrumb_link($url, $text)
    {
        if ($url != "" && $url != null) {
            return "<a class=\"crumb\" href=\"" . $url . "\">" . $text . "</a>";
        } else {
            return "<span class=\"crumb\">" . $text . "</span>";
        }
    }

    public static function getParentTerm($termObj)
    {
        $term = get_term_by('id', $termObj->parent, 'directory');
        if ($term->parent != 0) {
            $term->parentObj = static::getParentTerm($term);
        }
        return $term;
    }

    public static function termTree2HierarchicalArray($term)
    {
        $tree = array();
        while ($term) {
            $tree[] = $term->slug;
            $term = empty($term->parent) ? false : get_term($term->parent, 'directory');
        }
        return $tree;
    }

    public static function getTermTree($termObj)
    {
        $term = $termObj;
        //$term = get_term_by('id', $termObj->parent, 'directory');
        if ($term->parent != 0) {
            $term->parentObj = static::getParentTerm($term);
        }
        return $term;
    }

    public static function removeMgURI($uriArray)
    {
        $mgSlug = get_option('market_guide_slug', 'market-guide');
        foreach ($uriArray as $key => $uri) {
            if ($uri === $mgSlug) {
                unset($uriArray[$key]);
            }
        }
        return $uriArray;
    }

    public static function arrURI($URIstring)
    {
        $URIstring = str_replace('http://', '', $URIstring);
        $URIstring = str_replace('https://', '', $URIstring);
        $URIstring = str_replace($_SERVER['HTTP_HOST'] . '/', '', $URIstring);
        $URIarray = explode('/', $URIstring);
        $URIarray = array_filter($URIarray);
        return $URIarray;
    }

    public static function breadCrumbsWrap($breadcrumbs)
    {
        return "<div class=\"breadcrumbs\">" . $breadcrumbs . "</div>";
    }

    ///////////////////////       Main Breadcrumb Functions       ///////////////////////

    public static function breadcrumb($term, $slug, $preSeparator = true)
    {

        // Take term string, and uri string and append to current build breadcrumbs

        static::$breadcrumbsURI .= $slug . "/";

        if ($preSeparator) {
            static::$breadcrumbs .= " " . static::$marketGuideSeparator . " <wbr>";
        }

        static::$breadcrumbs .= '<span style="white-space: nowrap;">' . static::breadcrumb_link(static::$breadcrumbsURI, $term) . '</span>';
    }

    public static function breadcrumbsCategory()
    {

        // Build category breadcrumbs for category

        $uriSlugs = static::arrURI($_SERVER['REQUEST_URI']);

        $slugs = static::removeMgURI($uriSlugs);

        foreach ($slugs as $slug) {

            $termName = get_term_by('slug', $slug, 'directory');

            if ($termName) {

                static::breadcrumb($termName->name, $slug);
            }
        }

        echo static::breadCrumbsWrap(static::$breadcrumbs);
    }

    public static function breadcrumbListing()
    {

        $uriSlugs = static::arrURI($_SERVER['HTTP_REFERER']);

        $slugs = static::removeMgURI($uriSlugs);

        $revSlugs = array_reverse($uriSlugs);

        $term = get_term_by('slug', $revSlugs[0], 'directory');

        $thisPostTerms = wp_get_post_terms(get_the_ID(), 'directory');

        $match = 0;

        foreach ($thisPostTerms as $thisTerm) {
            if ($thisTerm->slug === $term->slug) {
                $match = 1;
            }
        }

        if (!$match) {
            $termTree = static::getTermTree($thisPostTerms[0]);
            $slugs = static::termTree2HierarchicalArray($termTree);
        }

        foreach ($slugs as $slug) {

            $term = get_term_by('slug', $slug, 'directory');

            if ($term) {

                static::breadcrumb($term->name, $slug);
            }
        }

        echo static::breadCrumbsWrap(static::$breadcrumbs);
    }

    public static function buildBreadCrumbs()
    {

        global $wp;

        $breadcrumbs = "";

        $referringURL = $_SERVER['HTTP_REFERER'];
        $refererURI = static::arrURI($referringURL);
        $requestURI = static::arrURI($_SERVER['REQUEST_URI']);


        // Add Top Level Market Guide Link
//        static::$breadcrumbs .= static::breadcrumb_link("/" . static::$marketGuideSlug . "/", static::$marketGuideTitle);
//		static::$breadcrumbsURI .= "/" . static::$marketGuideSlug . "/";

        static::breadcrumb(static::$marketGuideTitle, '/' . static::$marketGuideSlug, false);



        // Tests necessary to determine current breadcrumbs
        if (isset($referringURL) and $referringURL !== "" and $referringURL !== null) {
            $referer_set = true;
        } else {
            $referer_set = false;
        }

        if (in_array('listing', static::arrURI($_SERVER['REQUEST_URI']))) {
            $single_listing = true;
        } else {
            $single_listing = false;
        }

        $catRefererTest = strpos($referringURL, static::$marketGuideSlug);

        if ($catRefererTest !== false) {
            $category_referer = true;
        } else {
            $category_referer = false;
        }

        $listingRefererTest = strpos($referringURL, 'listing');

        if ($listingRefererTest !== false) {
            $listing_referer = true;
        } else {
            $listing_referer = false;
        }



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ($single_listing) {
            static::breadcrumbListing();
            return;
        } else {
            static::breadcrumbsCategory();
            return;
        }


//		static::breadcrumbsCategory();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//		// Tests to execute the route of breadcrumbs.
//
//		$CurrentURIarray = static::arrURI($_SERVER['REQUEST_URI']);
//
//		if(count($CurrentURIarray) == 1) {
//			// Top level buyers guide, no extra breadcrumbs.
//			echo static::breadCrumbsWrap($breadcrumbs);
//			return;
//
//		}
//
//		if($single_listing and $listing_referer) {
//
//			// Clicked on promoted widget from a single listing page. Build out breadcrumbs straight from database;
//			static::buildBreadCrumbsSinglePost($breadcrumbs);
//			return;
//
//		}
//
//
//
//
//		if($referer_set and !$listing_referer and !$category_referer) {
//			static::buildBreadCrumbsFromRequestString($breadcrumbs, $requestURI);
//			return;
//		}
//
//		if($referer_set and !$listing_referer and $category_referer) {
//			static::buildBreadCrumbsFromRequestString($breadcrumbs, $refererURI);
//			return;
//		}
//
//
//
//
//		if(!$single_listing and $listing_referer) {
//
//			// Navigated back up the breadcrumbs from single listing to category listing
//
//
//		}
//
//		if($single_listing and $category_referer) {
//
//		}
//
//		if($single_listing and !$category_referer) {
//
//			// Single Listing
//			static::buildBreadCrumbsSinglePost($breadcrumbs);
//			return;
//		}
//
//        static::buildBreadCrumbsFromRequestString($breadcrumbs, $requestURI);
    }

    public static function parseCompany($company)
    {
        $logo = ($company->logo);
        $name = ($company->companyname);
        $description = ($company->description);
        $shortdescription = ($company->shortdescription);
        $street = ($company->address->street);
        $locality = ($company->address->locality);
        $region = ($company->address->region);
        $postcode = ($company->address->postcode);
        $email = ($company->email);
        $website = ($company->website);
        $phone = ($company->phone);
        $fax = ($company->fax);
        $listing_type = ($company->listing_type);

        if (count($website) > 0 && strpos($website, 'http') === false) {
            $website = 'http://' . $website;
        }

        return compact(
            'logo', 'name', 'description', 'shortdescription', 'street', 'locality', 'region', 'postcode', 'email', 'website', 'phone', 'fax', 'listing_type'
        );
    }

    public static function assignPageTitle()
    {
        $someObj = get_queried_object();

        if (!is_object(get_queried_object())) {
            return get_option('market_guide_title', 'Market Guide');
        }

        if (is_single()) {
            return $someObj->post_title;
        }

        if (is_search()) {
            return 'Search Results';
        }

        if ($someObj->name) {
            return $someObj->name;
        }
        return;
    }

    ///////////////////////       Code written, but not used or fully functional       ///////////////////////
    // This isn't useful or used yet.
    public static function a360_custom_search($query)
    {
        // Ensure global search doesn't show business listings from the market guide.
        if (!is_admin() && $query->is_search() && $query->is_main_query() && $query->get('post_type') !== 'listing') {
            $type = $query->get('post_type');
            if ($type == '' or $type == 'any')
            {
                $types = get_post_types( array('exclude_from_search' => false));
                unset($types['listing']);
                $query->set('post_type', $types);
            }
        }
    }

    function redirect_after_post_save($location)
    {
        if (isset($_POST['save']) || isset($_POST['publish'])) {
            if (preg_match("/post=([0-9]*)/", $location, $match)) {
                $pl = get_permalink($match[1]);
                if ($pl) {
                    wp_redirect($pl);
                }
            }
        }
    }

    // This doesn't work
    public static function hide_custom_fields($protected, $meta_key)
    {
        //return $meta_key == '_a360[owner_id]' ? true : $protected;
    }

    public static function encode_xml_characters($string)
    {
        return str_replace('&', '__ESCAPED_AMP__', $string);
    }

    public static function decode_xml_characters($string)
    {
        return str_replace('__ESCAPED_AMP__', '&', $string);
    }

    public static function xml_fix_characters($xmlString)
    {
        $xmlString = str_replace('&reg;', '&#174;', $xmlString);
        $xmlString = str_replace('&bull;', '&#149;', $xmlString);
        $xmlString = str_replace('&trade;', '&#153;', $xmlString);
        $xmlString = str_replace('&copy;', '&#169;', $xmlString);
        $xmlString = str_replace('&auml;', '&#196;', $xmlString);
        $xmlString = str_replace('&eacute;', '&#201;', $xmlString);
        $xmlString = str_replace('&ouml;', '&#214;', $xmlString);
//		$xmlString = str_replace('\xC2;', 		'', 		$xmlString);
		$xmlString = str_replace('&Acirc;;', 	'', 		$xmlString);
		return $xmlString;
	}


	public static function parse_term_insertion_response($status, $ID = -1, &$successCount = 0) {
		$message = "";
		switch(true) {
			case ($status === false):
				$message .= "wp_set_post_terms == false - " . $ID . "\n<br />\n";
				break;
			case is_wp_error($status):
				$errorCodes = $status->get_error_codes();
				foreach($errorCodes as $eCode) {
					$eMessages = $status->get_error_messages($eCode);
					if(is_array($eMessages)) {
						foreach($eMessages as $eMsg) {
							$message .= $eMsg . "\n<br />\n";
						}
					} else {
						$message .= $eMsg . "\n<br />\n";
					}
				}
				break;
			case is_string($status):
				$message .= "First offending term: " .  $ID;
				break;
			case is_array($status):
				$successCount++;
				break;
		}
		return $message;

	}

	public static function parse_wp_errors($error) {
		$message = "";
		$errorCodes = $error->get_error_codes();
		foreach($errorCodes as $eCode) {
			$eMessages = $error->get_error_messages($eCode);
			if(is_array($eMessages)) {
				foreach($eMessages as $eMsg) {
					$message .= $eMsg . "\n<br />\n";
				}
			} else {
				$message .= $eMsg . "\n<br />\n";
			}
		}
		static::cookieMessage($message);
	}


}

class Promoted_Listings_Widget extends \WP_Widget
{

    public function __construct()
    {
        parent::__construct(
            'promoted_listings_widget', // Base ID
            __('Promoted Listings', 'text_domain'), // Name
            array('description' => __('A Widget of Promoted ' . get_option('market_guide_title', 'Market Guide') . ' Listings', 'text_domain'),) // Args
        );
    }

    public function widget($args, $instance)
    {
        global $wpdb;

        $limit = isset($instance['limit']) ? $instance['limit'] : 5;

        $query = new \WP_Query(array(
            'post_type' => 'listing',
            'post_status' => 'publish',
            'posts_per_page' => $limit,
            'orderby' => 'rand',
            'meta_query' => array(
                array(
                    'key' => '_a360_listing_listing_type',
                    'value' => 'promoted',
                    'compare' => '=',
                ),
            ),
        ));

        if ($query->have_posts()) {
            echo $args['before_widget'];

            if (!empty($instance['title'])) :
                ?>
                <h3 class="widget-title"><?= $instance['title']; ?></h3>
                <?php
            endif;

            $postCount = 0;
            while ($query->have_posts()) {
                $query->the_post();
                $company = \A360\Core\Content::forge(get_the_ID());
                $permaLink = get_permalink(get_the_ID());
                if ($postCount > 0):
                    ?>
                    <hr />
                    <?php
                else:
                    $postCount++;
                endif;
                ?>
                <section>
                    <header><a href="<?php echo $permaLink; ?>"><?php echo $company->companyname; ?></a></header>
                    <article>
                <?php if ($company->logo != null) {
                    ?><a href="<?php echo $permaLink; ?>">
                                <img src="<?php echo $company->logo; ?>" alt="<?php echo $company->companyname; ?>" title="<?php echo $company->companyname; ?>"/>
                            </a><?php }
                ?>
                <?php if ($company->shortdescription != null) {
                    echo $company->shortdescription . "<br />";
                } ?>
                    </article>
                </section>
                <?php
            }
            echo $args['after_widget'];
        }



        /*
          if($promotedPosts && count($promotedPosts) > 0) {
          $title = apply_filters( 'widget_title', $instance['title'] );

          if ( ! empty( $title ) )
          echo $args['before_title'] . $title . $args['after_title'];
          foreach($promotedPostIDs as $postID) {
          $company = \A360\Core\Content::forge($postID);
          ?>
          <section style="background-color: #EEEEEE">
          <h5><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php echo $company->companyname; ?></a></h5>
          <address>
          <?php if($company->logo != null) { echo $company->logo . "<br />"; } ?>
          <?php if( $company->description != null ) { echo $company->description . "<br />"; } ?>
          </address>
          </section>
          <?php
          }

          }
          // */
    }

    public function form($instance)
    {

        $instance = array_merge(array(
            'title' => __('Promoted Listing', 'text_domain'),
            'limit' => 3,
            'rotate' => true,
            ), $instance);

        extract($instance, EXTR_SKIP);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Post Limit:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo esc_attr($limit); ?>" />
        </p>
        <p>
            <input class="checkbox" type="checkbox" <?php checked($rotate); ?> id="<?php echo $this->get_field_id('rotate'); ?>" name="<?php echo $this->get_field_name('rotate'); ?>" value="1" />
            <label for="<?php echo $this->get_field_id('rotate'); ?>"><?php _e('Rotate?'); ?></label>
        </p>
                <?php
            }

            public function update($new_instance, $old_instance)
            {
                $instance = array();
                $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : __('Promoted Listing', 'text_domain');
                $instance['limit'] = (!empty($new_instance['limit']) ) ? absint($new_instance['limit']) : 3;
                $instance['rotate'] = !empty($new_instance['rotate']);

                return $instance;
            }

        }

        Guide::init();

        register_activation_hook(__FILE__, array('A360\Guide', 'a360_market_guide_activate'));
        register_deactivation_hook(__FILE__, array('A360\Guide', 'a360_market_guide_deactivate'));
        register_uninstall_hook(__FILE__, array('A360\Guide', 'a360_market_guide_uninstall'));











