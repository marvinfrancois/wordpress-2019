<?php
error_reporting(E_ERROR);
ini_set('display_errors',true);
/*
  Plugin Name: A360 Google Tag Manager
  Description: Site Tags and Ads
  Version: 0.1
  Author: Marvin Francois
  Author URI: http://www.allied360.com/
 */

if (!defined('WP_CONTENT_URL'))
    define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if (!defined('WP_CONTENT_DIR'))
    define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if (!defined('WP_PLUGIN_URL'))
    define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');
if (!defined('WP_PLUGIN_DIR'))
    define('WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins');

function activate_googletag() {
    add_option('google_tag_id', 'GTM-xxxxxxxxx');
    add_option('google_ad_id', 'xxxxxxxxx/xx_xxxxxx');
}

function deactive_googletag() {
    delete_option('google_tag_id');
    delete_option('google_ad_id');
}

function admin_init_googletagmanager() {
    register_setting('googletagmanager', 'google_tag_id');
    register_setting('googletagmanager', 'google_ad_id');
}

function admin_menu_googletagmanager() {
    add_options_page('Google Tags and Ads', 'Google Tags and Ads', 'manage_options', 'googletagmanager', 'options_page_googletagmanager');
}

function options_page_googletagmanager() {
    include(WP_PLUGIN_DIR.'/a360_tags/options.php');
}

register_activation_hook(__FILE__, 'activate_googletagmanager');
register_deactivation_hook(__FILE__, 'deactive_googletagmanager');

if (is_admin()) {
    add_action('admin_init', 'admin_init_googletagmanager');
    add_action('admin_menu', 'admin_menu_googletagmanager');
}

function head_adcode(){
    $googleCode='117991616/PSP_Interstitial';
    ?>
    <script>
        /*
        googletag.defineOutOfPageSlot('/117991616/PSP_Interstitial', 'ad-interstitial').addService(googletag.pubads());
        */
        require(['ads'], function(Ads) {
            Ads.define('PSP_300x80',    [300,80],   'ad-med-rect-small', 'smallrec');
            Ads.define('PSP_300x250_1', [300, 250], 'ad-med-rect-1', 'rectangle');
            Ads.define('PSP_300x250_2', [300, 250], 'ad-med-rect-2', 'rectangle');
            Ads.define('PSP_728x90', [728, 90], 'ad-leaderboard', 'leaderboard');
            Ads.define('PSP_940x60', [940, 60], 'ad-banner', 'banner');

            <?php foreach (apply_filters('verge_ad_targeting', array()) as $key => $value) : ?>
            Ads.setPageLevelTargeting('<?= $key; ?>', <?= json_encode($value); ?>);
            <?php endforeach; ?>
        });
    </script>
    <?
}

function head_hashtags() {
        $tags = new Tags();
    $map = json_encode($tags->Tags());
    ?>
    <script type="text/javascript">
        (function doHashTag() {
            if (window.location.hash) {
                var map = <?= $map; ?>;
                if (map.hasOwnProperty(window.location.hash)) {
                    window.location.href = map[window.location.hash];
                }
            }
            window.onhashchange = doHashTag;
        }());
    </script>
    <?php
}


class Tags
{


    public function Ads()
    {

    }


    public  function Tags()
    {
        if (!($map = wp_cache_get('hashtags'))) {

            $query = new WP_Query(array(
                'posts_per_page' => -1,
                'post_type' => 'any',
                'meta_query' => array(
                    array(
                        'key' => '__hashtag',
                        'value' => '',
                        'compare' => '!='
                    )
                )
            ));

            $map = array();

            while ($query->have_posts()) {
                $query->the_post();
                $map['#' . ltrim(current(get_post_meta(get_the_ID(), '__hashtag')), '#')] = post_permalink();
            }

            wp_reset_postdata();
            wp_cache_set('hashtags', $map, 'default', 3600); // 1 hour TTL
        }

        return $map;
    }
}



add_action('wp_head', 'head_hashtags');
add_action('wp_head', 'head_adcode');

