class Ads{
    var googletag
}


// define(function() {
//
//     var Ads = {};
//
//     Ads.breakpoints = {
//         mobile: [320,0],
//         tablet: [768,0],
//         desktop: [960,0]
//     };
//
//     Ads.mappings = {
//         leaderboard: [
//             [Ads.breakpoints.tablet, [ [728,90] ]],
//             [Ads.breakpoints.mobile, [ [320,50] ]]
//         ],
//         rectangle: [
//             [Ads.breakpoints.tablet, [ [300,250] ]],
//             [Ads.breakpoints.mobile, [ [300,100], [300,250] ]]
//         ],
//         smallrec: [
//             [Ads.breakpoints.tablet, [ [300,80] ]],
//             [Ads.breakpoints.mobile, [ [300,80], [300,80] ]]
//         ],
//         banner: [
//             [Ads.breakpoints.desktop, [ [940,60] ]],
//             [Ads.breakpoints.tablet,  [ [728,90] ]],
//             [Ads.breakpoints.mobile,  [ [320,50] ]]
//         ],
//         interstitial: [
//             [Ads.breakpoints.tablet, [ [640,480] ]]
//         ]
//     };
//
//     Ads.slots = {};
//
//     Ads.define = function (name, defaultSize, id, mapping) {
//         googletag.cmd.push(function() {
// //            console.log('define slot', name, defaultSize, id);
//
//             Ads.slots[id] = googletag.defineSlot('/117991616/'+name, defaultSize, id);
//
//             if (mapping) {
//                 Ads.slots[id].defineSizeMapping(Ads.mappings[mapping]);
//             }
//
//             switch (id) {
//                 case 'ad-leaderboard':
//                     Ads.slots[id].setCollapseEmptyDiv(true);
//                     break;
//             }
//
//             Ads.slots[id].addService(googletag.pubads());
//
//             googletag.enableServices();
//         });
//     };
//
//     Ads.render = function(slot) {
//         googletag.cmd.push(function() {
// //            console.log('render slot', slot);
//             googletag.display('ad-'+slot);
//         });
//     };
//
//     Ads.refresh = function() {
// //        console.log('refresh ads!');
//         for (var i in Ads.slots) {
//             googletag.pubads().refresh([Ads.slots[i]]);
//         }
//     };
//
//     Ads.setPageLevelTargeting = function(key, value) {
//         googletag.cmd.push(function() {
//             googletag.pubads().setTargeting(key, value);
//         });
//     };
//
//     return Ads;
// });