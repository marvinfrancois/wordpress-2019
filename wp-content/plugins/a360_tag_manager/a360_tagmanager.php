<?php
/*
Plugin Name: A360 Google Tag Manager
Description: Enables Google Tag and Ad Manager on all pages.
Version: 0.8
Author: Marvin Francois
*/

if (!defined('WP_CONTENT_URL'))
    define('WP_CONTENT_URL', get_option('siteurl') . '/wp-content');
if (!defined('WP_CONTENT_DIR'))
    define('WP_CONTENT_DIR', ABSPATH . 'wp-content');
if (!defined('WP_PLUGIN_URL'))
    define('WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins');
if (!defined('WP_PLUGIN_DIR'))
    define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');
define('A360_OPTION_TRIM_METHOD','none'); // Can be back or front

function admin_init_googletagmanager()
{
    register_setting('googletagmanager', 'use_google_tags');
    register_setting('googletagmanager', 'use_google_ads');
    register_setting('googletagmanager', 'tag_id');
    register_setting('googletagmanager', 'ad_id');
    for ($x = 1; $x <= 7; $x++) {
        register_setting('googletagmanager', 'mapping_name_' . $x);
        register_setting('googletagmanager', 'mapping_sizes_' . $x);

    }


    for ($x = 1; $x <= 7; $x++) {
        register_setting('googletagmanager', 'ad_slot_name_' . $x);
        register_setting('googletagmanager', 'ad_slot_mapping_' . $x);
        register_setting('googletagmanager', 'ad_slot_default_size_' . $x);
        register_setting('googletagmanager', 'ad_slot_target_' . $x);
        register_setting('googletagmanager', 'collapse_empty_div_' . $x);
    }


}

function admin_menu_googletagmanager()
{
    add_options_page('A360 Google Tag/Ad Manager', 'A360 Google Tag/Ad Manager', 'manage_options', 'googletagmanager', 'options_page_googletagmanager');
}

function options_page_googletagmanager()
{
    include(WP_PLUGIN_DIR . '/a360_tag_manager/options.php');
}

function google_ad_manager_admin_head()
{
    google_ad_manager_head(true);
}

function google_ad_manager_head($console = false)
{
    if ($console == false && get_option('use_google_ads') === '0') {
        return;
    }
    $cats = [];
    $tags = [];
    $urls = [];
    if (have_posts()) :
        while (have_posts()) :
            the_post();
            $id = get_the_ID();
            $cat = get_the_category($id);
            $url = get_permalink($id);
            $urls[]=$url;
            foreach ($cat as $word) {
                $cats[] = "'" . $word->name . "'";
            }
            $term  = wp_get_post_terms($id);
            foreach($term as $word){
                $tags[]="'".$word->name."'";
            }
            $cats = array_unique($cats);
            $tags = array_unique($tags);
        endwhile;
    endif;
    $ad_id = get_option('ad_id');
    for ($x = 1; $x <= 4; $x++) {
        $term = trim(get_option(sprintf('mapping_name_%d', $x)));
        $size = trim(get_option(sprintf('mapping_sizes_%d', $x)));
        $size_mappings[$term] = $size;
    }

    // Google can only target for up to 40 characters of the path, so we are taking the primary categories and drilling in as tight as possible to the full path till we get over 40 chars.
    $url =  $_SERVER['REQUEST_URI'];
    $url=trim($url,"/");
    $urlList = explode("/",$url);
    if(A360_OPTION_TRIM_METHOD !='none') {
        while (count($urlList) >= 2 && strlen($url) > 40) {
            switch (A360_OPTION_TRIM_METHOD) {
                case 'back':
                    array_pop($urlList);
                    break;
                default:
                    array_shift($urlList);
                    break;
            }
            $url = implode("/", $urlList);
        }
    }

    ?>
    <script async src="https://www.googletagservices.com/tag/js/gpt.js"></script>
    <script>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
    </script>
    <script>
        var gptAdSlots = [];
        <?php
        $js = [];
        for ($x = 1; $x <= 8; $x++) {
            $slot_name = trim(sprintf(get_option('ad_slot_name_' . $x)));
            $slot_mapping = $size_mappings[trim(sprintf(get_option('ad_slot_mapping_' . $x)))];
            $slot_default_size = trim(sprintf(get_option('ad_slot_default_size_' . $x)));
            $slot_target = trim(sprintf(get_option('ad_slot_target_' . $x)));
            $collapse_empty_div = get_option('collapse_empty_div_' . $x) == '1' ? 'googletag.pubads().collapseEmptyDivs();' : '';
#echo(sprintf("--%d--",get_option('collapse_empty_div_' . $x)));
            if (trim($slot_name) && trim($slot_mapping) && trim($slot_default_size) && trim($slot_target)) {
                $js[] = sprintf("
                     googletag.cmd.push(function() {
                     var mapping%d = googletag.sizeMapping().
                     %s
                     build();

                     gptAdSlots[%s] = googletag.defineSlot('/%s/%s',  %s, '%s').
                     defineSizeMapping(mapping%d).
                     addService(googletag.pubads());
                     %s
                      googletag.enableServices();
                     });
                    "
                    , $x
                    , $slot_mapping
                    , $x
                    , $ad_id
                    , $slot_name
                    , $slot_default_size
                    , $slot_target
                    , $x
                    , $collapse_empty_div
                );
            }
        }
        $js = implode("\r\n\r\n", $js);
        echo $js;
        ?>
        googletag.cmd.push(function() {


        googletag.pubads().setTargeting('Category', [<?php echo implode(",", $cats);?>]);
        googletag.pubads().setTargeting('Keywords', [<?php echo implode(",", $tags);?>]);
        googletag.pubads().setTargeting('URL', '<?php echo $url?>');
        <?php #if ($console === true) echo 'googletag.openConsole();';?>
        window.onresize = function(event){
            googletag.pubads().refresh();
        }
        } );
    </script>
    <?php
}


function googletagmanagerhead()
{
    if (get_option('use_google_tags') === '0') {
        return;
    }
    $tag_id = get_option('tag_id');

    echo sprintf("
    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','%s');</script>
    <!-- End Google Tag Manager -->
", $tag_id);

}

function google_tag_manager_body()
{
    if (get_option('use_google_tags') === '0') {
        return;
    }
    $tag_id = get_option('tag_id');

    echo sprintf("
    <!-- Google Tag Manager -->
        <noscript>
            <iframe src='//www.googletagmanager.com/ns.html?id=%s'
                      height='0' 
                      width='0' 
                      style='display:none;visibility:hidden'>
            </iframe>
        </noscript>
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','%s');
        </script>
    <!-- End Google Tag Manager -->
    \");"
        , $tag_id
        , $tag_id
    );
    ?>

    <?php
}

function head_hash_tags()
{
    $tags = new A360_Meta();
    $map = json_encode($tags->Tags());
    echo sprintf("
    <!-- Google Tag Manager Tags -->
    <script type=\"text/javascript\">
        (function doHashTag() {
            if (window.location.hash) {
                var map = %s;
                if (map.hasOwnProperty(window.location.hash)) {
                    window.location.href = map[window.location.hash];
                }
            }
            window.onhashchange = doHashTag;
        }());
    </script>
    <!-- END Google Tag Manager Tags -->
    ", $map);
}



if (is_admin()) {
    add_action('admin_init', 'admin_init_googletagmanager');
    add_action('admin_menu', 'admin_menu_googletagmanager');
}

class A360_Meta
{
    public function Tags()
    {
        if (!($map = wp_cache_get('a360_hashtags'))) {

            $query = new WP_Query(array(
                'posts_per_page' => -1,
                'post_type' => 'any',
                'meta_query' => array(
                    array(
                        'key' => '_a360__hashtag',
                        'value' => '',
                        'compare' => '!='
                    )
                )
            ));

            $map = array();

            while ($query->have_posts()) {
                $query->the_post();
                $map['#' . ltrim(current(get_post_meta(get_the_ID(), '_a360__hashtag')), '#')] = post_permalink();
            }

            wp_reset_postdata();
            wp_cache_set('a360_hashtags', $map, 'default', 3600); // 1 hour TTL
        }

        return $map;
    }
}

add_action('wp_head', 'google_ad_manager_head');
add_action('admin_head', 'google_ad_manager_admin_head');
add_action('wp_head', 'head_hash_tags');
//add_action('wp_head','google_tag_manager_body');
?>
