<div class="wrap">
    <h2>A360 Google Tag/Ad Manager</h2>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <form method="post" action="options.php" id="gtm">
        <?php wp_nonce_field('update-options'); ?>
        <?php settings_fields('googletagmanager'); ?>
        <script>
            $(function () {
                $("#ad_manager_tabs").tabs();
            });
        </script>
        <style>
            #ad_manager_tabs table label, #gtm label {
                display: inline-block;
                min-width: 6em;
                vertical-align: top;
            }

            #ad_manager_tabs table {
                width: auto;
            }

            #gtma {
                width: 100% !important;
            }
        </style>

        <div id="ad_manager_tabs">
            <h2>Basic Settings</h2>
            <table class="form-table" id="gtma">
                <tr valign="top">
                    <td><label>Tag ID</label><input type="text" style="width:10em;" name="tag_id"
                                                    value="<?php echo get_option('tag_id'); ?>"/></td>
                    <td><label>Use GTags</label>
                        <select name="use_google_tags">
                            <option value="1">True</option>
                            <option value="0" <?php echo get_option('use_google_tags') == '0' ? 'SELECTED' : ''; ?>>
                                False
                            </option>
                        </select>
                    </td>
                    <td>
                        <div style="width:200px">&nbsp;</div>
                    </td>
                    <td><label>Ad ID</label><input type="text" style="width:10em;" name="ad_id"
                                                   value="<?php echo get_option('ad_id'); ?>"/></td>
                    <td><label>Use GAds</label>
                        <select name="use_google_ads">
                            <option value="1">True</option>
                            <option value="0" <?php echo get_option('use_google_ads') == '0' ? 'SELECTED' : ''; ?>>
                                False
                            </option>
                        </select></td>
                </tr>
            </table>
            <ul>
                <li><a href="#tabs-2">Ad Units</a></li>
                <li><a href="#tabs-3">Ad Sizes</a></li>
            </ul>

            <div id="tabs-2">
                <h2>Ad Units</h2>

                <table>
                    <?php for ($x = 1; $x <= 8; $x++) { ?>
                        <tr>
                            <th>Ad Unit #<?php echo $x; ?></th>
                            <td><label>Slot Name</label><input name="ad_slot_name_<?php echo $x; ?>"
                                                               value="<?php echo get_option('ad_slot_name_' . $x); ?>"
                                                               type="text"/></td>
                            <td><label>Size Mapping</label><input name="ad_slot_mapping_<?php echo $x; ?>"
                                                                  value="<?php echo get_option('ad_slot_mapping_' . $x); ?>"
                                                                  type="text"/></td>
                            <td><label>Default Size</label><input name="ad_slot_default_size_<?php echo $x; ?>"
                                                                  value="<?php echo get_option('ad_slot_default_size_' . $x); ?>"
                                                                  type="text"/></td>
                            <td><label>Target</label><input name="ad_slot_target_<?php echo $x; ?>"
                                                            value="<?php echo get_option('ad_slot_target_' . $x); ?>"
                                                            type="text"/></td>
                            <td><label>Collapse Empty Div</label>
                                <select name="collapse_empty_div_<?php echo $x; ?>">
                                    <option value="0">False</option>
                                    <option value="1" <?php echo get_option('collapse_empty_div_' . $x) == '1' ? 'SELECTED' : ''; ?>>
                                        True
                                    </option>
                                </select>
                            </td>
                        </tr>

                    <?php } ?>
                </table>
                <?php for ($x = 1; $x <= 8; $x++) { ?>
                    <h3><?php echo get_option('ad_slot_name_' . $x); ?></h3>
                    <div id="<?php echo get_option('ad_slot_target_' . $x); ?>">
                        <script type="text/javascript">
                            googletag.cmd.push(function () {
                                googletag.display('<?php echo get_option('ad_slot_target_' . $x);?>');
                            });
                        </script>
                    </div>
                    <hr/>
                <?php } ?>
            </div>
            <div id="tabs-3">
                <h2>Ad Sizes</h2>
                <table>
                    <?php
                    for ($x = 1; $x <= 4; $x++) {
                        $term = get_option('mapping_name_' . $x);
                        $size = get_option('mapping_sizes_' . $x);
                        echo sprintf("
        <tr>
            <td colspan='3'><hr></td>
        </tr>
        <tr style='vertical-align: top'>
            <th scope='row'>Size Mapping #{$x}</th>
            <td style='vertical-align: top'>
                <label for='mapping_name_{$x}'>Name</label><input name='mapping_name_{$x}' value='{$term}' type='text' style='width:10em'/>
            </td>
          <td style='vertical-align: top'>
                <label for='mapping_sizes_{$x}'>Sizes</label><textarea name='mapping_sizes_{$x}' style='width:30em;height:6em;'>{$size}</textarea>
            </td>
        </tr>
        ");
                    }
                    ?>
                </table>
            </div>
        </div>
        <input type="hidden" name="action" value="update"/>
        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"/>
        </p>
    </form>
</div>
