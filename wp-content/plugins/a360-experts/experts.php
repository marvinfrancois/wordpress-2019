<?php
/*
  Plugin Name: A360 Experts
  Description: Directory of Experts?
  Version: 0.1
  Author: Steven Kohlmeyer
  Author URI: http://www.allied360.com/
 */

namespace A360;

use A360\Core\Plugin;
use A360\Core\Content;
use A360\Core\ContentType;
use A360\Core\FieldGroup;
use A360\Core\Field;
use A360\Core\Shortcode;
use A360\Core\Metabox;
use A360\Core\Taxonomy;
use A360\Core\User;

if (!class_exists('\A360\Core\Plugin'))
    return;

class Experts extends Plugin
{

    public static function bootstrap()
    {
        parent::bootstrap();

        add_action('widgets_init', __CLASS__ . '::widgets');
        
        add_action('pre_get_posts', function($query) {
            
            if (is_admin() or !$query->is_main_query() or $query->get('post_type') != 'expert')
            {
                return;
            }

            $query->set('orderby', 'post_title');
            $query->set('order', 'ASC');
        });
        
        add_filter('discussion_qanda_confirmation', function() {
            return 'Thank you for submitting a question to a '.get_bloginfo('name').' expert. Please check back here for a response. Generally, responses are posted within 3 weeks.';
        });
    }

    public static function widgets()
    {
        $asdf = class_exists('\A360\Experts_Widget');
        if (!class_exists('\A360\Experts_Widget'))
            return;
        register_widget('A360\Experts_Widget');
    }

    public static function register_content_types()
    {

        $expert = new ContentType('expert', 'Experts', 'Expert');
        $fg = new FieldGroup('expert-insight', 'Expert Insight', $expert);
        
        // Copy bio to post content before programmatic save
        $expert->on('beforeSave', function($content) {
            $content->post('post_content', $content->field('expert_bio'));
            return $content;
        });

        $expert->add_label('menu_name', 'Expert Insight');
        $expert->add_arg('show_in_admin_bar', true);
        $expert->add_arg('public', true);
        $expert->add_arg('has_archive', true);
        $expert->add_arg('rewrite', array(
            'slug' => 'expert-insight'
        ));
        $expert->add_arg('menu_postition', 24);
        $expert->add_arg('supports', array('title', 'excerpt', 'comments'));

        new Field\Image('profilePic', 'Profile Pic', $fg);
        new Field\Text('fullname', 'Name', $fg);
        new Field\Text('position', 'Position', $fg);
        new Field\Text('company_name', 'Company name', $fg);
        new Field\Image('logo', 'Logo', $fg);
        new Field\Url('website', 'Website', $fg);
        new Field\Text('phone', 'Phone', $fg);
        new Field\Text('email', 'Email', $fg);
        $bio_field = new Field\Editor('expert_bio', 'Expert Bio', $fg);
        
        // Copy bio to post content after control panel save
        // @todo: better hooks for programmatic vs control panel
        add_filter( 'wp_insert_post_data' , function($data, $postarr) use($bio_field) {
            if (isset($data['post_type']) and $data['post_type'] == 'expert')
            {
                $data['post_content'] = $bio_field->get_post_values();
            }
            return $data;
        }, '99', 2 );

        if (class_exists('\A360\Discussion\Discussion')) {
            \A360\Discussion\Discussion::enableForContentType($expert);
        }

        $expert->add_method('renderProfilePic', function($expert, $link = false) {
            if ($expert->profilePic) :
                ?>
                <div class="expert-profile-pic">
                <?php if ($link) : ?>
                    <a href="<?php the_permalink(); ?>">
                        <img class="expert-profile-pic" src="<?= $expert->profilePic; ?>">
                    </a>
                <?php else : ?>
                    <img class="expert-profile-pic" src="<?= $expert->profilePic; ?>">
                <?php endif; ?>
                </div>
                <?php
            endif;
        });

        $expert->add_method('renderLogo', function($expert) {
            if ($expert->logo) :
                ?>
                <div class="logo">
                    <?php if ($expert->website) : ?>
                        <a href="<?= $expert->website; ?>" target="_blank"><img src="<?= $expert->logo; ?>" alt="<?= $expert->company_name; ?>" class="expert-logo"></a>
                    <?php else: ?>
                        <img src="<?= $expert->logo; ?>" alt="<?= $expert->company_name; ?>">
                <?php endif; ?>
                </div>
                <?php
            endif;
        });

        $expert->add_method('renderField', function($expert, $field) {
            if ($expert->$field) :
                ?>
                <div class="<?= $field; ?>"><?= $expert->$field; ?></div>
                <?php
            endif;
        });

        $expert->add_method('renderCompany', function($expert, $link = true) {
            if ($expert->company_name) :
                ?>
                <div class="company">
                    <?php if ($link and $expert->website): ?>
                        <a href="<?= $expert->website; ?>" target="_blank"><?= $expert->company_name; ?></a>
                    <?php else: ?>
                        <?= $expert->company_name; ?>
                <?php endif; ?>
                </div>
                <?php
            endif;
        });

        $expert->add_method('renderEmail', function($expert) {
            if ($expert->email) :
                ?>
                <div class="email">
                    <a href="mailto:<?= $expert->email; ?>"><?= $expert->email; ?></a>
                </div>
                <?php
            endif;
        });
    }
}

register_activation_hook(__FILE__, 'flush_rewrite_rules');
register_deactivation_hook(__FILE__, 'flush_rewrite_rules');

Experts::init();

class Experts_Widget extends \WP_Widget
{

    public function __construct()
    {
        parent::__construct(
            'experts_widget', // Base ID
            __('Experts Widget', 'text_domain'), // Name
            array('description' => __('A Widget of Experts', 'text_domain'),) // Args
        );
    }

    public function widget($args, $instance)
    {
        global $wpdb;

        $limit = isset($instance['limit']) ? $instance['limit'] : 5;

        $query = new \WP_Query(array(
            'post_type' => 'expert',
            'post_status' => 'publish',
            'posts_per_page' => $limit,
            'orderby' => 'post_title',
            'order' => 'ASC',
        ));

        if ($query->have_posts()) {

            echo $args['before_widget'];

            if (!empty($instance['title'])) :
                ?>
                <h3 class="widget-title"><a href="/expert-insight/"><?= $instance['title']; ?></a></h3>
                <?php
            endif;

            $postCount = 0;
            while ($query->have_posts()) {
                $query->the_post();
                $expert = \A360\Core\Content::forge(get_the_ID());
                $permaLink = get_permalink(get_the_ID());

                if ($postCount++ > 0)
                    echo '<hr>';
                ?>
                <section class="expert expert-widget">
                    <article class="body">
                            <?php $expert->renderProfilePic(true); ?>
                        <div style="overflow:hidden">
                            <div><a href="<?php echo $permaLink; ?>" class="expert-title"><?php echo $expert->title; ?></a></div>
                            <?php
                            $expert->renderField('fullname');
                            $expert->renderCompany(false);
                            ?>
                        </div>
                    </article>
                </section>
                <?php
            }
            wp_reset_postdata();
            echo $args['after_widget'];
        }
    }

    public function form($instance)
    {

        $instance = array_merge(array(
            'title' => __('Expert Insight', 'text_domain'),
            'limit' => 3,
            'rotate' => true,
            ), $instance);

        extract($instance, EXTR_SKIP);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Post Limit:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo esc_attr($limit); ?>" />
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : __('Expert Insight', 'text_domain');
        $instance['limit'] = (!empty($new_instance['limit']) ) ? absint($new_instance['limit']) : 3;
        return $instance;
    }

}
