<?php

$options = get_option('dirtysuds_export_options');

if ($options['outputFormat'])
    $outputFormat       = $options['outputFormat'];
else
    $outputFormat       = '<ANSI-MAC>';

if ($options['outputFormat']=='<ANSI-MAC>')
    $newLine            = "\x0a";
else
    $newLine            = "\x0d\x0a";

$NormalParagraphStyle = 'NormalParagraphStyle';
$defineStyles =
    '<DefineParaStyle:'.
    $NormalParagraphStyle.
    '=<Nextstyle:'.$NormalParagraphStyle.'>'.
    '<cColor:Registration><cSize:10><cLeading:11>'.
    '<pFirstLineIndent:12><cFont:Times New Roman>'.
    '>';

$tab = "\t";
$doubleTab = "\t\t";
$images = array();
$captions = array();

/** ******************************************************************************************************************
 * getCaptions
 *
 * @author Matthew Moore
 *
 * @description
 *  Change [caption... ]...[/caption] to <caption>...</caption> for XML transfer. Then remove the captions from the
 *  main content and append them to the end.
 *
 * @param $content
 * @return mixed
 ********************************************************************************************************************/
function getCaptions($content)
{
    global $captions;

    //Change our captions to xml styled nodes
    $content = preg_replace('/\[caption\s*(.*?)](.*?)\[\/caption]/', '<caption $1>$2</caption>',$content);

    //grab our captions
    preg_match_all('/<caption\s*(.*?)>(.*?)\<\/caption>/', $content, $matches);

    //Global captions assigned.
    $captions = $matches;

    //Remove the captions
    return preg_replace('/<caption\s*(.*?)>(.*?)\<\/caption>/', '', $content);
}

/** ******************************************************************************************************************
 * concatCaptions
 *
 * @author Matthew Moore
 * @description
 *  Add our Captions back in to the content where we want them.
 *
 * @param $content
 * @return string
 *********************************************************************************************************************/
function concatCaptions($content)
{
    global $captions, $newLine;

    foreach ($captions[0] as $caption){
        $content .= $caption . $newLine;
    }

    return $content;
}

/** ******************************************************************************************************************
 * @author Matthew Moore
 *
 * @description
 *
 * @param $content
 * @return mixed
 ********************************************************************************************************************/
function getImages($content, $post)
{
    global $images;

    //Get Our Images
    preg_match_all('/<img.+\/>/',$content, $matches);
    $images = $matches;

    //Get our Featured image if it exists
    $featured_image = get_post_meta($post->post_id, '_thumbnail_id', true);

    //If there is a featured image and that image doesn't already exist in the
    if($featured_image && !in_array($featured_image, $images)){
        array_unshit($images, $featured_image);
    }

    //Remove the images from the content body and send it back for more processing
    return preg_replace('/<img.+\/>/', '', $content);


}

function concatImages($content)
{
    global $images, $newLine, $tab;

    $stylings = $newLine . $tab;

    foreach($images[0] as $image)
    {
        $content .= $image . $newLine;
    }

    return $content;
}

function printBodyTag2($content)
{
    $pos = strpos($content, '</byline>');
    if($pos)
    {
        return substr_replace($content, '<body>', $pos + 9, 0);
    }

    $pos = strpos($content, '</deck>');
    if($pos)
    {
        return substr_replace($content, '<body>', $pos + 7, 0);
    }

    $pos = strpos($content, '<article>');
    return substr_replace($content, '<body>', $pos + 9, 0);
}

function dirtysuds_content_taggedtext(){

    global $NormalParagraphStyle,$newLine,$tab, $doubleTab;
    $content = '';
    $post = get_post();

    $title = $tab . "<title>" . $newLine. $doubleTab. "Add Title" . $newLine . $tab . "</title>". $newLine;

    $allowed_taggedtext_tags = array(
        'p'      => array(
            'class' => array()
        ),
        'br'     => array(),
        'b'      => array(),
        'strong' => array(
            'class' => array()
        ),
        'i'      => array(),
        'em'     => array(),
        'u'      => array(),
        'del'    => array(),
        'li'     => array(),
        'ul'     => array(),
        'ol'     => array(),
        'sub'    => array(
            'class' => array()
        ),
        'sup'    => array(
            'class' => array()
        ),
        'h1'     => array(
            'class' => array()
        ),
        'h2'     => array(
            'class' => array()
        ),
        'h3'     => array(
            'class' => array()
        ),
        'h4'     => array(),
        'h5'     => array(
            'class' => array()
        ),
        'h6'     => array(),
        'blockquote' => array(
            'class' => array()
        ),
        'pre'   => array(
            'class' =>array()
        ),
        'address'=> array(
            'class' =>array(),
            'id'    =>array()
        ),
        'title' => array(
            'alt'   => array()
        ),
        'img' => array(
            'src'   => array(),
            'alt'   => array(),
            'class' => array(),
            'id'    => array(),
            'width' => array(),
            'height'=> array()
        )
    );

    $conversion_table = array(
        "\r"            => $newLine,
        "\f"            => $newLine,
        "<br />"        => $newLine,
        "<pre>"         => $newLine.'<ParaStyle:'.$NormalParagraphStyle.'>'.
            '<cFont:Andale Mono>',
        "</pre>"        => '<cFont:>',
        '<p>'           => $tab,
        '</p>'          => '',
        '<b>'           => '<cTypeface:Bold>',
        '<strong>'      => '<cTypeface:Bold>',
        '<i>'           => '&lt;i&gt;',
        '<em>'          => '&lt;em&gt;',
        '</b>'          => '<cTypeface:>',
        '</strong>'     => '<cTypeface:>',
        '</i>'          => '&lt;/i&gt;',
        '</em>'         => '&lt;/em&gt;',
        '<u>'           => '<cUnderline:1>',
        '</u>'          => '<cUnderline:>',
        '<del>'         => '<cStrikethru:1>',
        '</del>'        => '<cStrikethru:>',
        //'<sub>'         => '<cPosition:Subscript>',
        //'</sub>'        => '<cPosition:>',
        //'<sup>'         => '<cPosition:Superscript>',
        //'</sup>'        => '<cPosition:>',
        '<h1>'          => $newLine.'<ParaStyle:><cTypeface:Bold><cSize:48>',
        '<h2>'          => $newLine.'<ParaStyle:><cSize:36>',
        '<h3>'          => $newLine.'<ParaStyle:><cTypeface:Bold><cSize:18>',
        '<h4>'          => $newLine.'<ParaStyle:><cTypeface:Bold>',
        '<h5>'          => $newLine.'<ParaStyle:><cTypeface:Bold Italic>',
        '<h6>'          => $newLine.'<ParaStyle:><cTypeface:Italic>',
        // '</h1>'         => '<cTypeface:><cSize:>'.$newLine,
        // '</h2>'         => '<cSize:>'.$newLine,
        '</h3>'         => '',
        //'</h4>'         => '<cTypeface:>'.$newLine,
        '</h5>'         => '<cTypeface:>'.$newLine,
        '</h6>'         => '<cTypeface:>'.$newLine,
        // '<blockquote>'  => '<ParaStyle:'.$NormalParagraphStyle.'>'.
        //    '<pLeftIndent:12><pSpaceBefore:6><pSpaceAfter:6>',
        // '</blockquote>' => $newLine.'<pLeftIndent:><pSpaceBefore:><pSpaceAfter:>',
        '<0x0026>'      => '<0xFFFD>',

        /* Some day, we will have a better system for handeling lists */
        '<li>'          => '<0x2022> ',
        '</li>'         => '',
        '<ul>'          => '',
        '</ul>'         => '',
        '<ol>'          => '',
        '</ol>'         => '',
    );

    /*************
     * HTML Named Entities are the suck! We need to kill them. Kill them with fire!
     * There MUST be a better solution to all of this
     *************/
    $trickyCharacters = array(
        '&amp;'      => '<0x0026>',
        //'&sup1;'     => '<sup>1</sup>',
        // '&sup2;'     => '<sup>2</sup>',
        // '&sup3;'     => '<sup>3</sup>',
        '&oelig;'    => '&#338;',
        '&nbsp;'     => ' ',
        //'&iexcl;'    => '&iexcl;',
        //'&cent;'     => '<0x00A2>',
        //'&pound;'    => '<0x00A3>',
        '&curren;'   => '&#164;',
        '&yen;'      => '&lt;0x00A5&gt;',
        '&brvbar;'   => '&lt;0x00A6&gt;',
        '&sect;'     => '&lt;0x00A7&gt;',
        '&uml;'      => '&lt;0x00A8&gt;',
        '&copy;'     => '&lt;0x00A9&gt;',
        '&ordf;'     => '&lt;0x00AA&gt;',
        '&laquo;'    => '&lt;0x00AB&gt;',
        '&not;'      => '&lt;0x00AC&gt;',
        '&shy;'      => '&lt;0x00AD&gt;',
        '&reg;'      => '&lt;0x00AE&gt;',
        '&macr;'     => '&lt;0x00AF&gt;',
        '&deg;'      => '&lt;0x00B0&gt;',
        '&plusmn;'   => '&lt;0x00B1&gt;',
        '&acute;'    => '&lt;0x00B4&gt;',
        '&micro;'    => '&lt;0x00B5&gt;',
        '&para;'     => '&lt;0x00B6&gt;',
        '&middot;'   => '&lt;0x00B7&gt;',
        '&cedil;'    => '&lt;0x00B8&gt;',
        '&ordm;'     => '&lt;0x00BA&gt;',
        '&raquo;'    => '&lt;0x00BB&gt;',
        '&frac14;'   => '&lt;0x00BC&gt;',
        '&frac12;'   => '&lt;0x00BD&gt;',
        '&frac34;'   => '&lt;0x00BE&gt;',
        '&iquest;'   => '&lt;0x00BF&gt;',
        '&Agrave;'   => '&lt;0x00C0&gt;',
        '&Aacute;'   => '&lt;0x00C1&gt;',
        '&Acirc;'    => '&lt;0x00C2&gt;',
        '&Atilde;'   => '&lt;0x00C3&gt;',
        '&Auml;'     => '&lt;0x00C4&gt;',
        '&Aring;'    => '&lt;0x00C5&gt;',
        '&AElig;'    => '&lt;0x00C6&gt;',
        '&Ccedil;'   => '&lt;0x00C7&gt;',
        '&Egrave;'   => '&lt;0x00C8&gt;',
        '&Eacute;'   => '&lt;0x00C9&gt;',
        '&Ecirc;'    => '&lt;0x00CA&gt;',
        '&Euml;'     => '&lt;0x00CB&gt;',
        '&Igrave;'   => '&lt;0x00CC&gt;',
        '&Iacute;'   => '&lt;0x00CD&gt;',
        '&Icirc;'    => '&lt;0x00CE&gt;',
        '&Iuml;'     => '&lt;0x00CF&gt;',
        '&ETH;'      => '&lt;0x00D0&gt;',
        '&Ntilde;'   => '&lt;0x00D1&gt;',
        '&Ograve;'   => '&lt;0x00D2&gt;',
        '&Oacute;'   => '&lt;0x00D3&gt;',
        '&Ocirc;'    => '&lt;0x00D4&gt;',
        '&Otilde;'   => '&lt;0x00D5&gt;',
        '&Ouml;'     => '&lt;0x00D6&gt;',
        '&times;'    => '&lt;0x00D7&gt;',
        '&Oslash;'   => '&lt;0x00D8&gt;',
        '&Ugrave;'   => '&lt;0x00D9&gt;',
        '&Yuml;'     => '&lt;0x00D9&gt;',
        '&Uacute;'   => '&lt;0x00DA&gt;',
        '&Ucirc;'    => '&lt;0x00DB&gt;',
        '&Uuml;'     => '&lt;0x00DC&gt;',
        '&Yacute;'   => '&lt;0x00DD&gt;',
        '&THORN;'    => '&lt;0x00DE&gt;',
        '&szlig;'    => '&lt;0x00DF&gt;',
        '&agrave;'   => '&lt;0x00E0&gt;',
        '&aacute;'   => '&lt;0x00E1&gt;',
        '&acirc;'    => '&lt;0x00E2&gt;',
        '&atilde;'   => '&lt;0x00E3&gt;',
        '&auml;'     => '&lt;0x00E4&gt;',
        '&aring;'    => '&lt;0x00E5&gt;',
        '&aelig;'    => '&lt;0x00E6&gt;',
        '&ccedil;'   => '&lt;0x00E7&gt;',
        '&egrave;'   => '&lt;0x00E8&gt;',
        '&eacute;'   => '&lt;0x00E9&gt;',
        '&ecirc;'    => '&lt;0x00EA&gt;',
        '&euml;'     => '&lt;0x00EB&gt;',
        '&igrave;'   => '&lt;0x00EC&gt;',
        '&iacute;'   => '&lt;0x00ED&gt;',
        '&icirc;'    => '&lt;0x00EE&gt;',
        '&OElig;'    => '&lt;0x00EE&gt;',
        '&iuml;'     => '&lt;0x00EF&gt;',
        '&eth;'      => '&lt;0x00F0&gt;',
        '&ntilde;'   => '&lt;0x00F1&gt;',
        '&ograve;'   => '&lt;0x00F2&gt;',
        '&oacute;'   => '&lt;0x00F3&gt;',
        '&ocirc;'    => '&lt;0x00F4&gt;',
        '&otilde;'   => '&lt;0x00F5&gt;',
        '&ouml;'     => '&lt;0x00F6&gt;',
        '&divide;'   => '&lt;0x00F7&gt;',
        '&oslash;'   => '&lt;0x00F8&gt;',
        '&ugrave;'   => '&lt;0x00F9&gt;',
        '&uacute;'   => '&lt;0x00FA&gt;',
        '&ucirc;'    => '&lt;0x00FB&gt;',
        '&uuml;'     => '&lt;0x00FC&gt;',
        '&yacute;'   => '&lt;0x00FD&gt;',
        '&thorn;'    => '&lt;0x00FE&gt;',
        '&yuml;'     => '&lt;0x00FF&gt;',
        '&Scaron;'   => '&lt;0x0160&gt;',
        '&scaron;'   => '&lt;0x0161&gt;',
        '&fnof;'     => '&lt;0x0192&gt;',
        '&circ;'     => '&lt;0x02C6&gt;',
        '&tilde;'    => '&lt;0x02DC&gt;',
        '&Alpha;'    => '&lt;0x0391&gt;',
        '&Beta;'     => '&lt;0x0392&gt;',
        '&Gamma;'    => '&lt;0x0393&gt;',
        '&Delta;'    => '&lt;0x0394&gt;',
        '&Epsilon;'  => '&lt;0x0395&gt;',
        '&Zeta;'     => '&lt;0x0396&gt;',
        '&Eta;'      => '&lt;0x0397&gt;',
        '&Theta;'    => '&lt;0x0398&gt;',
        '&Iota;'     => '&lt;0x0399&gt;',
        '&Kappa;'    => '&lt;0x039A&gt;',
        '&Lambda;'   => '&lt;0x039B&gt;',
        '&Mu;'       => '&lt;0x039C&gt;',
        '&Nu;'       => '&lt;0x039D&gt;',
        '&Xi;'       => '&lt;0x039E&gt;',
        '&Omicron;'  => '&lt;0x039F&gt;',
        '&Pi;'       => '&lt;0x03A0&gt;',
        '&Rho;'      => '&lt;0x03A1&gt;',
        '&Sigma;'    => '&lt;0x03A3&gt;',
        '&Tau;'      => '&lt;0x03A4&gt;',
        '&Upsilon;'  => '&lt;0x03A5&gt;',
        '&Phi;'      => '&lt;0x03A6&gt;',
        '&Chi;'      => '&lt;0x03A7&gt;',
        '&Psi;'      => '&lt;0x03A8&gt;',
        '&Omega;'    => '&lt;0x03A9&gt;',
        '&alpha;'    => '&lt;0x03B1&gt;',
        '&beta;'     => '&lt;0x03B2&gt;',
        '&gamma;'    => '&lt;0x03B3&gt;',
        '&delta;'    => '&lt;0x03B4&gt;',
        '&epsilon;'  => '&lt;0x03B5&gt;',
        '&zeta;'     => '&lt;0x03B6&gt;',
        '&eta;'      => '&lt;0x03B7&gt;',
        '&theta;'    => '&lt;0x03B8&gt;',
        '&iota;'     => '&lt;0x03B9&gt;',
        '&kappa;'    => '&lt;0x03BA&gt;',
        '&lambda;'   => '&lt;0x03BB&gt;',
        '&mu;'       => '&lt;0x03BC&gt;',
        '&nu;'       => '&lt;0x03BD&gt;',
        '&xi;'       => '&lt;0x03BE&gt;',
        '&omicron;'  => '&lt;0x03BF&gt;',
        '&pi;'       => '&lt;0x03C0&gt;',
        '&rho;'      => '&lt;0x03C1&gt;',
        '&sigmaf;'   => '&lt;0x03C2&gt;',
        '&sigma;'    => '&lt;0x03C3&gt;',
        '&tau;'      => '&lt;0x03C4&gt;',
        '&upsilon;'  => '&lt;0x03C5&gt;',
        '&phi;'      => '&lt;0x03C6&gt;',
        '&chi;'      => '&lt;0x03C7&gt;',
        '&psi;'      => '&lt;0x03C8&gt;',
        '&omega;'    => '&lt;0x03C9&gt;',
        '&thetasym;' => '&lt;0x03D1&gt;',
        '&upsih;'    => '&lt;0x03D2&gt;',
        '&piv;'      => '&lt;0x03D6&gt;',
        '&ensp;'     => '&lt;0x2002&gt;',
        '&emsp;'     => '&lt;0x2003&gt;',
        '&thinsp;'   => '&lt;0x2009&gt;',
        '&zwnj;'     => '&lt;0x200C&gt;',
        '&zwj;'      => '&lt;0x200D&gt;',
        '&lrm;'      => '&lt;0x200E&gt;',
        '&rlm;'      => '&lt;0x200F&gt;',
        '&ndash;'    => '-',
        '&mdash;'    => '--',
        '&lsquo;'    => '\'',
        '&rsquo;'    => '\'',
        '&sbquo;'    => '&lt;0x201A&gt;',
        '&ldquo;'    => '\'',
        '&rdquo;'    => '\'',
        '&bdquo;'    => '&lt;0x201E&gt;',
        '&dagger;'   => '&lt;0x2020&gt;',
        '&Dagger;'   => '&lt;0x2021&gt;',
        '&bull;'     => '&lt;0x2022&gt;',
        '&hellip;'   => '&lt;0x2026&gt;',
        '&permil;'   => '&lt;0x2030&gt;',
        '&prime;'    => '&lt;0x2032&gt;',
        '&Prime;'    => '&lt;0x2033&gt;',
        '&lsaquo;'   => '&lt;0x2039&gt;',
        '&rsaquo;'   => '&lt;0x203A&gt;',
        '&oline;'    => '&lt;0x203E&gt;',
        '&frasl;'    => '&lt;0x2044&gt;',
        '&euro;'     => '&lt;0x20AC&gt;',
        '&image;'    => '&lt;0x2111&gt;',
        '&weierp;'   => '&lt;0x2118&gt;',
        '&real;'     => '&lt;0x211C&gt;',
        '&trade;'    => '&lt;0x2122&gt;',
        '&alefsym;'  => '&lt;0x2135&gt;',
        '&larr;'     => '&lt;0x2190&gt;',
        '&uarr;'     => '&lt;0x2191&gt;',
        '&rarr;'     => '&lt;0x2192&gt;',
        '&darr;'     => '&lt;0x2193&gt;',
        '&harr;'     => '&lt;0x2194&gt;',
        '&crarr;'    => '&lt;0x21B5&gt;',
        '&lArr;'     => '&lt;0x21D0&gt;',
        '&uArr;'     => '&lt;0x21D1&gt;',
        '&rArr;'     => '&lt;0x21D2&gt;',
        '&dArr;'     => '&lt;0x21D3&gt;',
        '&hArr;'     => '&lt;0x21D4&gt;',
        '&forall;'   => '&lt;0x2200&gt;',
        '&part;'     => '&lt;0x2202&gt;',
        '&exist;'    => '&lt;0x2203&gt;',
        '&empty;'    => '&lt;0x2205&gt;',
        '&nabla;'    => '&lt;0x2207&gt;',
        '&isin;'     => '&lt;0x2208&gt;',
        '&notin;'    => '&lt;0x2209&gt;',
        '&ni;'       => '&lt;0x220B&gt;',
        '&prod;'     => '&lt;0x220F&gt;',
        '&sum;'      => '&lt;0x2211&gt;',
        '&minus;'    => '&lt;0x2212&gt;',
        '&lowast;'   => '&lt;0x2217&gt;',
        '&radic;'    => '&lt;0x221A&gt;',
        '&prop;'     => '&lt;0x221D&gt;',
        '&infin;'    => '&lt;0x221E&gt;',
        '&ang;'      => '&lt;0x2220&gt;',
        '&and;'      => '&lt;0x2227&gt;',
        '&or;'       => '&lt;0x2228&gt;',
        '&cap;'      => '&lt;0x2229&gt;',
        '&cup;'      => '&lt;0x222A&gt;',
        '&int;'      => '&lt;0x222B&gt;',
        '&there4;'   => '&lt;0x2234&gt;',
        '&sim;'      => '&lt;0x223C&gt;',
        '&cong;'     => '&lt;0x2245&gt;',
        '&asymp;'    => '&lt;0x2248&gt;',
        '&ne;'       => '&lt;0x2260&gt;',
        '&equiv;'    => '&lt;0x2261&gt;',
        '&le;'       => '&lt;=',
        '&ge;'       => '&gt;=',
        '&sub;'      => '&lt;0x2282&gt;',
        '&sup;'      => '&lt;0x2283&gt;',
        '&nsub;'     => '&lt;0x2284&gt;',
        '&sube;'     => '&lt;0x2286&gt;',
        '&supe;'     => '&lt;0x2287&gt;',
        '&oplus;'    => '&lt;0x2295&gt;',
        '&otimes;'   => '&lt;0x2297&gt;',
        '&perp;'     => '&lt;0x22A5&gt;',
        '&sdot;'     => '&lt;0x22C5&gt;',
        '&lceil;'    => '&lt;0x2308&gt;',
        '&rceil;'    => '&lt;0x2309&gt;',
        '&lfloor;'   => '&lt;0x230A&gt;',
        '&rfloor;'   => '&lt;0x230B&gt;',
        '&lang;'     => '&lt;0x2329&gt;',
        '&rang;'     => '&lt;0x232A&gt;',
        '&loz;'      => '&lt;0x25CA&gt;',
        '&spades;'   => '&lt;0x2660&gt;',
        '&clubs;'    => '&lt;0x2663&gt;',
        '&hearts;'   => '&lt;0x2665&gt;',
        '&diams;'    => '&lt;0x2666&gt;',
    );

    $html_tags = array(
        /* quot; added for A360 -- This is need for any attributes added to our original tags. ie class="whatever"*/
        '/&quot;/',
        '/&(#60|lt);/',
        '/&(#62|gt);/',
        '/&#(#62);/',
        '/[\n\r\f]+/',
        '/&#[xX]([0-9A-Fa-f]{4})\;/e',
        '/&#([0-9]+)\;/e',
        '/([\n\r\f]*<p>|<br[\s\/]*>[\s\t\n\r\f]+|[\s\t]*[\n\r\f]+[\s\t]*)/',
        '/&(#60|lt);/',
        '/&#61;/',
        '/&(#62|gt);/',
        '/([\ ]|&nbsp;)+/',
        '/[\t]+[\ \t]*/',
        '/[\n\r\f]+/',
        '/\xc4([\x80-\xff])/e',
        '/\xc5([\x40-\xff])/e',
        '/&([A-Za-z]+)\;/e',
    );

    $tagged_tags = array(
        "\"",
        '<',
        '>',
        '&',
        $newLine,
        "'<0x'.$1.'>'",
        "'<0x'.str_pad(dechex($1),4,'0',STR_PAD_LEFT).'>'",
        "<p>",
        '\\<',
        ' ',
        '\\>',
        ' ',
        chr(9),
        $newLine,
        "'<0x'.str_pad(dechex(ord($1) +  128),4,'0',STR_PAD_LEFT).'>'",
        "'<0x'.str_pad(dechex(ord($1) +  192),4,'0',STR_PAD_LEFT).'>'",
        "'<0x'.str_pad(dechex(ord(html_entity_decode('&'.$1.';')),4,'0',STR_PAD_LEFT).'>'",

    );


    $a360_html_tags = array(
        /* A360 Additions - MM */
        '/<h2\s*class=\"deck\"\s*>(.*?)<\s*\/h2>/',
        '/<h3\s*class=\"subhead\"\s*>(.*?)<\s*\/h3>/',
        '/<p\s*class=\"reference\"\s*>(.*?)<\s*\/p>/',
        '/<p\s*class=\"bio\"\s*>(.*?)<\s*\/p>/',
        '/<p\s*class=\"byline\"\s*>(.*?)<\s*\/p>/',
        '/<sup\s*class=\"refmark\"\s*>(.*?)<\s*\/sup>/',
        '/<(c.*?)>/',
        '/<(0x.*?)>/',
        '/\(([^\s]\s*)[>](\s*\d+\.\d+\s*\))/',
        '/\(([^\s]\s*)[<](\s*\d+\.\d+\s*\))/'
    );


    $a360_tagged_text = array(
        $tab . '<deck>'. $newLine. $doubleTab .'$1' . $newLine . $tab . '</deck>' . $newLine,
        $newLine. $doubleTab . '<subhead>$1</subhead>' . $newLine,
        $newLine . '<reference>$1</reference>' .$newLine,
        '<bio>$1</bio>',
        '<byline>' . $newLine . $doubleTab . '$1' . $newLine . $tab .  '</byline>' . $newLine . $tab . $newLine . $tab,
        $newLine . $tab . '<refmark>$1</refmark>' .$newLine,
        '&lt;$1&gt;',
        '&lt;$1&gt;',
        '($1 &lt; $2)',
        '($1 &gt; $2)'
    );

    remove_all_filters('loop_end');

    $content = get_the_content();

    //Grab the Captions before they strip out the short codes
    $content = getCaptions($content);

    $content = strip_shortcodes($content);

    // Change double line-breaks in the text into HTML paragraphs
    $content = wpautop($content);

    // Strip all HTML except for $allowed_taggedtext_tags
    //$content = wp_kses($content,$allowed_taggedtext_tags);

    // Add Smart Quotes
    $content = wptexturize($content);

    $content = trim($content);

    //Convert special characters to HTML entities
    $content = mb_convert_encoding($content,'UTF-8',get_bloginfo('charset'));

    $content = htmlentities($content, ENT_QUOTES, 'UTF-8', false);

    // This is for Tagged Text. We might need to alter some of these, but certainly not all.
    // Infact we want to keep some of these as the inDesign XML interpreter can handle them. --MM 7/7/15
    $content = strtr($content,$trickyCharacters);

    $content = preg_replace($html_tags,$tagged_tags,$content);

    $content = preg_replace($a360_html_tags, $a360_tagged_text, $content);

    $content = strtr($content,$conversion_table);

    $content .= $newLine . $tab . "</body>";

    //Get our Images and move them after the body tag. --MM
    $content = getImages($content, $post);
    $content = concatImages($content);
    $content = concatCaptions($content);


    $content = '<article><title>'. $post->post_title . '</title>'. $newLine . $content . $newLine . '</article>';

    //Add the Opening Body tag
    $content = printBodyTag2($content);

    /*
        // Replace Unordered List Items with bullets
        $unorderedLists = explode('<ul>',' '.$content);
        $countUnorderedLists = count($unorderedLists);

        for ($i=1;$i<$countUnorderedLists+1;$i=$i+2) {
            $thislist = explode('</ul>',$unorderedLists[$i]);
            $thislist = $thislist[0];
            $content = strtr(
                $content,
                array(
                    '<ul>'.$thislist.'</ul>' =>
                    strtr(
                        $thislist,
                        array(
                            '<li>' => "<bnListType:Bullet>",
                            '</li>' => "\x0a<bnListType:>",
                            "<bnListType:>\x0a<bnListType:Bullet>" => "<bnListType:><bnListType:Bullet>",
                        )
                    )
                )
            );
        }

        */

    return trim($content);
}

// We don't want any optimization plugins mistaking our output for HTML. Let's turn them off.
ob_end_clean();
the_post();

// We don't want the browser to render the file, only download it. Let's call it a binary file
header('Content-type: binary/xml; charset=utf-8');

// We need to give the file some sort of name. In this case, the author's last name and the title of the story
// Don't forget to strip the spaces out. This makes it more compatible cross browser

header('Content-Disposition: filename='.preg_replace("/[^a-zA-Z0-9\-_]/", "",get_the_author_lastname().'-'.str_replace(' ','_',basename(get_permalink()))).'.xml;');

echo
    //$outputFormat.$newLine.
    //$defineStyles.$newLine.
dirtysuds_content_taggedtext();

exit();
