<?php

namespace A360\nav;

class a360_walker_nav_menu extends \Walker_Nav_Menu {

// add classes to ul sub-menus
public function start_lvl( &$output, $depth = 0, $args = array() ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $role = ($depth == 0 ? "role=\"navigation\"" : "");
    $id = ($depth == 0 ? "id=\"main-nav\"" : "");
    $div = ($depth == 0 ? $indent."<div>" : "");
    // build html
    $output .=<<<EOT
$div
$indent<ul a360_walker="yes" depth="$depth" >
EOT;
}

function end_lvl(&$output, $depth=0, $args=array()) {
	$div = ($depth == 0 ? "</div>" : "");
	$output .=<<<EOT
</ul>
$div
EOT;
}

// add main/sub classes to li's and links
 function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    global $wp_query;

    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

    // depth dependent classes
    $depth_classes = array(
    	( $depth == 0 ? 'rich' : ''),
    );
    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

	if (strpos($item->url, "--") !== false) {
		$output .=<<<EOT
</ul>
<ul>
EOT;
	} else {
	    // link attributes
	    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

	    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
	        $args->before,
	        $attributes,
	        $args->link_before,
	        apply_filters( 'the_title', $item->title, $item->ID ),
	        $args->link_after,
	        $args->after
	    );

	    // build html
	    $output .=<<<EOT
$indent<li class="$depth_class_names $class_names">
EOT;

	    // build html
	    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
}