<?php
//force git update
$true = true;

class NavSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'A360 Navigation Settings',
            'Navigation Settings',
            'manage_options',
            'a360-nav-setting-admin',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'a360_nav_options' );
		$handler = "options.php";
		echo<<<EOT
        <div class="wrap">
            <h2>Navigation Settings</h2>
            <form method="post" action="$handler" enctype="multipart/form-data">
EOT;
		// This prints out all hidden setting fields
		settings_fields( 'a360_nav' );
		do_settings_sections( 'a360-nav-setting-admin' );
		submit_button();
		echo<<<EOT
            </form>
        </div>
EOT;
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'a360_nav', // Option group
            'a360_nav_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'a360_nav_settings', // ID
            'A360 Navigation settings', // Title
            array( $this, 'print_nav_info' ), // Callback
            'a360-nav-setting-admin' // Page
        );

        add_settings_field(
        	'a360_nav_active',
        	'Activate a360 navigation menu',
        	array( $this, 'a360_nav_active_callback'),
        	'a360-nav-setting-admin',
        	'a360_nav_settings'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        if( isset( $input['a360_nav_active'] ) )
        	$new_input['a360_nav_active'] = sanitize_text_field( $input['a360_nav_active']);
        return $new_input;
    }

    /**
     * Print the metaslider section text
     */
    public function print_nav_info()
    {
        print 'Enter the navigation settings below:';
    }

    public function a360_nav_active_callback()
    {
    	$checked = (isset($this->options['a360_nav_active']) && $this->options['a360_nav_active'] == "active") ? "checked" : "";
    	echo<<<EOT
    	<input type="checkbox" id="a360_nav_active" name="a360_nav_options[a360_nav_active]" value="active" $checked />
EOT;
    }
}

if( is_admin() )
    $nav_settings = new NavSettings();
