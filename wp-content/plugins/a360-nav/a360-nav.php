<?php
/*
 Plugin Name: A360 Nav Menu
Plugin URI: http://www.whatsyouranthem.com
Description: Provides core class and helpers as well as initalize composer autoloader.
Version: 0.1
Author: Chris Caviness
Author URI: http://www.whatsyouranthem.com
*/

namespace A360\nav;

use a360_walker_nav_menu;

$state = false;

register_activation_hook(__FILE__, '\A360\nav\activate');
register_deactivation_hook(__FILE__, '\A360\nav\deactivate');
register_uninstall_hook(__FILE__, '\A360\nav\uninstall');

function activate() {

}

function deactivate() {

}

function uninstall() {

}

add_action('init', '\A360\nav\init');
require_once(__DIR__ . "/options.php");

function init() {
	require_once(__DIR__. "/a360_walker_nav_menu.php");
}

