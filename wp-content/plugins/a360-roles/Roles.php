<?php
/**
 * Plugin Name: A360 Roles
 * Description: Programmatically modify role capabilities
 * Version: 0.1
 * Author: Landon Springer
 * Author URI: http://www.allied360.com/
 */

namespace A360\Roles;

use A360\Core\Plugin;

if (!class_exists('\A360\Core\Plugin'))
    return;

class Roles extends Plugin
{
    protected static $capabilities = array(
        'editor' => array(
            'manage_options',
            'manage_polls'
        )
    );
    
    public static function bootstrap()
    {
        //static::register_autoloader(dirname(__FILE__).'/src', __NAMESPACE__);
        
        class_alias(__CLASS__, __NAMESPACE__);
        
//        var_dump(current_user_can('manage_polls')); exit;
//        static::activate();
        
        parent::bootstrap();
    }
    
    public static function activate()
    {
        foreach (static::$capabilities as $role => $caps) {
            $roleObject = get_role($role);
            foreach ($caps as $cap) {
                $roleObject->add_cap($cap);
            }
        }
    }
    
    public static function deactivate()
    {
        foreach (static::$capabilities as $role => $caps) {
            $role = get_role($role);
            foreach ($caps as $cap) {
                $role->remove_cap($cap);
            }
        }
    }
}

Roles::init();

register_activation_hook(__FILE__, array('A360\Roles\Roles', 'activate'));
register_deactivation_hook(__FILE__, array('A360\Roles\Roles', 'deactivate'));