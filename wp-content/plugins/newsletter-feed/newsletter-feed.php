<?php
/**
 * Plugin Name: Newsletter Feed
 * Description: Custom feed generator to add fields needed for newsletter builder software
 * Version: 0.1
 * Author: Landon Springer
 * Author URI: http://www.allied360.com/
 */

class NewsletterFeed
{
    public function __construct()
    {
        // Add MRSS namespace
        add_action('rss2_ns', array($this, 'addNamespace'));

        // Filter any unnecessary content out of item
        add_filter('the_content_feed', array($this, 'cleanupItemContent'));

        // Add media:content records for each RSS item
        add_action('rss2_item', array($this, 'addItemMedia'));

        // Remove WP SEO credits from this feed
        global $wpseo_front;
        if ($wpseo_front) {
            remove_filter( 'the_content_feed', array( $wpseo_front, 'embed_rssfooter' ) );
			remove_filter( 'the_excerpt_rss', array( $wpseo_front, 'embed_rssfooter_excerpt' ) );
        }

        // Rewrite primary category if plugin is enabled
        if (class_exists('A360\PrimaryCategory')) {
            add_filter('the_category_rss', array($this, 'rewritePrimaryCategory'));
        }

        // Do normal RSS2 feed (non-comments feed)
        do_feed_rss2(false);
    }

    public static function filterPosts($query)
    {
        if ($query->is_feed() and $query->is_main_query() and $query->get('feed') === 'mrss') {
            if (!empty($_GET['date'])) {
                switch ($_GET['date']) {
                    case 'yesterday':
                        $time = new DateTime();
                        $time = $time->sub(new DateInterval('P1D'))->getTimestamp();
                        static::filterPostsByDate($query, $time);
                        break;
                    default:
                        static::filterPostsByDate($query, strtotime($_GET['date']));
                        break;
                }
            }

            if(isset($_REQUEST['start_date'])) {
            	static::filterPostsByStartDate($query, strtotime($_REQUEST['start_date']));
            }

            if(isset($_REQUEST['end_date'])) {
            	static::filterPostsByEndDate($query, strtotime($_REQUEST['end_date']));
            }

            if(isset($_REQUEST['post_type'])) {
            	static::filterPostsByPostType($query, $_REQUEST['post_type']);
            } else {
            	$query->set('post_type', array('post', 'event'));
            }

            if(isset($_REQUEST['order'])) {
            	switch(strtolower($_REQUEST['order'])) {
            		case 'ascending':
            		case 'asc':
            			$order = 'ASC';
            			break;
            		case 'descending':
            		case 'desc':
            			$order = 'DESC';
            			break;
            		default:
            			$order = 'DESC';
            			break;
            	}
            	$query->set('order', $order);
            }
        }
    }

    public static function setpost_limits ($s) {
    	if(is_feed() && isset($_REQUEST['nolimit']))
    		return '';
    	else
    		return $s;
    }

    public static function filterPostsByPostType($query, $post_types) {
    	$old_arg = get_query_var('post_type', array());
    	switch (get_type($post_types)) {
    		case "string":
    			$post_types = split(",", $post_types);
    			break;
    		case "array":
    			break;
    	}
    	$post_types = array_merge($old_arg, $post_types);
    	$query->set('post_type', $post_types);
    }

    public static function filterPostsByDate($query, $date)
    {
        $args = array(
            'year'  => (int)date('Y', $date),
            'month' => (int)date('n', $date),
            'day'   => (int)date('j', $date),
        );

        $query->set('date_query', $args);
    }

    public static function filterPostsByStartDate($query, $date) {
    	$date = is_string($date) ? strtotime($date) : $date;
    	$old_arg = get_query_var('date_query', array());
    	$args = array(
    		'after' => array(
    			'year'  => idate('Y', $date),
    			'month' => idate('m', $date),
    			'day'   => idate('d', $date)
    		),
    		'inclusive' => true
    	);
    	$query->set('date_query', array_merge($old_arg, $args));
    	$query->set('order', 'ASC');
    }

    public function filterPostsByEndDate($query, $date) {
    	$date = is_string($date) ? strtotime($date) : $date;
    	$old_arg = get_query_var('date_query', array());
    	$args = array(
    		'before' => array(
    			'year'  => idate('Y', $date),
    			'month' => idate('m', $date),
    			'day'   => idate('d', $date)
    		),
    		'inclusive' => true
    	);
    	$query->set('date_query', array_merge($old_arg, $args));
    	$query->set('order', 'ASC');
    }

    public function addNamespace()
    {
        echo ' xmlns:media="http://search.yahoo.com/mrss/"';
    }

    public function cleanupItemContent($content)
    {
        $content = preg_replace('#((src|href)=["\'])/#', '$1'.site_url('/'), $content);
        $content = preg_replace('#<script[\s\S]*?/script>#', '', $content);
        return $content;
    }

    public function addItemMedia()
    {
        $media  = '';

        if (has_post_thumbnail()) {

            $media .= "\t\t<media:group>\n";
            $attachment_id = get_post_thumbnail_id();

            $sizes = apply_filters('newsletter_feed_media_sizes', array(
                'thumbnail' => wp_get_attachment_image_src($attachment_id, apply_filters('newsletter_feed_thumbnail_size', 'featured')),
                'featured'  => wp_get_attachment_image_src($attachment_id, apply_filters('newsletter_feed_original_size', 'featured')),
                'old_featured'  => wp_get_attachment_image_src($attachment_id, apply_filters('newsletter_feed_featured_size', 'featured')),
           ), $attachment_id);


            foreach ($sizes as $size => $attr) {

                list($url, $width, $height) = $attr;
                $content  = sprintf("\t\t\t<media:content url=\"%s\" width=\"%d\" height=\"%d\"", $url, $width, $height);
                $content .= "/>\n";
                $media   .= apply_filters('newsletter_feed_media_content_'.$size, $content);
            }

            $media .= "\t\t</media:group>\n";
        }

        echo apply_filters('newsletter_feed_media_group', $media);
    }

    public function rewritePrimaryCategory($the_list) {
        if (($cat = A360\PrimaryCategory::getPrimaryCategory())) {

            $cat = $cat->name;
            if ($cat == 'Uncategorized' && get_post_type() == 'event') {
                $cat = 'Upcoming Events';
            }

            $the_list = "\t\t<category><![CDATA[" . @html_entity_decode( $cat, ENT_COMPAT, get_option('blog_charset') ) . "]]></category>\n";
        }
        return $the_list;
    }
}

add_filter('post_limits', array('NewsletterFeed','setpost_limits'));

// Have to hook into the query before do_feed_mrss hook is called
add_action('pre_get_posts', array('NewsletterFeed', 'filterPosts'));

// Do the MRSS feed
add_action('do_feed_mrss', function() {
    return new NewsletterFeed();
});

// Do the InDesign print feed
add_action('do_feed_print', function() {
    include dirname(__FILE__).'/feed-print.php';
});
