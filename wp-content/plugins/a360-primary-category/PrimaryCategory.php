<?php
/**
 * Plugin Name: A360 Primary Category
 * Description: Enabled primary category selection for post types
 * Version: 0.1
 * Author: Landon Springer
 * Author URI: http://www.allied360.com/
 */

namespace A360\PrimaryCategory;
use A360\Core\Plugin;


class PrimaryCategory 
{
    protected static $registry = array();
    
    public static function init()
    {
        parent::init();
        
        class_alias(__CLASS__, 'A360\\PrimaryCategory');
    }
    
    public static function bootstrap()
    {
        static::register_autoloader(dirname(__FILE__).'/src', 'A360\PrimaryCategory');
        
        global $pagenow;
            
        if (is_admin() and ($pagenow === 'post.php' || $pagenow === 'post-new.php'))
        {
            add_action('admin_head', __CLASS__.'::action_admin_head');
            add_action('admin_enqueue_scripts', __CLASS__.'::action_enqueue_scripts');
        }
       
        add_action('save_post',  __CLASS__.'::action_save_post');
    }
    
    public static function action_admin_head()
    {
        add_filter('wp_terms_checklist_args', function($args, $post_id) {

            if (PrimaryCategory::isEnabledForTaxonomy($args['taxonomy'])) {
                $args['walker'] = PrimaryCategory::makeWalker();
            }

            return $args;
        }, 10, 2);
    }
    
    public static function action_enqueue_scripts()
    {
        $type = get_post_type();
        
        if (!empty(static::$registry[$type])) {
            wp_enqueue_script('a360-primary-category', plugins_url('/assets/post.js', __FILE__), array('jquery'), 1, true);
            wp_localize_script('a360-primary-category', 'A360_PC_Taxonomies', static::$registry[$type]);
        }
    }
    
    public static function action_save_post($post_id)
    {
        // Bail if we're doing an auto save
		if (defined('DOING_AUTOSAVE') and DOING_AUTOSAVE)
            return $post_id;
        
        // Save a meta value for each taxonomy
        if (isset($_POST['primary_term'])) {
            foreach ($_POST['primary_term'] as $taxonomy => $term_id) {
                $key = '_primary_'.$taxonomy;
                delete_post_meta($post_id, $key);
                add_post_meta($post_id, $key, $term_id);
            }
        }
    }
    
    public static function makeWalker()
    {
        $class = static::getWalkerClass();
        return new $class;
    }
    
    public static function getWalkerClass()
    {
        return __NAMESPACE__.'\\Walker\\Walker_Category_Checklist';
    }
    
    public static function isEnabledForTaxonomy($taxonomy = 'category', $post_type = null)
    {
        empty($post_type) and $post_type = get_post_type();
        return isset(static::$registry[$post_type]) and in_array($taxonomy, static::$registry[$post_type]);
    }
    
    public static function enableForTaxonomy($taxonomy = 'category', $post_type = 'post')
    {
        empty(static::$registry[$post_type]) and static::$registry[$post_type] = array();
        static::$registry[$post_type][] = $taxonomy;
    }
    
    public static function getEnabledTaxonomies($post_type = null)
    {
        empty($post_type) and $post_type = get_post_type();
        return empty(static::$registry[$post_type]) ? array() : static::$registry[$post_type];
    }
    
    public static function getPrimaryCategory($post_id = null, $taxonomy = 'category')
    {
        $post_id = $post_id ?: get_the_ID();
        if (($cat = get_post_meta($post_id, '_primary_'.$taxonomy, true)))
        {
            return get_category($cat);
        }
        else
        {
            return current(get_the_category($post_id));
        }
    }
}

PrimaryCategory::init();