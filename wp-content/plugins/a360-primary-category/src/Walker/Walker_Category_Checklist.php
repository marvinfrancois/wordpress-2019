<?php
namespace A360\PrimaryCategory\Walker;

use A360\PrimaryCategory\PrimaryCategory;

use Walker_Category_Checklist as WP_WCC;

class Walker_Category_Checklist extends WP_WCC
{
    protected $primary_selected = array();
    
    public function __construct()
    {
        $post_id = get_the_ID();
        $taxonomies = PrimaryCategory::getEnabledTaxonomies();
        foreach ($taxonomies as $taxonomy)
        {
            $this->primary_selected[$taxonomy] = get_post_meta($post_id, '_primary_'.$taxonomy, true);
        }
    }
    
    public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 )
    {
        parent::start_el($output, $category, $depth, $args, $id);
        
        $taxonomy = (isset($args['taxonomy']) ? $args['taxonomy'] : 'category');
        $name     = 'primary_term['.$taxonomy.']';
        $checked  = empty($this->primary_selected[$taxonomy]) ? false : $this->primary_selected[$taxonomy] == $category->term_id;
        $output  .= ' <label style="text-align:right;"><input type="radio" id="primary-'.$taxonomy.'-'.$category->term_id.'" name="'.$name.'" value="'.$category->term_id.'"'.checked($checked, true, false).'></label>';
    }
}
