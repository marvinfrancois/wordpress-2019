jQuery(function($) {
    $.each(A360_PC_Taxonomies, function(i, taxonomy) {
        
        // Automatically select corresponding checkbox when primary radio is checked
        $('input[name="primary_term\['+taxonomy+'\]"]').change(function() {
            var val = $(this).val();
            $('#in-'+taxonomy+'-'+val).attr('checked', 'checked');
        });
        
        // Automatically unselect primary radio when checkbox is unchecked
        $('.'+taxonomy+'checklist input[type="checkbox"]').change(function() {
            var $t = $(this);
            if (!$t.is(':checked')) {
                $('#primary-'+taxonomy+'-'+$t.val()).removeAttr('checked');
            }
        });
    });
});