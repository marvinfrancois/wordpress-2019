<!DOCTYPE html>
<html>
<head>
	<?php
	ini_set('display_errors', 'on');
	error_reporting(E_ALL);
	include './wp-bootstrap.php';
	$results	= get_posts('posts_per_page=10&post_type=reference');
	?>
	<title>Insert External Sidebar</title>
	<script>
	var ajax_object = {
		ajax_url: '<?= admin_url( 'admin-ajax.php' ); ?>'
	};
	</script>
	<script type="text/javascript" src="../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<style type="text/css">
	html body { min-width:0; }
	#search input {
		-moz-box-sizing:border-box;
		-webkit-box-sizing:border-box;
		box-sizing:border-box;
		width:100%;
		margin:4px 0 0;
		padding:4px;
	}
	#results { height:250px; overflow:auto; border:1px solid rgb(223, 223, 223); background:#f6f6f6; border-top:0; margin-bottom:1em; }
	#results ul { list-style-type:none; margin:0; padding:0; }
	#results a { display:block; padding:3px 6px; text-decoration: none; }
	#results a.selected,
	#results a:hover { background:#2B6FB6; color:#fff; }
	.mceActionPanel { margin-top:1em; }
	</style>
</head>
<body>
	<form name="source" onsubmit="return Reference.insert()" action="#">
		<div class="title">Choose a Sidebar</div>
		<div id="search">
			<input type="text" placeholder="Search for a sidebar">
		</div>
		<div id="results">
			<ul>
			<?php foreach ($results as $post) : ?>
				<li><a href="javascript:{}" onclick="Reference.select(<?= $post->ID; ?>, this)"><?= $post->post_title; ?></a></li>
			<?php endforeach; ?>
			</ul>
		</div>

		<div class="mceActionPanel">
			<input type="submit" id="insert" name="insert" value="{#insert}">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();">
		</div>
	</form>
	<script>
	var Reference = {
		e : null,
		ID : null,
		mediaID: null,
		input: null,
		init : function(e) {
			var self = this;
			this.e = e;
			tinyMCEPopup.resizeToInnerSize();

			window.top.require(['a360'], function(a360) {
				self.input = jQuery('#search input');
				self.input.on('keydown', function() {
					var args = {
						type: 'Content',
						post_type: 'reference',
						search: this.value
					};
					a360.autoCompleter.search(args, function(response) {
						var list = jQuery('#results ul').empty();
						jQuery.each(response, function() {
							list.append('<li><a href="javascript:{}" onclick="Reference.select('+this.ID+', this)">'+this.display+'</a></li>');
						});
					});
				});
			});
		},
		select: function(id, ref) {
			this.ID = id;
			jQuery('#results .selected').removeClass('selected');
			jQuery(ref).addClass('selected');
		},
		insert : function() {

			var content = tinyMCEPopup.execCommand('DoShortcode', 'sidebar', {
				id		: this.ID,
                _default: 'Insert Button Label Here'
			});

			tinyMCEPopup.execCommand('mceReplaceContent', false, content);
			tinyMCEPopup.close();

			return false;
		}
	};
	tinyMCEPopup.onInit.add(Reference.init, Reference);
	</script>
</body>
</html>
