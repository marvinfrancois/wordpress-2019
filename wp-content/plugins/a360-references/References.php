<?php
/**
 * Plugin Name: A360 References
 * Description: Allows in-article references to other supporting content
 * Version: 0.1
 * Author: Landon Springer
 * Author URI: http://www.allied360.com/
 */

namespace A360\References;

use A360\Core\Plugin;

if (!class_exists('\A360\Core\Plugin'))
    return;

class References extends Plugin
{
    public static function bootstrap()
    {
        static::register_autoloader(dirname(__FILE__).'/src', __NAMESPACE__);
        
        class_alias(__CLASS__, __NAMESPACE__);
        
        parent::bootstrap();
        
        // Register TinyMCE plugin
		if ( ( current_user_can('edit_posts') or current_user_can('edit_pages') ) and get_user_option('rich_editing') ) {
            add_filter("mce_external_plugins", function ($plugin_array) {
                $plugin_array['reference'] = plugin_dir_url(__FILE__).'editor_plugin.js?v=2';
                return $plugin_array;
            });

            add_filter('mce_buttons', function ($buttons) {
                array_push($buttons, "separator", "insert_reference_button");
                return $buttons;
            });
        }
    }
    
    public static function register_content_types()
    {
        Content\Reference::bootstrap();
    }
    
    public static function register_shortcodes()
    {
        Shortcode::register();
    }
}

References::init();

