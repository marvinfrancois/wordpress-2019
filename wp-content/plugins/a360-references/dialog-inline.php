<!DOCTYPE html>
<html>
<head>
	<title>Insert Inline Sidebar</title>
	<script type="text/javascript" src="../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<style type="text/css">
	html body { min-width:0; }
	.mceActionPanel { position:absolute; bottom:4px; right:4px; width:140px; }
    label { font-weight:bold; display:block; margin:1em 0 .3em 0; }
	</style>
</head>
<body>
	<form name="source" onsubmit="return Sidebar.insert()" action="#">
        <label>Float</label>
        <select name="float">
            <option value="">None</option>
            <option value="right">Right</option>
            <option value="left">Left</option>
        </select>
        
        <label>Width (px)</label>
        <input type="text" name="width">

		<div class="mceActionPanel">
			<input type="submit" id="insert" name="insert" value="{#insert}">
			<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();">
		</div>
	</form>
	<script>
	var Sidebar = {
		e : null,
		ID : null,
		mediaID: null,
		input: null,
		init : function(e) {
			tinyMCEPopup.resizeToInnerSize();
		},
		insert : function() {
            
            var attr = {
                _default: '-Sidebar Content Here-'
            };
            
            $('form > :input').each(function() {
                var $t = $(this),
                    val = $t.val();
                if (val) {
                    attr[$t.attr('name')] = val;
                }
            });

			var content = tinyMCEPopup.execCommand('DoShortcode', 'sidebar', attr);

			tinyMCEPopup.execCommand('mceReplaceContent', false, content);
			tinyMCEPopup.close();

			return false;
		}
	};
	tinyMCEPopup.onInit.add(Sidebar.init, Sidebar);
	</script>
</body>
</html>
