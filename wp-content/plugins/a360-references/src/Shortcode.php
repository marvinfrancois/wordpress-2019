<?php

namespace A360\References;

use A360\Core\Shortcode as SC;

class Shortcode extends SC
{
    const SHORTCODE = 'reference';
    
    public static function register($shortcode = null)
    {
        return parent::register(array('reference', 'sidebar'));
    }
    
    public static function render($atts, $content = '')
    {
        shortcode_atts(array(
			'class'	=> '',
            'float' => 'none',
		), $atts);
        
        extract($atts, EXTR_SKIP);
        
        // Handle differently for feeds
        global $wp_query;
        $feed = $wp_query->is_feed() ? $wp_query->get('feed') : false;
        
        // Referenced content
        if (isset($id)) {
            if (($url = post_permalink($id))) {
                if ($feed) {
                    if ($feed === 'print') {
                        $reference = get_post($id);
                        echo sprintf('<sidebar url="%s">%s</sidebar>', $url, apply_filters('the_content', $reference->post_content, $feed));
                    }
                } else {
                    $class .= ' article-reference-trigger';
                    echo sprintf('<a href="%s" data-modal="ajax" class="%s">%s</a>', $url, $class, strip_tags($content));
                }
            }
        // Inline support content
        } else {
            $container = ($feed == 'print') ? 'sidebar' : 'div';
            $inline_classes = array('article-reference', 'float-'.$float);
            $inline_styles  = array();
            if (isset($bgcolor)) {
                $inline_styles[] = 'background-color:'.$bgcolor.';';
            }
            if (isset($width)) {
                $inline_styles[] = 'max-width:'.$width.'px;';
            }
            if (substr($content, 0, 4) === '</p>' && substr($content, -3) === '<p>') {
                $content = substr($content, 4, strlen($content) - 7);
            }
            echo sprintf('<%1$s class="%2$s" style="%3$s">%4$s</%1$s>', $container, implode(' ', $inline_classes), implode('', $inline_styles), $content);
        }
    }
}