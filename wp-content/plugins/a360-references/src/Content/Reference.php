<?php
namespace A360\References\Content;

use A360\Core\Content;
use A360\Core\ContentType;

class Reference extends Content
{
    protected static $content_type;
    
    public static function bootstrap()
    {
        static::$content_type = new ContentType('reference', 'Sidebars', 'Sidebar');
        static::$content_type->add_arg('supports', array('title', 'excerpt', 'editor'));
        static::$content_type->add_arg('exclude_from_search', true);
        static::$content_type->add_arg('public', true);
    }
}