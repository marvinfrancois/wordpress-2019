!function() {
    
    var pluginUrl;

	// Load plugin specific language pack
	//tinymce.PluginManager.requireLangPack('reference');

	tinymce.create('tinymce.plugins.ReferencesPlugin', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
            
            pluginUrl = url;

			// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');
			ed.addCommand('DirectoryReferenceExternalOpen', function(a, b) {
				//console.log(arguments);
				w = ed.windowManager.open({
					file : url + '/dialog-external.php?v=1',
					title : 'Insert External Sidebar',
					width : 400 + ed.getLang('directory.delta_width', 0),
					height : 400 + ed.getLang('directory.delta_height', 0),
					inline : 1
				}, {
					plugin_url : url
				});
			});
            
            ed.addCommand('DirectoryReferenceInlineOpen', function(a, b) {
				//console.log(arguments);
				w = ed.windowManager.open({
					file : url + '/dialog-inline.php?v=2',
					title : 'Insert Inline Sidebar',
					width : 400 + ed.getLang('directory.delta_width', 0),
					height : 400 + ed.getLang('directory.delta_height', 0),
					inline : 1
				}, {
					plugin_url : url
				});
			});

			ed.addCommand('DoShortcode', function(shortcode, attributes) {

				var content = '',
                    selection = ed.selection.getContent() || attributes._default;

				content += '['+shortcode;

				for (var i in attributes) {
                    if (i[0] !== '_') {
                        content += ' '+i+'="'+attributes[i]+'"';
                    }
                }

				content += ']';

				if (selection)
					content += selection+'[/'+shortcode+']';

				return content;
			});

		},

		/**
		 * Creates control instances based in the incoming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {

			if (n == "insert_reference_button")
			{
                console.log(n, cm);
                
				// Create Menu
				n = cm.createMenuButton("insert_reference_button", {
					title : "Insert Sidebar",
					image : pluginUrl + "/layout_sidebar.png",
					icons : false
				});

				// Render menu
				n.onRenderMenu.add(function (c, b) {

					// Add button to insert external sidebar
					b.add({
						title :'External Sidebar',
						onclick :function () {
							tinyMCE.activeEditor.execCommand("DirectoryReferenceExternalOpen");
						}
					});

					// Button to insert inline sidebar
					b.add({
						title :'Inline Sidebar',
						onclick :function () {
							tinyMCE.activeEditor.execCommand("DirectoryReferenceInlineOpen");
						}
					});

				});

				return n;
			}
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname	: 'References plugin',
				author		: 'Landon Springer',
				authorurl	: 'http://www.allied360.com',
				version		: "0.1"
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('reference', tinymce.plugins.ReferencesPlugin);

}();