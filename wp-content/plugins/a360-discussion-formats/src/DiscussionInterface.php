<?php
namespace A360\Discussion;

interface DiscussionInterface
{
    public function getCommentsTemplate();
}