<?php
namespace A360\Discussion\Drivers;

use A360\Discussion\Discussion;
use A360\Discussion\DiscussionInterface;
use A360\Core\Template;

class Qanda implements \A360\Discussion\DiscussionInterface
{   
    public function __construct()
    {
        // Register hooks
        add_filter('comment_form_default_fields', array($this, 'comment_fields'));
        add_action('comment_post', array($this, 'save_comment_meta'));
        add_action('pre_comment_on_post', array($this, 'concat_name'));
        add_filter('preprocess_comment', array($this, 'validate_data'));

        // Remove URL from form
        add_filter('comment_form_field_url', function() {
            return '';
        });
        
        $class = __CLASS__;
        add_filter('comments_list_args_qanda', function($args) use($class) {
            $args['callback'] = $class . '::questionsTemplate';
            return $args;
        });
        
        
        add_action('admin_menu', function() use($type) {
			remove_meta_box( 	'commentsdiv', 	$type->name, 'side' );
			add_meta_box(		'commentsdiv',	__('Questions / Answers'), 'post_comment_meta_box', $type->name, 'normal', 'low');
		});
    }
    
    public function getCommentsTemplate()
    {
        return Template::locate(array('comments-qanda.php', 'comments.php'), Discussion::getViewPath());
    }

    public function comment_fields($fields)
    {
        $this->textField('first_name', 'First Name <span class="required">*</span> ', $fields);
        $this->textField('last_name', 'Last Name <span class="required">*</span> ', $fields);
        $this->textField('city', 'City ', $fields);
        $this->textField('state', 'State ', $fields);

        $fields['permission'] = '<p class="comment-form-permission">' .
            '<label for"permission">' . __("Permission to use your name ") . '</label>' .
            '<input id="permission" name="permission" type="checkbox" value="true" >' .
            '</p>';

        // Unset name field, we will concat first and last name later
        unset($fields['author']);

        return $fields;
    }

    public function textField($fieldName, $fieldLabel, &$fields)
    {
        $fields[$fieldName] = '<p class="comment-form-' . $fieldName . '">' .
            '<label for"' . $fieldName . '">' . __($fieldLabel) . '</label>' .
            '<input id="' . $fieldName . '" name="' . $fieldName . '" type="text" >' .
            '</p>';
    }

    public function save_comment_meta($id)
    {
        $this->saveCommentMeta('first_name', $id);
        $this->saveCommentMeta('last_name', $id);
        $this->saveCommentMeta('city', $id);
        $this->saveCommentMeta('state', $id);
        $this->saveCommentMeta('permission', $id);
    }

    /**
     * @todo Remove $_POST reference
     * 
     * @param type $key
     * @param type $id
     */
    public static function saveCommentMeta($key, $id)
    {
        if (static::testValue($key)) {
            $val = wp_filter_nohtml_kses($_POST[$key]);
            add_comment_meta($id, $key, $val);
        }
    }

    /**
     * @todo Remove $_POST reference
     * 
     * @param type $key
     * @return boolean
     */
    public function testValue($key)
    {
        $val = $_POST[$key];
        if (isset($val) and ($val != '')) {
            return true;
        }
        return false;
    }

    /**
     * @todo Remove $_POST references
     * 
     * @param type $comment_post_ID
     */
    public function concat_name($comment_post_ID)
    {
        $_POST['author'] = $_POST['first_name'] . ' ' . $_POST['last_name'];
    }

    /**
     * @todo Remove $_POST references
     * 
     * @param type $commentData
     * @return type
     */
    public static function validate_data($commentData)
    {

        // If this is a reply, return data;
        if (isset($commentData['comment_parent'])) {
            return $commentData;
        }
        $fieldsFailed = array();
        if (!static::validate_field($_POST['first_name'])) {
            $fieldsFailed[] = 'First Name';
        }
        if (!static::validate_field($_POST['last_name'])) {
            $fieldsFailed[] = 'Last Name';
        }
        if (count($fieldsFailed) > 0) {
            $message = '<strong>ERROR</strong>: please fill the required fields (';
            $message .= implode(', ', $fieldsFailed);
            $message .= ").";
            wp_die(__($message));
        }
        return $commentData;
    }

    public static function validate_field($field)
    {
        if (!isset($field) or '' === $field or ( 2 > strlen($field) )) {
            return false;
        }
        return true;
    }

    public static function questionsTemplate($comment, $args, $depth)
    {
        $type = $depth === 1 ? 'question' : 'answer';

        if ($depth === 1 and $comment->comment_approved and !$args['has_children']) {
            return;
        }
        ?>

        <li id="<?= $type; ?>-<?php comment_ID(); ?>" <?php comment_class($type); ?>>
            <article id="comment-<?php comment_ID(); ?>" class="comment-body">

                    <?php if ($depth === 1) : ?>

                    <div class="question">

                        <div class="actions">
                            <?php
                            if ($args['has_children']):
                                ?>
                                <a href="#" data-view="<?php comment_ID(); ?>">View Answer</a>
                                <?php
                            endif;
                            ?>
                        </div>

                        <?= wpautop($comment->comment_content); ?>
                        
                        <?php
                        if (!$comment->comment_approved) :
                            defined('DONOTCACHEPAGE') or define('DONOTCACHEPAGE', true);
                            ?>
                            <p class="comment-awaiting-moderation" style="margin-top:1em;padding:1em;background:#d9edf7;border:1px solid #bce8f1;color:#31708f;text-align:center;font-style:italic;">
                                <?php _e(apply_filters('discussion_qanda_confirmation', 'Your question is awaiting an answer.'), 'allied'); ?>
                            </p>
                            <?php
                        endif;
                        ?>
                    </div>

                <?php else : ?>

                    <div class="answer">
                <?= wpautop($comment->comment_content); ?>
                    </div>

        <?php endif; ?>

            </article>
        <?php
    }

}

/**
 * 
 * <div class="questions expert-questions">
        <?php
        if (comments_open() || get_comments_number() > 0) {
            comments_template();
        }
        ?>
    </div>
    <script type="text/javascript">
        ready(['jquery', 'discussion/qanda'], function($, init) {
            $('#respond h3').html('<a href="#">' + $('#respond h3').html() + '</a>');
            init($('.questions'));
        });
    </script>
 * 
 */
