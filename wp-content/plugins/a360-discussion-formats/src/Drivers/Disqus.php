<?php
namespace A360\Discussion\Drivers;

use A360\Discussion\Discussion;
use A360\Discussion\DiscussionInterface;
use A360\Core\Template;

class Disqus implements DiscussionInterface
{
    public function getCommentsTemplate()
    {
        return Template::locate(array('comments-disqus.php', 'comments.php'), Discussion::getViewPath());
    }
    
    public function getBlogName()
    {
        return apply_filters('discussion_disqus_blogname', '');
    }
    
    public function getUrl()
    {
        if (($content = Discussion::content()) and $content->disqus_url)
        {
            return $content->disqus_url;
        }
    }
}

/**
global $disqus_url;
if (get_post_type() == 'post' and ($postObject = Post::forge()) and $postObject->disqus_url)
{
    $disqus_url = $postObject->disqus_url;
}
 */