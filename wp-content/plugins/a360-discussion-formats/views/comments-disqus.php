<?php
use A360\Discussion\Discussion;
$instance = Discussion::driver();
?>

<div id="comments" class="comments-area discussion discussion-disqus">

	<div id="disqus_thread"></div>
    
	<script type="text/javascript">
		var disqus_shortname = '<?= $instance->getBlogName(); ?>';
	<?php if (($disqus_url = $instance->getUrl())) : ?>
		var disqus_url = '<?= $disqus_url; ?>';
	<?php endif; ?>

		(function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>

</div>
