<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to allied_comment() which is
 * located in the inc/template-tags.php file.
 *
 * @package Allied
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area questions <?= get_post_type(); ?>-questions">
    
    <?php if (1 or !is_user_logged_in()) :
		comment_form(array(
            'id_form'               => 'question_form',
			'title_reply' 			=> '',
			'label_submit'			=> 'Post Question',
			'comment_field'			=> '<p class="comment-form-comment"><label for="comment">' . _x( 'Question', 'noun' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
			'comment_notes_after'	=> '',
			'comment_notes_before'	=> '',
		));
	endif;
	?>

	<?php if ( have_comments() ) : ?>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above" class="navigation-comment" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'allied' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'allied' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'allied' ) ); ?></div>
		</nav><!-- #comment-nav-before -->
		<?php endif; // check for comment navigation ?>

		<ol class="comment-list">
			<?php
				/* Loop through and list the comments. Tell wp_list_comments()
				 * to use allied_comment() to format the comments.
				 * If you want to overload this in a child theme then you can
				 * define allied_comment() and that will be used instead.
				 * See allied_comment() in inc/template-tags.php for more.
				 */
                $args = apply_filters('comments_list_args_qanda', array(
                    'reverse_top_level' => false,
                    'per_page' => 1000,
                    'type' => 'comment'
                ));
                wp_list_comments($args);
			?>
		</ol><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="navigation-comment" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Questions navigation', 'allied' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Questions', 'allied' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Questions &rarr;', 'allied' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

</div><!-- #comments -->
