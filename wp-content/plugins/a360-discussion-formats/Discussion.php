<?php

/*
  Plugin Name: A360 Discussion Format
  Description: Descriptions
  Version: 0.1
  Author: Steven Kohlmeyer
  Author URI: http://www.allied360.com/
 */
/*

  Reference for info: http://wp.smashingmagazine.com/2012/05/08/adding-custom-fields-in-wordpress-comment-form/

 */

namespace A360\Discussion;

use A360\Core\Plugin;
use A360\Core\Content;
use A360\Core\ContentType;
use A360\Core\FieldGroup;
use A360\Core\Field;
use A360\Core\Shortcode;
use A360\Core\Metabox;
use A360\Core\Taxonomy;
use A360\Core\User;

if (!class_exists('\A360\Core\Plugin'))
    return;

class Discussion extends Plugin
{

    protected static $contentTypes = array();
    protected static $driver;
    protected static $content;
    protected static $viewPath = NULL;

    public static function driver()
    {
        return static::$driver;
    }

    public static function content()
    {
        return static::$content;
    }

    public static function bootstrap()
    {
        static::register_autoloader(dirname(__FILE__).'/src', 'A360\Discussion');

        static::$viewPath = dirname(__FILE__).'/views';

        // Add custom comment types
        add_filter('admin_comment_types_dropdown', function($types) {
            $types['question'] = 'Questions';
            $types['answer']   = 'Answers';
            return $types;
        });

        add_action('init', function() {
            if (isset($_POST['comment_post_ID']))
            {
                Discussion::makePostDiscussionDriver($_POST['comment_post_ID']);
            }
        });

        add_action('wp', function() {
            Discussion::makePostDiscussionDriver();
        });

        parent::bootstrap();
    }

    public static function makePostDiscussionDriver($post_id = null)
    {
        empty($post_id) and $post_id = get_the_ID();

        if (is_single($post_id) or isset($_POST['comment_post_ID'])) {
			if ((comments_open($post_id) || get_comments_number($post_id) > 0)) {
				if ((static::$content = Content::forge($post_id)) and ($format = static::$content->discussion_format)) {
					$class = __NAMESPACE__ . '\\Drivers\\' . ucfirst($format);
	                if (class_exists($class)) {
	                    $driver = new $class;
	                    add_filter('comments_template', array($driver, 'getCommentsTemplate'));
	                    Discussion::$driver = $driver;
	                }
	            }
	        }
        } else {
        	return false;
        }

//        if (!is_single($post_id) and !isset($_POST['comment_post_ID']))
//            return false;

/*  moved to the condition
        if ((comments_open($post_id) || get_comments_number($post_id) > 0)) {

            if ((static::$content = Content::forge($post_id)) and ($format = static::$content->discussion_format)) {

                $class = __NAMESPACE__ . '\\Drivers\\' . ucfirst($format);

                if (class_exists($class)) {
                    $driver = new $class;
                    add_filter('comments_template', array($driver, 'getCommentsTemplate'));
                    Discussion::$driver = $driver;
                }
            }
        }
*/
    }


    /**
     * Set the view path
     *
     * @param $path
     * @return bool|null
     */
    public static function setViewPath($path){
        if(!is_dir($path))
            return false;
        static::$viewPath = $path;
        return static::$viewPath;
    }

    public static function getViewPath()
    {
        return static::$viewPath;
    }

    public static function isEnabledForContentType($type = null, $post_id = null)
    {
        if (is_null($type) and !empty($post_id)) {
            $type = get_post_type($post_id);
        }
        $type = ($type instanceof ContentType) ? $type->name : $type;
        empty($type) and $type = get_post_type();
        return in_array($type, static::$contentTypes);
    }

    public static function enableForContentType(ContentType $type)
    {
        static::$contentTypes[] = $type->name;

        $fg = new FieldGroup('discussion', 'Discussion Settings', $type);

        // Discussion Format Selection
        Field\Selection\Dropdown::forge('discussion_format', 'Discussion Format', $fg, array(
            '' => '-Use WordPress Default-',
            'qanda' => 'Question & Answer',
            'disqus' => 'Disqus'
        ));

        // Disqus URL
        Field\Url::forge('disqus_url', 'Disqus URL', $fg);
    }

}

Discussion::init();
